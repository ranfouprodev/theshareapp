package corp.com.shareapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import corp.com.shareapp.model.EventModel;
import corp.com.shareapp.R;
import corp.com.shareapp.model.EventTags;

/**
 * Created by randyfournier on 7/28/16.
 */
public class EventListAdapter extends ArrayAdapter {

    private static final String TAG = "EventListAdapter";


    private List<EventModel> eventModelList;
    private int resource;
    private LayoutInflater inflater;

    public EventListAdapter(Context context, int resource, List<EventModel> objects) {
        super(context, resource, objects);

        eventModelList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.event_list, null);
        }

        TextView tvEventName = (TextView) convertView.findViewById(R.id.tvEventName);
        TextView tvEventType = (TextView) convertView.findViewById(R.id.tvEventType);
        TextView tvPhotoCount = (TextView) convertView.findViewById(R.id.tvPhotoCount);
        TextView tvRemaining = (TextView) convertView.findViewById(R.id.tvRemaining);
        final ImageView ivClickToViewEvent = (ImageView) convertView.findViewById(R.id.ivClickToViewEvent);


        tvEventName.setText(eventModelList.get(position).getEvent_name());
        tvEventType.setText(eventModelList.get(position).getType() + " Event");

        EventTags et = new EventTags();
        et.setEvent_id(eventModelList.get(position).getEvent_id());
        et.setEvent_name(eventModelList.get(position).getEvent_name());
        et.setRemaining(eventModelList.get(position).getRemaining());
        et.setIs_owner(eventModelList.get(position).getIs_owner());
        et.setLib_allowed(eventModelList.get(position).getLib_allowed());
        et.setRead_only(eventModelList.get(position).isRead_only());
        et.setIs_subscribed(eventModelList.get(position).is_subscribed());

        tvEventType.setTag(et);

        String photo = eventModelList.get(position).getPic_count() > 1 || eventModelList.get(position).getPic_count() == 0 ? "Photos" : "Photo";

        tvPhotoCount.setText(String.valueOf(eventModelList.get(position).getPic_count()) + " " + photo);
        tvRemaining.setText(eventModelList.get(position).getRemaining() + " left");



        return convertView;
    }
}
