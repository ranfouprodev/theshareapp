package corp.com.shareapp.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.bigkoo.svprogresshud.SVProgressHUD;

import corp.com.shareapp.main.LoginActivity;
import corp.com.shareapp.network.Users;
import corp.com.shareapp.R;

public class LostPasswordActivity extends AppCompatActivity {

    private static final String TAG = "LostPasswordActivity";
    private SVProgressHUD mSVProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //FacebookSdk.sdkInitialize(getApplicationContext());
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_lost_password);

        mSVProgressHUD = new SVProgressHUD(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // fix flicker between screen transitions
        overridePendingTransition(0, 0);
    }


    public void onClickToLogin(View v) {
        Intent i = new Intent(LostPasswordActivity.this, LoginActivity.class);
        startActivity(i);
    }

    public void onClickRecover(View v) {

        EditText emailField = (EditText) findViewById(R.id.editTextEmail);
        String email = emailField.getText().toString().trim();

        if (email != null && !email.equals("")) {

            Users um = new Users(this);

            mSVProgressHUD.show();
            um.passwordForgot(email, mSVProgressHUD);
        } else {
            emailField.setError("Incomplete, Please enter an email.");
        }
    }
}
