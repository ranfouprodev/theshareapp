package corp.com.shareapp.location;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import corp.com.shareapp.R;
import corp.com.shareapp.nearby.NearbyTabActivity;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "Map";

    double lat=0;
    double lon=0;
    String location_name="";

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getAttachedBy().equals("nearby")){
            NearbyTabActivity.setSelectedCardIndex(getPosition());
        }
    }

    private int position;
    private String attachedBy;

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    boolean mShowMap;
    GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_fragment);

        Intent i = getIntent();
        lat = i.getDoubleExtra("lat", 0);
        lon = i.getDoubleExtra("lon",0);
        setPosition(i.getIntExtra("position",0));
        setAttachedBy(i.getStringExtra("attachedBy"));
        location_name = i.getStringExtra("location_name");

        mShowMap = GooglePlayServiceUtility.isPlayServiceAvailable(this) && initMap();

        Log.d(TAG, "onCreate: attachedBy =" + getAttachedBy() + " position = " + getPosition());

        TextView tvMapTitle = (TextView)findViewById(R.id.tvMapTitle);
        tvMapTitle.setText(location_name);
    }


    public void mapClose(View v) {

        if(getAttachedBy().equals("nearby")){

            NearbyTabActivity.setSelectedCardIndex(getPosition());
        }
        finish();
    }

    private boolean initMap() {
        if (mMap == null) {

            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
        return (mMap != null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng cameraLatLng = new LatLng(lat,lon);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(cameraLatLng, 16);
        mMap.moveCamera(update);

        mMap.addMarker(new MarkerOptions()
                .position(cameraLatLng)
                .title(location_name)
                .anchor(.5f, .5f)
        );

        CircleOptions circleOptions = new CircleOptions()
                .strokeWidth(3)
                .fillColor(0x330000FF)
                .strokeColor(Color.BLUE)
                .center(cameraLatLng)
                .radius(333);
        mMap.addCircle(circleOptions);
    }
}
