package corp.com.shareapp.model;

/**
 * Created by randyfournier on 8/2/16.
 */

public class PictureTags {
    private int img_id;
    private String url;
    private int position;
    private String username;
    private int event_id;
    private String event_name;
    private String remaining;
    private Boolean has_reported;
    private int numComments;

    public int getNumComments() {
        return numComments;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public Boolean getHas_reported() {
        return has_reported;
    }

    public void setHas_reported(Boolean has_reported) {
        this.has_reported = has_reported;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getImg_id() {
        return img_id;
    }

    public void setImg_id(int img_id) {
        this.img_id = img_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
