package corp.com.shareapp.interfaces;

import java.util.ArrayList;

import corp.com.shareapp.model.EventModel;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface ISearchPopoverListener {

    void Searches(ArrayList<EventModel> searchlist);
}
