package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import corp.com.shareapp.R;

/**
 * Created by randyfournier on 8/20/16.
 */
public class EventTabFragment extends Fragment {

    public EventTabFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.event_tab_fragment, container, false);
        return theView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        InputMethodManager imm =  (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        TextView tvSearch = (TextView) view.findViewById(R.id.tvSearch);

        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go to the SearchFragment
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new SearchFragment(), "evt")
                        .commit();
            }
        });

    }
}
