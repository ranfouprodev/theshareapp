package corp.com.shareapp.interfaces;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ashwinnbhanushali on 20/04/17.
 */

public interface ApiCallback {
     void onSuccess(int statusCode, Header[] headers, JSONObject res);
     void onFailure(int statusCode, Header[] headers, String res, Throwable t);
     void noInternet();
}
