package corp.com.shareapp.model;

import android.graphics.Bitmap;
import android.net.Uri;


/**
 * Created by randyfournier on 8/2/16.
 */

public class PostPhotoModel {
    private static final String TAG = "PostPhotoModel";

    private Bitmap pictureAsBitmap;

    private String imagePath;

    private Uri uri;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Bitmap getPictureAsBitmap() {
        return pictureAsBitmap;
    }

    public void setPictureAsBitmap(Bitmap pictureAsBitmap) {
        this.pictureAsBitmap = pictureAsBitmap;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    private String caption = "";
}
