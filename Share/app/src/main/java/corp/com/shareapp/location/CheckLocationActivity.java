package corp.com.shareapp.location;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import corp.com.shareapp.R;
import corp.com.shareapp.main.LoginActivity;
import corp.com.shareapp.network.Nearby;

public class CheckLocationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_location);

        TextView top_message = (TextView)findViewById(R.id.tvCheckLocationMessage);
        TextView below_message = (TextView)findViewById(R.id.tvCheckLocationMessage2);

        top_message.setText(getText(R.string.get_location_message_top));
        below_message.setText(getText(R.string.get_location_message_below));

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedprefs.edit();
        editor.putBoolean("disallowLocation", true);
        editor.commit();
    }

    public void onClickToLoginLocation(View v) {
        //set shared prefs visitedCheckLocation = true
        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedprefs.edit();
        editor.putBoolean("visitedCheckLocation", true);
        editor.commit();
        Intent i = new Intent(CheckLocationActivity.this, LoginActivity.class);
        i.addFlags(i.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(i);
        finish();
    }

    public void onClickLocationRefresh(View v) {
        Nearby nb = new Nearby(this);
        nb.checkLocation();
    }

    public void onClickLocationSend(View v){
        EditText email = (EditText)findViewById(R.id.etLocationEmail);
        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String registration_id = sharedprefs.getString("registration_id","");

        Nearby nb = new Nearby(this);

        nb.futureUser(email.getText().toString().trim(), registration_id);
    }
}
