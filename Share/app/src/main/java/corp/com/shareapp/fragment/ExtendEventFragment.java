package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.SkuDetails;
import com.facebook.appevents.AppEventsLogger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.SkuListAdapter;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.profile.ProfileActivity;

/**
 * Created by randyfournier on 8/20/16.
 */
public class ExtendEventFragment extends Fragment {

    private static final String TAG = "ExtendEventFragment";
    private int selectedSku = -1;
    private List<SkuDetails> skus;
    private BillingProcessor bp;
    private int event_id;
    private SkuDetails skuDetails;
    private DateFormat dateFormat = new SimpleDateFormat( "MM/dd/yyyy" );
    private TextView tvExtendExpires;

    int days;

    public void setSkus(List<SkuDetails> skus) {
        this.skus = skus;
    }

    public int getSelectedSku() {
        return selectedSku;
    }

    public void setSelectedSku(int selectedSku) {
        this.selectedSku = selectedSku;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger( getActivity() );
        logger.logEvent( "Visit Event Extend" );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate( R.layout.extend_event_fragment, container, false );

        return theView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {


        final Bundle c = getArguments();

        if (c != null) {

            TextView tvExtendEventTitle = (TextView) view.findViewById( R.id.tvExtendEventTitle );
            String event_name = "Extend " + c.getString( "event_name" );
            event_id = c.getInt( "event_id" );
            tvExtendExpires = (TextView) view.findViewById( R.id.tvExtendExpires );

            //String daysRemaining = ((ProfileActivity) getActivity()).getRemaining();

            final String daysRemaining = c.getString( "remaining" );

            days = Integer.parseInt( daysRemaining.substring( 0, daysRemaining.length() - 1 ) );

            final Calendar calOriginal = getCalendar();
            String newDate = dateFormat.format( calOriginal.getTime() );

            tvExtendExpires.setText( "Event will expire on: " + newDate + "." );

            tvExtendEventTitle.setText( event_name );
        }

        skus = new ArrayList<>();
        //= {"Extend Event 1 Week","Extend Event 1 Month","Extend Event 3 Months","Extend Event 6 Months","Extend Event 1 Year"};

        if (getActivity() instanceof EventActivity) {
            bp = ((EventActivity) getActivity()).getBp();
        } else {
            bp = ((ProfileActivity) getActivity()).getBp();
        }

        skus.add( bp.getPurchaseListingDetails( "corp.com.shareapp.extend_event_week" ) );
        skus.add( bp.getPurchaseListingDetails( "corp.com.shareapp.extend_event_month" ) );
        skus.add( bp.getPurchaseListingDetails( "corp.com.shareapp.extend_event_three_months" ) );
        skus.add( bp.getPurchaseListingDetails( "corp.com.shareapp.extend_event_six_months" ) );
        skus.add( bp.getPurchaseListingDetails( "corp.com.shareapp.extend_event_year" ) );


        SkuListAdapter adapter = new SkuListAdapter( getActivity(), R.layout.sku_list, skus );

        final ListView listView = (ListView) view.findViewById( R.id.lvSkuList );
        listView.setAdapter( adapter );

        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                setSelectedSku( position );
                skuDetails = skus.get( position );
                changeDateOfExpiry( skuDetails );
            }
        } );

        Button btnExtendEvent = (Button) view.findViewById( R.id.btnExtendEvent );
        btnExtendEvent.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedSku == -1) {
                    NetworkUtils nu = new NetworkUtils( getActivity() );
                    nu.networkDialog( "Please select a product!", "Click on one of the products above." );
                } else { //a sku was selected
                    NetworkUtils nu = new NetworkUtils( getActivity() );
                    //nu.networkDialog("Skus", "You selected " + skus.get(selectedSku).productId);
                    //nu.networkDialog("item name = ", skus.get(selectedSku).title);
                    Log.e("Purchase","Purchase started");
                    if(getActivity() instanceof ProfileActivity){
                        ProfileActivity activity = (ProfileActivity)getActivity();
                        activity.setEvent_id(event_id);
                    }else if(getActivity() instanceof EventActivity){
                        EventActivity activity = (EventActivity)getActivity();
                        activity.setEvent_id(event_id);
                    }
                    bp.purchase( getActivity(), skuDetails.productId );

                    selectedSku = getSelectedSku();
                    //bp.purchase(getActivity(), "android.test.purchased");
                }

            }
        } );

    }

    @NonNull
    private Calendar getCalendar() {
        final Calendar calOriginal = Calendar.getInstance();
        calOriginal.add( Calendar.DAY_OF_YEAR, days);
        return calOriginal;
    }

    public void changeDateOfExpiry(SkuDetails skuDetails) {
        String newDate = null;
        Calendar expiryCal = null;
        final Calendar calOriginal = getCalendar();


        switch (skuDetails.productId) {
            case "corp.com.shareapp.extend_event_week":
                expiryCal = calOriginal;
                expiryCal.add( Calendar.DATE, 7 );
                newDate = dateFormat.format( expiryCal.getTime() );
                tvExtendExpires.setText( "Event will expire on: " + newDate + "." );
                break;
            case "corp.com.shareapp.extend_event_month":
                expiryCal = calOriginal;
                expiryCal.add( Calendar.MONTH, 1 );
                newDate = dateFormat.format( expiryCal.getTime() );
                tvExtendExpires.setText( "Event will expire on: " + newDate + "." );
                break;
            case "corp.com.shareapp.extend_event_three_months":
                expiryCal = calOriginal;
                expiryCal.add( Calendar.MONTH, 3 );
                newDate = dateFormat.format( expiryCal.getTime() );
                tvExtendExpires.setText( "Event will expire on: " + newDate + "." );
                break;
            case "corp.com.shareapp.extend_event_six_months":
                expiryCal = calOriginal;
                expiryCal.add( Calendar.MONTH, 6 );
                newDate = dateFormat.format( expiryCal.getTime() );
                tvExtendExpires.setText( "Event will expire on: " + newDate + "." );
                break;
            case "corp.com.shareapp.extend_event_year":
                expiryCal = calOriginal;
                expiryCal.add( Calendar.YEAR, 1 );
                newDate = dateFormat.format( expiryCal.getTime() );
                tvExtendExpires.setText( "Event will expire on: " + newDate + "." );
                break;
            default:
                break;
        }
    }

}