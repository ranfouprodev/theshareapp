package corp.com.shareapp.network;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.interfaces.ApiCallback;
import corp.com.shareapp.interfaces.EventGalleryListener;
import corp.com.shareapp.interfaces.IBatchUploadListener;
import corp.com.shareapp.interfaces.ICreateEventListener;
import corp.com.shareapp.interfaces.IEventButtonListener;
import corp.com.shareapp.interfaces.IEventsListener;
import corp.com.shareapp.interfaces.IPhotosListener;
import corp.com.shareapp.interfaces.IPrivateEventListener;
import corp.com.shareapp.interfaces.ISearchListener;
import corp.com.shareapp.interfaces.IUploadListener;
import corp.com.shareapp.model.EventModel;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PostPhotoModel;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

import static java.lang.Integer.parseInt;

/**
 * Created by randyfournier on 9/9/16.
 */
public class Events extends ContextWrapper {

    private static final String TAG = "Events";

    AsyncHttpClient client = new AsyncHttpClient();

    // no ssl workaround
    /*AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);*/

    private SVProgressHUD mSVProgressHUD;

    IEventsListener eventslistener;
    IPhotosListener myphotoslistener;
    EventGalleryListener eventgallerylistener;
    IUploadListener uploadCompleted;
    ISearchListener searchlistener;
    ICreateEventListener cel;
    int page_number = 1;
    private boolean allUserPicsLoaded = false;

    private ArrayList<PictureModel> piclist = new ArrayList<>();

    public ArrayList<PictureModel> getPiclist() {
        return piclist;
    }

    public void setPiclist(ArrayList<PictureModel> piclist) {
        this.piclist = piclist;
    }

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public Events(Context base) {
        super(base);
        mSVProgressHUD = new SVProgressHUD(getBaseContext());
    }

    public void eventsPublicCreate(String event_name) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_name", event_name);
            params.put("can_upload", true);

            mSVProgressHUD.show();

            client.post(getString(R.string.host) + getString(R.string.rt_events_public_create), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "Created Events " + res.toString());
                    mSVProgressHUD.dismiss();

                    // check for critical error
                    String error_level = "";
                    try {
                        if (res.has("error_message")) {

                            if (res.has("error_level")) {
                                error_level = res.getString("error_level");
                            }

                            if (error_level.equals("critical")) {
                                nu.criticalError("Error", res.getString("error_message"));
                            } else {
                                nu.networkDialog(res.getString("op_status"), res.getString("error_message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject dataObject = res.getJSONObject("data");

                        Gson gson = new Gson();
                        EventModel event = gson.fromJson(dataObject.toString(), EventModel.class);

                        Activity activity = (Activity) getBaseContext();
                        cel = (ICreateEventListener) activity;
                        cel.EventCreated(event);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mSVProgressHUD.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                    mSVProgressHUD.dismiss();
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
            mSVProgressHUD.dismiss();
        }
    }

    public void eventsPrivateCreate(String event_name, String password) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_name", event_name);
            params.put("password", password);
            params.put("can_upload", true);

            mSVProgressHUD.show();

            client.post(getString(R.string.host) + getString(R.string.rt_events_private_create), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "Created Events " + res.toString());
                    mSVProgressHUD.dismiss();

                    // check for critical error
                    String error_level = "";
                    try {
                        if (res.has("error_message")) {

                            if (res.has("error_level")) {
                                error_level = res.getString("error_level");
                            }

                            if (error_level.equals("critical")) {
                                nu.criticalError("Error", res.getString("error_message"));
                            } else {
                                nu.networkDialog(res.getString("op_status"), res.getString("error_message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject dataObject = res.getJSONObject("data");

                        Gson gson = new Gson();
                        EventModel event = gson.fromJson(dataObject.toString(), EventModel.class);

                        Activity activity = (Activity) getBaseContext();
                        cel = (ICreateEventListener) activity;
                        cel.EventCreated(event);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mSVProgressHUD.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void eventsPrivateAccess(final int pos, final ArrayList<EventModel> searchlist, String pword) throws JSONException, UnsupportedEncodingException {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            final String session_id = sharedprefs.getString("session_id", "");

           //Log.d(TAG, "eventsPrivateAccess: password= " + pword);
            //Log.d(TAG, "eventsPrivateAccess: event_id= " + searchlist.get(pos).getEvent_id());

            JSONObject params = new JSONObject();
            params.put("password", pword);
            params.put("session_id", session_id);
            params.put("event_id", String.valueOf(searchlist.get(pos).getEvent_id()));

            ByteArrayEntity entity = new ByteArrayEntity(params.toString().getBytes("UTF-8"));

            client.post(getBaseContext(), getString(R.string.host) + getString(R.string.rt_events_private_access), entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "in eventsPrivateAccess " + res.toString());
                    Log.d(TAG, session_id);
                    Activity activity = (Activity) getBaseContext();
                    IPrivateEventListener privateEventCallback = (IPrivateEventListener) activity;

                    try {
                        String op_status = res.getString("op_status");
                        if(op_status.equals("success")){

                            privateEventCallback.PrivateEventSuccessful(pos, searchlist);

                        }else{
                            privateEventCallback.PrivateEventFail(pos, searchlist);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //call JSONEventsPrivateAccess(pos, searchlist, res);
                    // create a callback with event_id as arg
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void subscribe(final int event_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);

            client.post(getString(R.string.host) + getString(R.string.rt_events_subscribe), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "eventsSubscribe" + res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void unsubscribe(final int event_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);

            client.post(getString(R.string.host) + getString(R.string.rt_events_unsubscribe), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "eventsUnsubscribe" + res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void lib_allowed(final int event_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);

            client.post(getString(R.string.host) + getString(R.string.rt_events_lib_allowed), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    //lib allowed set for this event
                    Log.d(TAG, "lib_allowed" + res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void read_only(final int event_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);

            client.post(getString(R.string.host) + getString(R.string.rt_events_read_only), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    //lib allowed set for this event
                    Log.d(TAG, "read_only" + res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void eventsDelete(final int event_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");
            final String history = sharedprefs.getString("history", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);

            client.post(getString(R.string.host) + getString(R.string.rt_events_delete), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    //check sharedprefs history to see if this event exists if so remove it
                    List<EventModel> historyList = new ArrayList<>();
                    Gson gson = new Gson();

                    if (!history.equals("")) {
                        try {
                            JSONArray searches = new JSONArray(history);

                            for (int iter = 0; iter < searches.length(); iter++) {
                                JSONObject finalObject = searches.getJSONObject(iter);
                                EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);
                                historyList.add(search);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //if not exist in historylist add this clicked item
                        for (EventModel search : historyList) {
                            if (search.getEvent_id()==event_id) {
                                historyList.remove(search);

                                String historyItems = gson.toJson(historyList);

                                SharedPreferences.Editor editor = sharedprefs.edit();

                                editor.putString("history", historyItems);
                                editor.commit();
                            }
                        }
                    }

                    Log.d(TAG, "picturesDelete event" + res.toString());
                    events();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void eventsExtend(final int event_id, String product_id, String receipt) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);
            params.put("product_id", product_id);
            params.put("receipt", receipt);

            client.post(getString(R.string.host) + getString(R.string.rt_events_extend), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "eventsExtend" + res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void eventsExtendWithCallback(final int event_id, String product_id, String receipt, final ApiCallback callback) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);
            params.put("product_id", product_id);
            params.put("receipt", receipt);

            client.post(getString(R.string.host) + getString(R.string.rt_events_extend), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "eventsExtend" + res.toString());
                    callback.onSuccess(statusCode,headers,res);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                    callback.onFailure(statusCode,headers,res,t);
                }
            });

        } else {
            //nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
            callback.noInternet();
        }
    }

    public void events() { // fetch all events for this session_id
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("count", 20);
            params.put("page_number", 1);
            params.put("is_mine", true);

            client.get(getString(R.string.host) + getString(R.string.rt_events), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    Log.d(TAG, "events" + res.toString());

                    // check for critical error
                    try {
                        if (res.has("error_message")) {
                            if (res.getString("error_level").equals("critical")) {
                                nu.criticalError("Error", res.getString("error_message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject dataObject = res.getJSONObject("data");
                        JSONArray parentArray = dataObject.getJSONArray("events");

                        ArrayList<EventModel> eventlist = new ArrayList<>();

                        Gson gson = new Gson();
                        for (int i = 0; i < parentArray.length(); i++) {
                            JSONObject finalObject = parentArray.getJSONObject(i);
                            EventModel event = gson.fromJson(finalObject.toString(), EventModel.class);
                            eventlist.add(event);
                        }

                        Activity activity = (Activity) getBaseContext();
                        eventslistener = (IEventsListener) activity;
                        eventslistener.Events(eventlist);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public boolean isAllUserPicsLoaded() {
        return allUserPicsLoaded;
    }

    public void picturesUser() {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            int count = 20;
            int page_number = 1;

            SharedPreferences sharedprefs = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("count", count);
            params.put("session_id", session_id);

            params.put("page_number", getPage_number());


            client.get(getString(R.string.host) + getString(R.string.rt_pictures_user), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "picturesUser " + res.toString());

                    int count = 0;

                    // check for critical error
                    try {
                        if (res.has("error_message")) {
                            if (res.getString("error_level").equals("critical")) {
                                nu.criticalError("Error", res.getString("error_message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject dataObject = res.getJSONObject("data");
                        JSONArray parentArray = dataObject.getJSONArray("pictures");

                        count =  parseInt(dataObject.getString("count"));
                        setPage_number(parseInt(dataObject.getString("page_number")));

                        Gson gson = new Gson();
                        for (int i = 0; i < parentArray.length(); i++) {
                            JSONObject finalObject = parentArray.getJSONObject(i);
                            PictureModel picture = gson.fromJson(finalObject.toString(), PictureModel.class);
                            getPiclist().add(picture);
                        }

                        Activity activity = (Activity) getBaseContext();
                        myphotoslistener = (IPhotosListener) activity;
                        if(count==20){
                            allUserPicsLoaded = false;
                        }else{
                            allUserPicsLoaded = true;
                        }
                        myphotoslistener.MyPhotos(piclist);

                        if(count == 20){


                            setPage_number(getPage_number() + 1);

                            picturesUser();
                        }else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }


    }

    public void picturesEvent(final int event_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");
            String sorting_option = sharedprefs.getString("event_sorting_option", "date");

            String count = "20";

            String page_number = "1";

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("event_id", event_id);
            params.put("count", count);
            params.put("sorting_option", sorting_option);

            params.put("page_number", getPage_number());

            client.get(getString(R.string.host) + getString(R.string.rt_pictures_event), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    int count = 0;

                    Log.d(TAG, "picturesEvent " + res.toString());
                    // check for critical error
                    try {
                        if (res.has("error_message")) {
                            if (res.getString("error_level").equals("critical")) {
                                nu.criticalError("Error", res.getString("error_message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONObject dataObject = res.getJSONObject("data");
                        JSONArray parentArray = dataObject.getJSONArray("pictures");

                        count =  parseInt(dataObject.getString("count"));
                        setPage_number(parseInt(dataObject.getString("page_number")));

                        Gson gson = new Gson();
                        for (int i = 0; i < parentArray.length(); i++) {
                            JSONObject finalObject = parentArray.getJSONObject(i);
                            PictureModel picture = gson.fromJson(finalObject.toString(), PictureModel.class);
                            getPiclist().add(picture);
                        }

                        Activity activity = (Activity) getBaseContext();
                        eventgallerylistener = (EventGalleryListener) activity;
                        eventgallerylistener.EventGalleryPictures(piclist);

                        if(count == 20){

                            setPage_number(getPage_number() + 1);

                            picturesEvent(event_id);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void picturesDelete(int pic_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("pic_id", pic_id);

            client.post(getString(R.string.host) + getString(R.string.rt_pictures_delete), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "picturesDelete " + res.toString());
                    picturesUser();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }



    public void picturesEventCreate(final File picture, String caption, final String event_name, final int event_id, final ArrayList<PostPhotoModel> photos, final boolean batch, final boolean byPopover) throws IOException {

        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("picture", picture, "image/png");
            params.put("caption", caption);
            params.put("event_id", event_id);
            params.put("session_id", session_id);

            //mSVProgressHUD.show();

            client.post(getString(R.string.host) + getString(R.string.rt_pictures_event_create), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {



                    Activity activity = (Activity) getBaseContext();

                    if(!batch) {
                        Log.d(TAG, "picturesEventCreate:OnSuccess  res = " + res.toString());
                        //mSVProgressHUD.dismiss();
                        boolean deleted = picture.delete();
                        Log.d(TAG, "picturesEventCreate deleted= " + String.valueOf(deleted));


                        uploadCompleted = (IUploadListener) activity;
                        uploadCompleted.Uploaded(event_id);

                        //Toast.makeText(getBaseContext(), "Photo(s) posted to " + event_name, Toast.LENGTH_LONG).show();
                    }else{
                        // handle batch
                        //remove first item from photos
                        photos.remove(0);
                        boolean deleted = picture.delete();
                        //call callback
                        IBatchUploadListener batchUploadCompleted = (IBatchUploadListener) activity;
                        batchUploadCompleted.batchUploaded(photos, event_name, event_id, byPopover);
                        Toast.makeText(getBaseContext(), "Posting photo " + (photos.size()+1) + " to " + event_name + ".", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, "picturesEventCreate:OnFailure res = " + res);
                    mSVProgressHUD.dismiss();
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

            //mSVProgressHUD.dismiss();

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void uploadMulti(ArrayList<PostPhotoModel> photos, String event_name, int event_id, boolean byPopover){


            File pic = new File(photos.get(0).getImagePath());
            try {
                //Toast.makeText(getBaseContext(), "Posting photo " + photos.size() + " to " + event_name + ".", Toast.LENGTH_LONG).show();
                picturesEventCreate(pic, photos.get(0).getCaption(), event_name, event_id, photos, true, byPopover);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public void eventsSearch(final String event_name, boolean my_events) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = this.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            int count = 6;

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("count", count);
            params.put("event_name", event_name);

            if(my_events){
                params.put("is_mine", my_events);
            }


            client.get(getString(R.string.host) + getString(R.string.rt_events), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "onSuccess eventsSearch: " + res.toString());

                    // check for critical error
                    try {
                        if (res.has("error_message")) {
                            if (res.getString("error_level").equals("critical")) {
                                nu.criticalError("Error", res.getString("error_message"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ArrayList<EventModel> searchlist = new ArrayList<>();
                    Gson gson = new Gson();

                    try {
                        JSONObject dataObject = res.getJSONObject("data");
                        JSONArray parentArray = dataObject.getJSONArray("events");

                        for (int i = 0; i < parentArray.length(); i++) {
                            JSONObject finalObject = parentArray.getJSONObject(i);
                            EventModel event = gson.fromJson(finalObject.toString(), EventModel.class);
                            searchlist.add(event);
                        }

                        if (event_name.equals("")) { //if this is an empty search then append what is in sharedprefs to list in callback
                            //add the history to searchlist
                            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);

                            String previousHistory = sharedprefs.getString("history", "");

                            if (!previousHistory.equals("")) {
                                try {
                                    JSONArray searches = new JSONArray(previousHistory);

                                    for (int iter = 0; iter < searches.length(); iter++) {
                                        JSONObject finalObject = searches.getJSONObject(iter);
                                        EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);
                                        searchlist.add(search);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        Activity activity = (Activity) getBaseContext();


                        boolean match = false;



                        if(searchlist.size()==0 && !event_name.equals("")){
                            match = false;
                        }else{
                            //check searchlist names for match
                            for(EventModel search : searchlist){

                                Log.d(TAG, "onSuccess: check for match search = " + search.getEvent_name().toLowerCase());
                                Log.d(TAG, "onSuccess: check for match event_name =" + "#" + event_name);

                                if(search.getEvent_name().toLowerCase().equals("#" + event_name))
                                {
                                    match=true;
                                }
                            }
                        }


                           IEventButtonListener eventbuttonlistener = (IEventButtonListener) activity;
                            eventbuttonlistener.ShowCreateEventButton(event_name, !match);


                        searchlistener = (ISearchListener) activity;
                        searchlistener.Searches(searchlist);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //Log.d(TAG, res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }
}