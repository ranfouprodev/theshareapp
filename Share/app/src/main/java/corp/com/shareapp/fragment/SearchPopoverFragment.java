package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import corp.com.shareapp.R;
import corp.com.shareapp.network.Events;

/**
 * Created by randyfournier on 8/20/16.
 */
public class SearchPopoverFragment extends Fragment {

    private static final String TAG = "SearchPopoverFragment";


    public SearchPopoverFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.fragment_popover_1, container, false);
        return theView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        final Events em = new Events(getActivity());
        em.eventsSearch("", true);

        InputMethodManager imm =  (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        TextView tvSearch = (TextView) view.findViewById(R.id.tvSearch);

        final FrameLayout popover_scene1 = (FrameLayout)view.findViewById(R.id.popover_scene1);
        final FrameLayout popover_scene2 = (FrameLayout)view.findViewById(R.id.popover_scene2);

        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //toggle second scene
                popover_scene1.setVisibility(View.GONE);
                popover_scene2.setVisibility(View.VISIBLE);

                EditText searchEditText = (EditText) getActivity().findViewById(R.id.etPopoverSearch);

                Drawable img;
                Resources res = getResources();
                img = res.getDrawable(R.drawable.ic_search_16dp);
                if (img != null) {
                    img.setBounds(0, 0, 50, 50);
                }
                searchEditText.setCompoundDrawables(img, null, null, null);
                searchEditText.setCompoundDrawablePadding(20);
                //searchEditText.setPadding(0, 0, 0, 0);

                //Log.d(TAG, "onViewCreated value = " + searchEditText.getText().toString());


                searchEditText.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);
                searchEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        EditText searchEditText = (EditText) getActivity().findViewById(R.id.etPopoverSearch);
                        Log.d(TAG, "aftertextchanged value = " + s.toString());

                        if (TextUtils.isEmpty(searchEditText.getText().toString())) {
                            em.eventsSearch("", true);
                        } else {
                            em.eventsSearch(s.toString(), false);
                        }
                    }
                });
            }
        });

        TextView tvCancel = (TextView) view.findViewById(R.id.tvSearchClickCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //toggle first scene
                popover_scene2.setVisibility(View.GONE);
                popover_scene1.setVisibility(View.VISIBLE);
            }
        });



    }
}

