package corp.com.shareapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.FullScreenImageAdapter;
import corp.com.shareapp.model.PictureModel;

public class FullScreenViewActivity extends AppCompatActivity {

    private FullScreenImageAdapter adapter;
    private HackyViewPager viewPager;
    private static final String TAG ="FullScreenViewActivity";
    private String attachedBy;



    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_full_screen_view);

        viewPager = (HackyViewPager) findViewById(R.id.pagerFullScreen);


        Intent i = getIntent();
        int position = i.getIntExtra("position", -1);

        Log.d(TAG, "onCreate: fullscreenviewactivity position = " + position);

        String piclistJson = i.getStringExtra("picturelist");
        setAttachedBy(i.getStringExtra("attachedBy"));
        List<PictureModel> piclist = new ArrayList<>();

        try {
            JSONArray pictures = new JSONArray(piclistJson);

            Gson gson = new Gson();
            for (int iter = 0; iter < pictures.length(); iter++) {
                JSONObject finalObject = pictures.getJSONObject(iter);
                PictureModel picture = gson.fromJson(finalObject.toString(), PictureModel.class);
                piclist.add(picture);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Inside Pager", "position = " + 0 + " picturelist = " + piclist.get(0).getPic_url());

        //get picturemodel from intent

        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, piclist);

        viewPager.setAdapter(adapter);

        // displaying selected image first
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("pos", viewPager.getCurrentItem());
        setResult(100, intent);
        finish();
    }


}
