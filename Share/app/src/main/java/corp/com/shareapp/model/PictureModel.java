package corp.com.shareapp.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.Serializable;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by randyfournier on 7/28/16.
 */
public class PictureModel implements Serializable{

    private static final String TAG = "PictureModel";

    public PictureModel() {

    }


    private String type; //location or event
    private int pic_id;
    private String pic_url;
    private String pic_thumb_url;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getPic_thumb_url() {
        return pic_thumb_url;
    }

    public void setPic_thumb_url(String pic_thumb_url) {
        this.pic_thumb_url = pic_thumb_url;
    }

    private String username;
    private String caption;
    private String creation_date;
    private String expire_date;
    private int like_count;
    private boolean has_liked;
    private boolean has_reported;
    private int num_comments;
    private int event_id;
    private String event_name;
    private double lon;//location only
    private double lat;//location only
    private String location_name;//location only

    public String getTimePosted() {
        Locale.setDefault(Locale.US);
        TimeZone.setDefault(TimeZone.getTimeZone("America/Los_Angeles"));
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime created = formatter.parseDateTime(getCreation_date());

        DateTime now = new DateTime(DateTimeZone.UTC).withZoneRetainFields(DateTimeZone.forID("America/Los_Angeles"));

        Period p = new Period(created, now, PeriodType.dayTime());

        //Log.d(TAG, String.valueOf(p.getMinutes()));

        if (p.getDays() == 0 && p.getHours() == 0) {
            return String.valueOf(p.getMinutes()) + "m";
        } else if (p.getDays() == 0 && p.getHours() > 0) {
            return String.valueOf(p.getHours()) + "h";
        } else if (p.getDays() > 0) {
            return String.valueOf(p.getDays() + 1) + "d";
        } else {
            return "0m";
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPic_id() {
        return pic_id;
    }

    public void setPic_id(int pic_id) {
        this.pic_id = pic_id;
    }

    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public int getLike_count() {
        return like_count;
    }

    public void setLike_count(int like_count) {
        this.like_count = like_count;
    }

    public boolean getHas_liked() {
        return has_liked;
    }

    public void setHas_liked(boolean has_liked) {
        this.has_liked = has_liked;
    }

    public boolean getHas_reported() {
        return has_reported;
    }

    public void setHas_reported(boolean has_reported) {
        this.has_reported = has_reported;
    }

    public int getNum_comments() {
        return num_comments;
    }

    public void setNum_comments(int num_comments) {
        this.num_comments = num_comments;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return "#" + event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getRemaining() {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime end = formatter.parseDateTime(getExpire_date());

        DateTime now = new DateTime();

        Period p = new Period(now, end, PeriodType.dayTime());

        if (p.getDays() == 0 && p.getHours() == 0) {
            return String.valueOf(p.getMinutes()) + "m";
        } else if (p.getDays() == 0 && p.getHours() > 0) {
            return String.valueOf(p.getHours()) + "h";
        } else if (p.getDays() > 0) {
            return String.valueOf(p.getDays()) + "d";
        } else {
            return "0m";
        }
    }
}