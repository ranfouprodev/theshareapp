package corp.com.shareapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.anjlab.android.iab.v3.SkuDetails;

import java.util.List;

import corp.com.shareapp.R;

/**
 * Created by randyfournier on 7/28/16.
 */
public class SkuListAdapter extends ArrayAdapter {

    Context mContext;

    private List<SkuDetails> skuModelList;
    private int resource;
    private LayoutInflater inflater;

    public SkuListAdapter(Context context, int resource, List<SkuDetails> objects) {
        super(context, resource, objects);
        this.mContext = context;
        skuModelList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sku_list, null);
        }

        TextView tvSku = (TextView) convertView.findViewById(R.id.tvSku);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);

        tvSku.setText(skuModelList.get(position).title.substring(0, skuModelList.get(position).title.length()-15));//trim (The Share App)
        tvPrice.setText(skuModelList.get(position).priceText);

        return convertView;
    }
}
