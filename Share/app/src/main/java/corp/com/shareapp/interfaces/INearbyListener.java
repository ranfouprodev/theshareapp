package corp.com.shareapp.interfaces;

import java.util.List;

import corp.com.shareapp.model.PictureModel;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface INearbyListener {

    void nearbyCompleted(List<PictureModel> piclist);
}
