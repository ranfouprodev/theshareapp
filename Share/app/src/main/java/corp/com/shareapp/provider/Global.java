package corp.com.shareapp.provider;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by randyfournier on 7/6/16.
 */
public class Global extends Application {

    private String host = "http://api.theshareapp.co";

    private String session_id;
    private String username;

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        FacebookSdk.sdkInitialize(this.getApplicationContext()); // facebook login
        AppEventsLogger.activateApp(this);
    }


    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHost() {
        return this.host;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
