package corp.com.shareapp.comment;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import corp.com.shareapp.network.Comments;
import corp.com.shareapp.R;

public class CommentsActivity extends AppCompatActivity {

    private static final String TAG = "CommentsActivity";

    int pic_id =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        Toolbar commentsToolbar = (Toolbar) findViewById(R.id.comments_toolbar);

        commentsToolbar.setTitle("");

        setSupportActionBar(commentsToolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        TextView tx = (TextView) findViewById(R.id.comments_toolbar_title);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/lato_bold.ttf");
        tx.setTypeface(custom_font);


        Bundle b = getIntent().getExtras();

        if(b != null)
            pic_id = b.getInt("pic_id");

        final Comments cm = new Comments(this);

        cm.pictureGet(pic_id, null);

        final EditText etComment = (EditText) findViewById(R.id.tvComment);
        Button btComment = (Button)findViewById(R.id.btComment);

        btComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                String comment = etComment.getText().toString().trim();
                if(pic_id !=0 && !comment.equals("")){
               cm.createComment(comment, pic_id, null);
                    etComment.setText("");
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle b = getIntent().getExtras();
        int pic_id =0;

        if(b != null)
            pic_id = b.getInt("pic_id");

        Log.d(TAG, "pic_id= " + pic_id);

        Comments cm = new Comments(this);
       // cm.pictureGet(pic_id);

        // fix flicker between screen transitions
        overridePendingTransition(0, 0);
    }


}
