package corp.com.shareapp.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.loopj.android.http.AsyncHttpClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import corp.com.shareapp.ExtendEventSuccess;
import corp.com.shareapp.R;
import corp.com.shareapp.camera.PostPhotoActivity;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.model.EventTags;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.profile.ProfileActivity;

/**
 * Created by randyfournier on 8/20/16.
 */
public class EventPictureViewFragment extends Fragment{

    public static final String TAG = "EvtPictureViewFragment";

    AsyncHttpClient client = new AsyncHttpClient();

    //private FragmentActivity mContext;

    private OnGalleryViewCompleteListener mListener;
    private int event_id=0;
    private String event_name = "";
    private String remaining = "";
    private boolean is_owner = true;

    private int selectedPosition;
    Button btEventGalleryExtend ;

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    private boolean lib_allowed;

    private boolean read_only;

    private TextView tvGalleryExpires;

    private Button btEmptyExpires;

    public Button getBtEmptyExpires() {
        return btEmptyExpires;
    }

    public void setBtEmptyExpires(Button btEmptyExpires) {
        this.btEmptyExpires = btEmptyExpires;
    }

    public TextView getTvGalleryExpires() {
        return tvGalleryExpires;
    }

    public void setTvGalleryExpires(TextView tvGalleryExpires) {
        this.tvGalleryExpires = tvGalleryExpires;
    }

    public boolean isRead_only() {
        return read_only;
    }

    public void setRead_only(boolean read_only) {
        this.read_only = read_only;
    }

    public boolean isLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    private String attachedBy = "";

    private boolean new_event;

    public boolean isNew_event() {
        return new_event;
    }

    public void setNew_event(boolean new_event) {
        this.new_event = new_event;
    }

    private boolean subscribed;

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public EventPictureViewFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ExtendEventSuccess event) {

        setRemaining(event.getRemainingTime());
        displayRemaining();
        if(btEventGalleryExtend!=null && btEventGalleryExtend.getTag()!=null){
            EventTags et = (EventTags) btEventGalleryExtend.getTag();
            et.setRemaining(event.getRemainingTime());
        }
    };
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.event_picture_view_fragment, container, false);

        return theView;
    }

    @Override
    public void onResume() {


        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit Events Home");

        Log.d(TAG, "onResume: eventPictureViewFragment");

        Events ev = new Events(getActivity());
        ev.picturesEvent(getEvent_id());

        if(getAttachedBy().equals("gallery") || getAttachedBy().equals("myphotos") || getAttachedBy().equals("popover")){
            ProfileActivity parentActivity = ((ProfileActivity) getActivity());
            setLib_allowed(parentActivity.isLib_allowed());
            setRead_only(parentActivity.isRead_only());
            setSubscribed(parentActivity.isSubscribed());

           ev.events();
            setRemaining(parentActivity.getEventlist().get(selectedPosition).getRemaining());
            displayRemaining();

        }else {

            Log.d(TAG, "onResume: getAttachedBy = " + getAttachedBy());
            if (!isNew_event() && !getAttachedBy().equals("popover")) {
                EventActivity parentActivity = ((EventActivity) getActivity());
                setLib_allowed(parentActivity.isLib_allowed());
                setRead_only(parentActivity.isRead_only());
                setSubscribed(parentActivity.isSubscribed());
            }
        }

        super.onResume();
    }

    public boolean getIs_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {

        //ArrayList<PictureModel> picturelist= new ArrayList<>();

        final Bundle b = getArguments();

        EventTags et = new EventTags();

        if (b != null && b.containsKey("event_id")) {
            setEvent_id(b.getInt("event_id"));
            et.setEvent_id(b.getInt("event_id"));

            setEvent_name(b.getString("event_name"));
            et.setEvent_name(b.getString("event_name"));

            setAttachedBy(b.getString("attachedBy"));

            setRead_only(b.getBoolean("read_only"));
            et.setRead_only(b.getBoolean("read_only"));

            setSelectedPosition(b.getInt("position"));

            if(!getAttachedBy().equals("search")){
                ProfileActivity profile = ((ProfileActivity) getActivity());
                profile.setEvent_id(getEvent_id());
            }else{
                EventActivity eventtab = ((EventActivity) getActivity());
                eventtab.setRead_only(isRead_only());
                eventtab.setEvent_id(getEvent_id());
            }


            Log.d(TAG, "onViewCreated: attachedBy = " + getAttachedBy());

            setRemaining(b.getString("remaining"));
            et.setRemaining(b.getString("remaining"));

           switch (getAttachedBy()) {
                case "gallery":
                case "myphotos":
                case "popover":
                    ProfileActivity profile = ((ProfileActivity) getActivity());
                    profile.setRemaining(getRemaining());
                    break;
                case "search":

                    EventActivity search = ((EventActivity) getActivity());
                    search.setRemaining(getRemaining());

                    if(getRemaining().equals("")) {
                        search.setRemaining("30d");
                    }

                    break;
            }

            setIs_owner(b.getBoolean("is_owner"));

            setLib_allowed(b.getBoolean("lib_allowed"));

            setSubscribed(b.getBoolean("is_subscribed"));

            setNew_event(b.getBoolean("new_event"));
        }

        setTvGalleryExpires((TextView)view.findViewById(R.id.tvGalleryExpires));

        btEventGalleryExtend = (Button)view.findViewById(R.id.btEventGalleryExtend);

        setBtEmptyExpires((Button)view.findViewById(R.id.btEmptyExpires));


        et.setEvent_id(getEvent_id());
        btEmptyExpires.setTag(et);
        btEventGalleryExtend.setTag(et);

        if(is_owner){
            btEventGalleryExtend.setVisibility(View.VISIBLE);
        }else{
            btEventGalleryExtend.setVisibility(View.GONE);
        }

        /*if (getRemaining() != null && getRemaining().length() > 0 && getRemaining().charAt(getRemaining().length() - 1) == 'd') {
            setRemaining(getRemaining().substring(0, getRemaining().length() - 1) + " days");
        }*/

        displayRemaining();

        //final String event_name = "#test";
        final TextView tvTitle = (TextView) view.findViewById(R.id.tvEventGalleryTitle);
        tvTitle.setText(getEvent_name());

        //Log.d(TAG, "Is_owner= " + is_owner);
        //Log.d(TAG,"position = " + position);
        //Log.d(TAG, "event_name = " + event_name);

        ImageView ivClose = (ImageView) view.findViewById(R.id.ivEventGalleryBack);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(attachedBy.equals("popover")){
                    EventTabFragment fragment = new EventTabFragment();
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment, "sf")
                            .addToBackStack(null)
                            .commit();
                }else {
                    if(attachedBy.equals("search")){
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack();
                        fm.popBackStack();
                    }else{
                        FragmentManager fm = getFragmentManager();
                        fm.popBackStack();
                    }
                }
            }
        });

        ImageView ivAddPicture = (ImageView) view.findViewById(R.id.ivEventGalleryAddPicture);
        ivAddPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "onClick: lib_allowed = " + isLib_allowed());
                Log.d(TAG, "onClick: is_owner = " + is_owner);
                Log.d(TAG, "onClick: read_only = " + read_only);

                switch (getAttachedBy()) {
                    case"search":
                        EventActivity search = ((EventActivity) getActivity());
                    setRead_only(search.isRead_only());
                        break;

                    case"gallery":
                        ProfileActivity profile = ((ProfileActivity) getActivity());
                        setRead_only(profile.isRead_only());
                        break;
                }

                if(!read_only || is_owner) {

                    if (!isLib_allowed()) {
                        Bundle bundle = new Bundle();
                        bundle.putString("event_name", getEvent_name());
                        bundle.putInt("event_id", getEvent_id());
                        bundle.putString("choice", "camera");

                        Intent i = new Intent(getActivity(), PostPhotoActivity.class);
                        i.putExtras(bundle);
                        startActivity(i);

                    } else {

                        final Dialog dialog = new Dialog(getActivity());
                        dialog.setContentView(R.layout.chooser_dialog);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.show();
                        TextView tvCaption = (TextView) dialog.findViewById(R.id.tvNetworkDialogCaption);

                        //tvCaption.setText(StringUtils.capitalize(op_status.toLowerCase()));

                        //TextView tvMessage = (TextView)dialog.findViewById(R.id.tvNetworkDialogMessage);
                        //tvMessage.setText(message);

                        TextView tv_OK = (TextView) dialog.findViewById(R.id.tvNetworkDialogOK);
                        tv_OK.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        //Log.d(TAG, "event_name = " + event_name);
                        //Log.d(TAG, "event_name = " + getEvent_name());

                        Button btChooserCamera = (Button) dialog.findViewById(R.id.btChooserCamera);
                        btChooserCamera.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //dialog.dismiss();
                                // pass into a bundle to be read from postphoto activity
                                // event_id and event name
                                Bundle bundle = new Bundle();
                                bundle.putString("event_name", getEvent_name());
                                bundle.putInt("event_id", getEvent_id());
                                bundle.putString("choice", "camera");
                                dialog.dismiss();

                                Intent i = new Intent(getActivity(), PostPhotoActivity.class);
                                i.putExtras(bundle);
                                startActivity(i);
                            }
                        });

                        Button btChooserGallery = (Button) dialog.findViewById(R.id.btChooserGallery);
                        btChooserGallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
                                logger.logEvent("Visit Library");

                                //dialog.dismiss();
                                Bundle bundle = new Bundle();
                                bundle.putString("event_name", getEvent_name());
                                bundle.putInt("event_id", getEvent_id());
                                bundle.putString("choice", "gallery");
                                dialog.dismiss();

                                Intent i = new Intent(getActivity(), PostPhotoActivity.class);
                                i.putExtras(bundle);
                                startActivity(i);
                            }
                        });
                    }
                }else{
                    NetworkUtils nu = new NetworkUtils(getActivity());

                    //network dialog used as an info dialog
                    nu.networkDialog("Read Only","The owner of " + event_name + " has made this event read only. Only the owner may post to this event.");
                }
            }
        });

        GridView gvClicker = (GridView) view.findViewById(R.id.gvEventPhotos);
        gvClicker.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle args = new Bundle();
                args.putString("attachedBy", getAttachedBy());
                args.putString("event_name", tvTitle.getText().toString());
                //args.putString("event_name", "");
                args.putInt("position", position);

                DetailCardFragment frag = new DetailCardFragment();
                frag.setArguments(args);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, frag, "dcf")
                            .addToBackStack(null)
                            .commit();

            }
        });



        final SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerEventGallery);

        final ImageView ivEventGalleryMore = (ImageView) view.findViewById(R.id.ivEventGalleryMore);
        ivEventGalleryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (getAttachedBy()) {

                    case "search":

                        EventActivity search = ((EventActivity) getActivity());
                        search.showEventMore(ivEventGalleryMore);
                        search.setEvent_id(getEvent_id());
                        search.setEvent_name(getEvent_name());
                        search.setRemaining(getRemaining());
                        search.setIs_owner(getIs_owner());
                        search.setAttachedBy(getAttachedBy());
                        search.setLib_allowed(isLib_allowed());
                        search.setSubscribed(isSubscribed());

                        break;

                    case "gallery":
                    case "myphotos":
                    case "popover":
                        ProfileActivity profile = ((ProfileActivity) getActivity());
                        profile.showEventMore(ivEventGalleryMore);
                        profile.setEvent_id(getEvent_id());
                        profile.setEvent_name(getEvent_name());
                        profile.setRemaining(getRemaining());
                        profile.setIs_owner(getIs_owner());
                        profile.setAttachedBy(getAttachedBy());
                        profile.setLib_allowed(isLib_allowed());
                        profile.setRead_only(isRead_only());
                        profile.setSubscribed(isSubscribed());

                       break;
                }
            }
        });

            this.mListener = (OnGalleryViewCompleteListener) getActivity();
            this.mListener.onGalleryViewComplete(swipeContainer, getEvent_id());
    }

    public static interface OnGalleryViewCompleteListener {
        public abstract void onGalleryViewComplete(SwipeRefreshLayout swipeContainer, int event_id);
    }

    public void displayRemaining(){

        Log.d(TAG, "displayRemaining: " + getRemaining());
        
        String duration="";
        char lastChar = getRemaining().charAt(getRemaining().length() - 1);
        switch(lastChar){
            case 'd':
                duration = "days";
                break;

            case 'h':
                duration = "hours";
                break;

            case 'm':
                duration = "minutes";
                break;
        }

        String expiresMessage = getRemaining().substring(0, getRemaining().length()-1) + " " + duration + ".";

        if(btEmptyExpires!=null){
            btEmptyExpires.setText("Event expires in " + expiresMessage);
        }
        if(tvGalleryExpires!=null){
            tvGalleryExpires.setText("Expires in " + expiresMessage);
        }
    }
}
