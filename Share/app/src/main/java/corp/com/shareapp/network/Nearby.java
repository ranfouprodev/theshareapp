package corp.com.shareapp.network;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import corp.com.shareapp.R;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.interfaces.IUploadListener;
import corp.com.shareapp.interfaces.INearbyListener;
import corp.com.shareapp.location.CheckLocationActivity;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.user.SignupActivity;

import static java.util.Locale.getDefault;

/**
 * Created by randyfournier on 7/28/16.
 */
public class Nearby extends ContextWrapper implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /**
     * this class uses the GoogleApiClient to get lat lon so
     *  some api calls in user like checkLocation and futureUser get called in the connect callback
     *
     *
     */

    private static final String TAG = "Nearby";

    AsyncHttpClient client = new AsyncHttpClient();

    // no ssl workaround -- turns off ssl check for testing purposes is cert expired
    /*AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);*/

    private GoogleApiClient mGoogleApiClient;
    private SVProgressHUD mSVProgressHUD;
    String callingMethod = "get"; //switch/case in GoogleApiClient.connect:get=getNearby || post=post to Nearby etc...
    IUploadListener uploadCompleted;
    INearbyListener nearbyListener;
    private int page_number = 1;

    private List<PictureModel> piclist = new ArrayList<>();

    public List<PictureModel> getPiclist() {
        return piclist;
    }

    public void setPiclist(List<PictureModel> piclist) {
        this.piclist = piclist;
    }

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public File getPicture() {
        return picture;
    }

    public void setPicture(File picture) {
        this.picture = picture;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    File picture = null;
    String caption = "";

    private String email;
    private String registration_id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistration_id() {
        return registration_id;
    }

    public void setRegistration_id(String registration_id) {
        this.registration_id = registration_id;
    }

    public Nearby(Context base) {
        super(base);

        mSVProgressHUD = new SVProgressHUD(getBaseContext());

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    public void picturesNearby() {

        callingMethod = "get"; // in google location services onConnected callback we need a flag to indicate who is calling to get the lat/lon
        mGoogleApiClient.connect();

    }

    public void checkLocation() {

        callingMethod = "check"; // in google location services onConnected callback we need a flag to indicate who is calling to get the lat/lon
        mGoogleApiClient.connect();

    }

    public void futureUser(String email, String registration_id) {

        callingMethod = "future"; // in google location services onConnected callback we need a flag to indicate who is calling to get the lat/lon

        setEmail(email); // setter so we can get this passed in email in onConnected callback
        setRegistration_id(registration_id); // same as above

        mGoogleApiClient.connect();
    }

    private void JSONPicturesNearby(JSONObject res) {
        //Log.d(TAG, res.toString());
        NetworkUtils nu = new NetworkUtils(this);

        int count;

        // check for critical error -- when a session_id is dead we need to fetch a new one with a login
        try {
            if (res.has("error_message")) {
                if (res.getString("error_level").equals("critical")) {
                    nu.criticalError("Error", res.getString("error_message")); // critical error will take you to login activity respectively
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (res.getString("op_status").equals("success")) {

                try {
                    JSONObject dataObject = res.getJSONObject("data");
                    JSONArray parentArray = dataObject.getJSONArray("pictures");

                    count =  Integer.parseInt(dataObject.getString("count"));
                    setPage_number(Integer.parseInt(dataObject.getString("page_number")));

                    Gson gson = new Gson();
                    for (int i = 0; i < parentArray.length(); i++) {
                        JSONObject finalObject = parentArray.getJSONObject(i);
                        PictureModel picture = gson.fromJson(finalObject.toString(), PictureModel.class);
                        getPiclist().add(picture);
                    }

                    Activity activity = (Activity) getBaseContext();

                    nearbyListener = (INearbyListener) activity;
                    nearbyListener.nearbyCompleted(piclist); // 2nd param is for refresh and return to last cell in recyclerview

                    if(count == 20){ // if there are more images, then refetch
                        setPage_number(getPage_number() + 1);

                        picturesNearby();

                    }else {
                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);

                        boolean tokenUploaded = sharedprefs.getBoolean("tokenUploaded", false);

                        //if (!tokenUploaded){ // has firebase token been registered with share api yet?
                            Users us = new Users(getBaseContext());
                        us.updateRegId(); // register our firebase token with share api after getting our complete list of nearby pics
                    //}

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void picturesNearbyCreate(File picture, String caption) throws IOException {

        setPicture(picture); // need to get from inside onConnected callback
        setCaption(caption); // need to get from inside onConnected callback

        callingMethod = "post"; // in google location services onConnected callback we need a flag to indicate who is calling to get the lat/lon

        mGoogleApiClient.connect();
    }

    public void picturesDelete(int pic_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("pic_id", pic_id);

            client.post(getString(R.string.host) + getString(R.string.rt_pictures_delete), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    //Log.d(TAG, res.toString());

                    picturesNearby(); // do a refresh of nearby list after deleting the picture
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {

            //Log.d(TAG,String.valueOf(mLastLocation.getLatitude()));

            double lat = mLastLocation.getLatitude();

            //Log.d(TAG,String.valueOf(mLastLocation.getLongitude()));

            double lon = mLastLocation.getLongitude();

            mGoogleApiClient.disconnect();

            final NetworkUtils nu = new NetworkUtils(this);

            switch (callingMethod) {
                case "get": //get nearby
                    //check for connection
                    if (nu.isOnline()) {

                        String count = "20";

                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        String session_id = sharedprefs.getString("session_id", "");
                        int radius = sharedprefs.getInt("radius", 6);
                        String sorting_option = sharedprefs.getString("sorting_option", "date");
                        //Log.d(TAG, "sorting_option= " + sorting_option);
                        //Log.d(TAG, "session_id= " + session_id);

                        //uncomment lat and lon for emulator
                        //lon = 98.973944000;
                        //lat = 18.738371000;

                        float[] radiusVals = {0.5f, 1, 2, 5, 10, 25, 50};

                        //Log.d(TAG, "radius= " + radiusVals[radius]);

                        RequestParams params = new RequestParams();
                        params.put("lon", lon);
                        params.put("lat", lat);
                        params.put("radius", radiusVals[radius]);
                        params.put("count", count);
                        params.put("sorting_option", sorting_option);
                        params.put("session_id", session_id);

                        Log.d(TAG, "onConnected: in Nearby session_id = " + session_id);

                        params.put("page_number", getPage_number());

                        client.get(getString(R.string.host) + getString(R.string.rt_pictures_nearby), params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                                try {
                                    JSONPicturesNearby(res);
                                }catch (Exception e)
                                {

                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                //Log.d(TAG, "res = " + res);
                                nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                            }
                        });

                    } else {
                        nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
                    }
                    break;

                case "post": //post to Nearby
                    if (nu.isOnline()) {

                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        String session_id = sharedprefs.getString("session_id", "");

                        //double lon=98.973944000;
                        //double lat=18.738371000;

                        //reverse Geocoding
                        Geocoder geocoder = new Geocoder(this, getDefault());

                        List<Address> addresses = null;

                        try {
                            addresses = geocoder.getFromLocation(lat, lon, 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Address address = addresses.get(0);

                        //Log.d(TAG, "geocoder= "+ address.getAddressLine(0));

                        String trimmedAddress = address.getAddressLine(0).trim();

                        trimmedAddress = trimmedAddress.replaceAll("([0-9])(/*)(#*)", ""); // remove from address

                        RequestParams params = new RequestParams();

                        File picture = getPicture();

                        try {
                            params.put("picture", picture, "image/png");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        params.put("caption", getCaption());
                        params.put("lon", lon);
                        params.put("lat", lat);
                        params.put("location_name", trimmedAddress);
                        params.put("session_id", session_id);

                        mSVProgressHUD.show();

                        client.post(getString(R.string.host) + getString(R.string.rt_pictures_nearby_create), params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                                Log.d(TAG, "picturesNearbyCreate:OnSuccess  res = " + res.toString());

                                mSVProgressHUD.dismiss();

                                boolean deleted = getPicture().delete();

                                setPicture(null);

                                //Log.d(TAG, "deleted= " + String.valueOf(deleted));

                                Activity activity = (Activity) getBaseContext();
                                uploadCompleted = (IUploadListener) activity;
                                uploadCompleted.Uploaded(0);


                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                Log.d(TAG, "picturesNearbyCreate:OnFailure res = " + res);
                                mSVProgressHUD.dismiss();

                                nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                            }
                        });

                    } else {
                        mSVProgressHUD.dismiss();
                        nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
                    }
                    break;

                case "check": //checkLocation

                    if (nu.isOnline()) {

                        //allow signup from here

                        //lat = 34.05;
                        //lon = -118.24;

                        // thailand hard coded
                        //lat = 18.7382254;
                        //lon = 98.9738213;

                        Log.d(TAG, "onConnected: lat = " + lat);
                        Log.d(TAG, "onConnected: lon = " + lon);

                        RequestParams params = new RequestParams();
                        params.put("lat", lat);
                        params.put("lon", lon);

                        client.get(getString(R.string.host) + getString(R.string.rt_checkLocation), params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                                Log.d(TAG, res.toString());
                                try {
                                    if(res.getInt("count")==0){
                                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                                        boolean visited = sharedprefs.getBoolean("visitedCheckLocation",false);
                                        if(!visited) {
                                            Intent i = new Intent(getBaseContext(), CheckLocationActivity.class);
                                            startActivity(i);
                                        }
                                    }else{
                                        Intent i = new Intent(getBaseContext(), SignupActivity.class);
                                        i.addFlags(i.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(i);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                Log.d(TAG, "res = " + res);

                                nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                            }
                        });

                    } else {
                        nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
                    }

                    break;

                case "future":

                    if (nu.isOnline()) {

                        //dummy lat/lon
                        //lat = -118.24
                        //lon = 34.05

                        RequestParams params = new RequestParams();
                        params.put("email", getEmail());
                        params.put("lat", lat);
                        params.put("lon", lon);
                        params.put("registration_id", getRegistration_id());

                        client.post(getString(R.string.host) + getString(R.string.rt_futureUser), params, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                                Log.d(TAG, res.toString());
                                nu.networkDialog("Success", "Thanks for signing up! We'll keep you updated.");


                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                Log.d(TAG, "res = " + res);

                                nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                            }
                        });

                    } else {
                        nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
                    }

                    break;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
