package corp.com.shareapp.interfaces;

import java.util.ArrayList;

import corp.com.shareapp.model.PictureModel;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface IMyPhotosListener {
     void MyPhotosList(ArrayList<PictureModel> piclist);
}
