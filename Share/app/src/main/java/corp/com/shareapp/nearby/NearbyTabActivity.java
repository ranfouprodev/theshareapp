package corp.com.shareapp.nearby;

import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.actionsheet.ActionSheet;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.appevents.AppEventsLogger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.NearbyRecyclerViewAdapter;
import corp.com.shareapp.camera.CameraHelper;
import corp.com.shareapp.fragment.CommentsFragment;
import corp.com.shareapp.fragment.NearbyCardFragment;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.interfaces.ILikeListener;
import corp.com.shareapp.interfaces.INearbyListener;
import corp.com.shareapp.interfaces.IReportListener;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PictureTags;
import corp.com.shareapp.network.Nearby;
import corp.com.shareapp.network.Users;

public class NearbyTabActivity extends AppCompatActivity implements
        ActionSheet.ActionSheetListener, INearbyListener, ILikeListener, IReportListener {

    private static final String TAG = "NearbyTabActivity";
    private static int position;
    private static int comment_count;
    private static int selectedCardIndex = -1;
    ActionSheet as;
    NearbyRecyclerViewAdapter adapter;
    String pic_url = "";
    String username;
    boolean setReportUnreportMode = false;
    SwipeRefreshLayout swipeContainer;
    boolean reportedState;
    boolean reportStateFetched;
    int addedComments = 0;
    private int pic_id;
    private boolean actionsheetShowing;
    private List<PictureModel> nearbyList;
    private ImageView ivLike;
    private ListView lvNearby = null;
    private RecyclerView rvNearby = null;
    private int top = 0;
    private int previousPicId;

    public static int getPosition() {
        return position;
    }

    public static void setPosition(int position) {
        NearbyTabActivity.position = position;
    }

    public static int getComment_count() {
        return comment_count;
    }

    public static void setComment_count(int comment_count) {
        NearbyTabActivity.comment_count = comment_count;
    }

    public static int getSelectedCardIndex() {
        return selectedCardIndex;
    }

    public static void setSelectedCardIndex(int selectedCardIndex) {
        NearbyTabActivity.selectedCardIndex = selectedCardIndex;
    }

    public List<PictureModel> getNearbyList() {
        return nearbyList;
    }

    public void setNearbyList(List<PictureModel> nearbyList) {
        if (nearbyList != null) {
            this.nearbyList.clear();
            this.nearbyList.addAll(nearbyList);
        }
    }

    public boolean isActionsheetShowing() {
        return actionsheetShowing;
    }

    public void setActionsheetShowing(boolean actionsheetShowing) {
        this.actionsheetShowing = actionsheetShowing;
    }

    public ImageView getIvLike() {
        return ivLike;
    }

    public void setIvLike(ImageView ivLike) {
        this.ivLike = ivLike;
    }

    public RecyclerView getRvNearby() {
        return rvNearby;
    }

    public void setRvNearby(RecyclerView rvNearby) {
        this.rvNearby = rvNearby;
    }

    public ListView getLvNearby() {
        return lvNearby;
    }

    public void setLvNearby(ListView lvNearby) {
        this.lvNearby = lvNearby;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getPreviousPicId() {
        return previousPicId;
    }

    public void setPreviousPicId(int previousPicId) {
        this.previousPicId = previousPicId;
    }

    public boolean isReportStateFetched() {
        return reportStateFetched;
    }

    public void setReportStateFetched(boolean reportStateFetched) {
        this.reportStateFetched = reportStateFetched;
    }

    public int getAddedComments() {
        return addedComments;
    }

    public void setAddedComments(int addedComments) {
        this.addedComments = addedComments;
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_layout);

        selectedCardIndex = -1;

        if (ContextCompat.checkSelfPermission(NearbyTabActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(NearbyTabActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(NearbyTabActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        10);

            }
        }

        nearbyList = new ArrayList<PictureModel>();
        adapter = new NearbyRecyclerViewAdapter(NearbyTabActivity.this, nearbyList);

        NearbyCardFragment frag = new NearbyCardFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.nearby_layout, frag)
                .commit();

        //replaceFragment(frag);

    }

    public void showPopup(View v) {

        PictureTags myTag = (PictureTags) v.getTag();
        Log.d(TAG, "ellipsis clicked" + " pic_id= " + myTag.getImg_id());
        Log.d(TAG, "Pic_url= " + myTag.getUrl());
        //reportStateFetched=false;

        //set pic_id
        pic_id = myTag.getImg_id();

        if (previousPicId != pic_id) {
            reportStateFetched = false;
            previousPicId = pic_id;
        }

        position = myTag.getPosition();
        username = myTag.getUsername();
        pic_url = myTag.getUrl();

        if (!reportStateFetched) {
            reportedState = myTag.getHas_reported();
            reportStateFetched = true;
        }

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String currentUsername = sharedprefs.getString("username", "");


        setTheme(R.style.ActionSheetStyleiOS7);

        if (username.equals(currentUsername)) {
            //create picturesDelete dialog
            setReportUnreportMode = false;
            as = ActionSheet.createBuilder(this, getSupportFragmentManager())
                    .setCancelButtonTitle("Cancel")
                    .setOtherButtonTitles("Download", "Delete")
                    .setCancelableOnTouchOutside(true)
                    .setListener(this).show();
            setActionsheetShowing(true);
        } else {
            //create picturesReport/unreport dialog
            setReportUnreportMode = true;

            if (reportedState) {
                as = ActionSheet.createBuilder(this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Download", "Unreport")
                        .setCancelableOnTouchOutside(true)
                        .setListener(this).show();
                setActionsheetShowing(true);
            } else {
                as = ActionSheet.createBuilder(this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Download", "Report")
                        .setCancelableOnTouchOutside(true)
                        .setListener(this).show();
                setActionsheetShowing(true);
            }

        }
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
        actionSheet.dismiss();
        setActionsheetShowing(false);
        //reportStateFetched =false;
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        setActionsheetShowing(false);
        //build dialog
        if (index == 1 && !setReportUnreportMode) {
            final Dialog dialog = new Dialog(NearbyTabActivity.this);
            dialog.setContentView(R.layout.delete_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            TextView btnDelete = (TextView) dialog.findViewById(R.id.btnDelete);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call picturesDelete routine with pic_id
                    deletePic();
                    dialog.dismiss();
                    //refresh the listview
                }
            });


        } else if (index == 1) {

            //do picturesReport
            final Dialog dialog = new Dialog(NearbyTabActivity.this);

            if (reportedState) {
                dialog.setContentView(R.layout.unreport_dialog);
            } else {
                dialog.setContentView(R.layout.report_dialog);
            }

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView btnReport = (TextView) dialog.findViewById(R.id.btnReport);
            btnReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call picturesDelete routine with pic_id
                    reportPic();
                    dialog.dismiss();
                    //refresh the listview
                }
            });
        }

        // download to device
        if (index == 0) {
            //Download
            Log.d(TAG, pic_url);
            Glide.with(this)
                    .load(pic_url)
                    .asBitmap()
                    .toBytes(Bitmap.CompressFormat.JPEG, 80)
                    .into(new SimpleTarget<byte[]>() {
                        @Override
                        public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                            new AsyncTask<Void, Void, File>() {

                                @Override
                                protected File doInBackground(Void... params) {
                                    File photoDirectory = CameraHelper.getPhotoDirectory();
                                    Log.d(TAG, photoDirectory.getPath());
                                    File file = new File(photoDirectory + "/" + CameraHelper.getTimeStampFilename());
                                    File dir = file.getParentFile();
                                    try {
                                        if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                                            throw new IOException("Cannot ensure parent directory for file " + file);
                                        }
                                        BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                                        s.write(resource);
                                        s.flush();
                                        s.close();
                                        return file;

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(File file) {
                                    NetworkUtils nu = new NetworkUtils(NearbyTabActivity.this);
                                    nu.downloadDialog();

                                    //gallery scanner
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        Intent mediaScanIntent = new Intent(
                                                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                        Uri contentUri = Uri.fromFile(file);
                                        mediaScanIntent.setData(contentUri);
                                        NearbyTabActivity.this.sendBroadcast(mediaScanIntent);
                                    } else {
                                        sendBroadcast(new Intent(
                                                Intent.ACTION_MEDIA_MOUNTED,
                                                Uri.parse("file://"
                                                        + Environment.getExternalStorageDirectory())));
                                    }

                                }

                            }.execute();

                        }
                    });
        }
    }

    private void deletePic() {

        //Log.d(TAG, "pic_id= " + pic_id + " position= " + position);

        Nearby nm = new Nearby(this);
        nm.picturesDelete(pic_id);
    }

    private void reportPic() {

        //Log.d(TAG, "pic_id= " + pic_id + " position= " + position);

        Users um = new Users(this);
        um.picturesReport(pic_id);
    }

    public void onClickFilterAndSort(View v) {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("Visit Nearby Filter");
        Intent i = new Intent(NearbyTabActivity.this, NearbyFilterAndSortActivity.class);
        startActivity(i);

    }

    public void onClickToComments(View v) {

        PictureTags pic_id = (PictureTags) v.getTag();
        Log.d(TAG, "pic_id= " + pic_id.getImg_id());

        /*Intent i = new Intent(NearbyTabActivity.this, CommentsActivity.class);
        i.putExtra("pic_id", pic_id.getImg_id());
        startActivity(i);*/

        Log.d(TAG, "clicked");

        Bundle b = new Bundle();
        b.putInt("pic_id", pic_id.getImg_id());
        b.putString("attachedBy", "nearby");
        b.putInt("numComments", pic_id.getNumComments());
        b.putInt("position", pic_id.getPosition());
        //setSelectedCardIndex(pic_id.getPosition());

        //Log.d(TAG, "onClickToComments: position = " + pic_id.getPosition());

        CommentsFragment frag = new CommentsFragment();
        frag.setArguments(b);

     /*   getFragmentManager().beginTransaction()
                .add(R.id.nearby_layout, frag, "cf")
                .addToBackStack(null)
                .commit();


*/
        replaceFragment(frag);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(R.id.nearby_layout, fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }


    @Override
    public void onBackPressed() {

        if (actionsheetShowing) {
            as.dismiss();
            setActionsheetShowing(false);
        } else {


            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish();
            } else {
                super.onBackPressed();
            }
        }


    }

    @Override
    public void nearbyCompleted(List<PictureModel> piclist) {

        //setSelectedCardIndex(pos);

        setNearbyList(piclist);

        if (rvNearby == null) {
            rvNearby = (RecyclerView) findViewById(R.id.rvNearby);
            rvNearby.setLayoutManager(new LinearLayoutManager(NearbyTabActivity.this));
            rvNearby.setAdapter(adapter);
        }

        if (rvNearby.getAdapter() != null) {


         rvNearby.getAdapter().notifyDataSetChanged();

            LinearLayoutManager layoutManager = (LinearLayoutManager) rvNearby.getLayoutManager();
            layoutManager.scrollToPositionWithOffset(NearbyTabActivity.selectedCardIndex, 0);

            ImageView ivNearbyEmpty = (ImageView) findViewById(R.id.ivNearbyEmpty);

            if (piclist.size() == 0) {
                //getLvNearby().setVisibility(View.INVISIBLE);
                rvNearby.setVisibility(View.INVISIBLE);
                ivNearbyEmpty.setVisibility(View.VISIBLE);

            } else {
                //if(getLvNearby()!=null) {
                if (rvNearby != null) {
                    //getLvNearby().setVisibility(View.VISIBLE);
                    rvNearby.setVisibility(View.VISIBLE);
                    ivNearbyEmpty.setVisibility(View.GONE);

                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("Visit Nearby");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        int pos = data.getIntExtra("pos", 0);
        //int commentCount = data.getIntExtra("commentCount", 0);
        //rvNearby.scrollToPosition(pos);
        LinearLayoutManager layoutManager = (LinearLayoutManager) rvNearby.getLayoutManager();
        layoutManager.scrollToPositionWithOffset(pos, 0);
        setSelectedCardIndex(pos);
        Log.d(TAG, "onActivityResult: selectedcardIndex = " + selectedCardIndex);
        //Nearby nm = new Nearby(NearbyTabActivity.this);
       // nm.picturesNearby(); // new Nearby fetch

        //reset no further pull
        //setSelectedCardIndex(-1);

    }

    @Override
    public void setLike(String liked, TextView tvLikes) {
        Log.d(TAG, "setLike: liked = " + liked);

        //fetch textbox value to an int
        int likes = Integer.parseInt(tvLikes.getText().toString());

        switch (liked) {
            case "like":
                likes++;
                tvLikes.setText(String.valueOf(likes));

                break;

            case "unlike":
                likes--;
                if (likes < 0)
                    likes = 0;

                tvLikes.setText(String.valueOf(likes));
                break;
        }
    }

    @Override
    public void setReported(String reported) {
        reportedState = false;

        if (reported.equals("report")) {
            reportedState = true;
        }
    }
}
