package corp.com.shareapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.model.PictureModel;

/**
 * Created by randyfournier on 7/28/16.
 *
 * This adapter is used for both the 3 column My Photos grid and the 2 Column EventPictureView grid
 */
public class ProfilePhotosGridViewAdapter extends ArrayAdapter {

    private static final String TAG = "ProfilePhotosGridAdapter";

    private Context mContext;

    private List<PictureModel> photosList;
    private int resource;
    private LayoutInflater inflater;
    private int columns;
    private boolean multi = false;

    public ProfilePhotosGridViewAdapter(Context context, int resource, List<PictureModel> objects, int columns, boolean multi) {
        super(context, resource, objects);
        this.mContext = context;
        photosList = objects;
        this.resource = resource;
        this.columns = columns;
        this.multi = multi;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = inflater.inflate(resource, null);

            holder.checkbox = (CheckBox)convertView.findViewById(R.id.cbSelect);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.checkbox.setId(position);

        corp.com.shareapp.helper.SquareImageView ivPreviewPic = (corp.com.shareapp.helper.SquareImageView) convertView.findViewById(R.id.ivGridPhoto);




        //float thumbsize = (100/(float)columns) * 0.01f;

        //Log.d(TAG, "getView: " + photosList.get(position).getPic_thumb_url());

        Glide.with(mContext).load(photosList.get(position).getPic_thumb_url())
                .asBitmap()
                .placeholder(R.drawable.loading_spinner)
                .error(R.drawable.network_error)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                //.thumbnail(thumbsize)
                .centerCrop()
                .into(ivPreviewPic);

        // get reference to checkbox


        if(multi) {
            holder.checkbox.setVisibility(View.VISIBLE);

            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;

                    int id = cb.getId();

                    if(photosList.get(id).isSelected()){
                        cb.setChecked(false);
                        photosList.get(id).setSelected(false);
                    }else{
                        cb.setChecked(true);
                        photosList.get(id).setSelected(true);
                    }
                }
            });

            holder.checkbox.setChecked(photosList.get(position).isSelected());
            holder.id = position;
        }

        return convertView;
    }

    class ViewHolder {
        CheckBox checkbox;
        int id;
    }
}
