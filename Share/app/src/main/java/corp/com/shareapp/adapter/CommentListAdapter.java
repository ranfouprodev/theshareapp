package corp.com.shareapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.model.CommentModel;

/**
 * Created by randyfournier on 7/28/16.
 */
public class CommentListAdapter extends ArrayAdapter {

    Context mContext;

    private List<CommentModel> commentModelList;
    private int resource;
    private LayoutInflater inflater;

    public CommentListAdapter(Context context, int resource, List<CommentModel> objects) {
        super(context, resource, objects);
        this.mContext = context;
        commentModelList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.comment_list, null);
        }

        TextView tvCommentUsername = (TextView) convertView.findViewById(R.id.tvCommentUsername);
        TextView tvCommentCreated = (TextView) convertView.findViewById(R.id.tvCommentCreated);
        TextView tvCommenttext = (TextView) convertView.findViewById(R.id.tvCommentText);

        tvCommentUsername.setText(commentModelList.get(position).getUsername());
        tvCommentCreated.setText(commentModelList.get(position).getSent());
        tvCommenttext.setText(commentModelList.get(position).getComment_text());

        return convertView;
    }
}
