package corp.com.shareapp.profile;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.baoyz.actionsheet.ActionSheet;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.ExtendEventSuccess;
import corp.com.shareapp.R;
import corp.com.shareapp.Utility;
import corp.com.shareapp.adapter.DetailCardPagerAdapter;
import corp.com.shareapp.adapter.EventListAdapter;
import corp.com.shareapp.adapter.EventSearchListAdapter;
import corp.com.shareapp.adapter.ProfilePhotosGridViewAdapter;
import corp.com.shareapp.camera.CameraHelper;
import corp.com.shareapp.comment.CommentsActivity;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.event.EventFilterAndSortActivity;
import corp.com.shareapp.fragment.CreateEventFragment;
import corp.com.shareapp.fragment.DetailCardFragment;
import corp.com.shareapp.fragment.EventPictureViewFragment;
import corp.com.shareapp.fragment.EventSettingsFragment;
import corp.com.shareapp.fragment.ExtendEventFragment;
import corp.com.shareapp.fragment.ProfileEventsFragment;
import corp.com.shareapp.fragment.ProfilePhotosFragment;
import corp.com.shareapp.fragment.SettingsFragment;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.interfaces.ApiCallback;
import corp.com.shareapp.interfaces.EventGalleryListener;
import corp.com.shareapp.interfaces.IEventButtonListener;
import corp.com.shareapp.interfaces.IEventsListener;
import corp.com.shareapp.interfaces.ILikeListener;
import corp.com.shareapp.interfaces.IMyPhotosListener;
import corp.com.shareapp.interfaces.IPhotosListener;
import corp.com.shareapp.interfaces.ISearchListener;
import corp.com.shareapp.model.EventModel;
import corp.com.shareapp.model.EventTags;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PictureTags;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.network.Users;
import cz.msebera.android.httpclient.Header;

import static android.view.View.VISIBLE;

public class ProfileActivity extends AppCompatActivity implements
        BillingProcessor.IBillingHandler, ActionSheet.ActionSheetListener, IPhotosListener, IEventsListener, EventGalleryListener, EventPictureViewFragment.OnGalleryViewCompleteListener, ILikeListener, IEventButtonListener, ISearchListener, LoaderManager.LoaderCallbacks<File> {

    private SVProgressHUD mSVProgressHUD;
    private DetailCardPagerAdapter detailCardViewPagerAdapter;

    public BillingProcessor getBp() {
        return bp;
    }

    public void setBp(BillingProcessor bp) {
        this.bp = bp;
    }

    public BillingProcessor bp;


    private static final String TAG = "ProfileActivity";
    int isReport = 0;
    int pic_id;
    private static int comment_count=-1;

    public static int getDownloadCount() {
        return downloadCount;
    }

    public static void setDownloadCount(int downloadCount) {
        ProfileActivity.downloadCount = downloadCount;
    }

    private static int downloadCount=0;
    private int downloadingCount=0;
    private boolean lib_allowed=true;

    private int downloadCursor=0;

    private boolean read_only;

    private ArrayList<PictureModel> downloadList;

    public boolean isRead_only() {
        return read_only;
    }

    public void setRead_only(boolean read_only) {
        this.read_only = read_only;
    }

    private String target ="";
    private int target_id=0;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public boolean isLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public int getDownloadingCount() {
        return downloadingCount;
    }

    public void setDownloadingCount(int downloadingCount) {
        this.downloadingCount = downloadingCount;
    }



    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    private boolean multi = false;

    public static int getPosition() {
        return position;
    }

    public static void setPosition(int position) {
        ProfileActivity.position = position;
    }

    private static int position;

    private boolean subscribed;

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    private static int returnedPos = -1;

    private static int selectedCardIndex;

    public static int getSelectedCardIndex() {
        return selectedCardIndex;
    }

    public static void setSelectedCardIndex(int selectedCardIndex) {
        ProfileActivity.selectedCardIndex = selectedCardIndex;
    }

    public static int getReturnedPos() {
        return returnedPos;
    }

    public static void setReturnedPos(int returnedPos) {
        ProfileActivity.returnedPos = returnedPos;
    }

    public static int getComment_count() {
        return comment_count;
    }

    public static void setComment_count(int comment_count) {
        ProfileActivity.comment_count = comment_count;
    }

    private ArrayList<PictureModel> myPhotos;

    public ArrayList<PictureModel> getMyPhotos() {
        return myPhotos;
    }

    public void setMyPhotos(ArrayList<PictureModel> myPhotos) {
        this.myPhotos = myPhotos;
        if(detailCardViewPagerAdapter!=null){
            detailCardViewPagerAdapter.notifyDataSetChanged();
        }
    }

    public ArrayList<PictureModel> getPiclist() {
        return piclist;
    }

    public void setPiclist(ArrayList<PictureModel> piclist) {
        this.piclist = piclist;
    }

    ArrayList<PictureModel> piclist;

    public ArrayList<PictureModel> getEventgallery() {
        return eventgallery;
    }

    ArrayList<PictureModel> eventgallery;

    public void setEventgallery(ArrayList<PictureModel> eventgallery) {
        this.eventgallery = eventgallery;
    }

    ArrayList<EventModel> eventlist;
    int event_id = 0;
    String event_name = "";

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    String attachedBy = "";

    public Boolean getIs_owner() {
        return is_owner;
    }

    public void setIs_owner(Boolean is_owner) {
        this.is_owner = is_owner;
    }

    Boolean is_owner = true;

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    String remaining = "";

    public ArrayList<EventModel> getEventlist() {
        return eventlist;
    }

    @Override
    protected void onDestroy() {

        if (bp != null)
            bp.release();

        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.container_layout);

        mSVProgressHUD = new SVProgressHUD(this);

        if(!BillingProcessor.isIabServiceAvailable(this)) {

            Toast.makeText(this, "In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16", Toast.LENGTH_LONG).show();
        }

        bp = new BillingProcessor(this, getString(R.string.license_key), "13805237321912938442", this);

        Intent intent = getIntent();

        Log.d(TAG, "onCreate: has_extra:target = " + intent.hasExtra("target"));

        if(intent.hasExtra("event_id")) {
            event_id = intent.getIntExtra("event_id", 0);
            event_name = intent.getStringExtra("event_name");
            gotoEventImageGalleryView();
        }

        ProfilePhotosFragment frag = new ProfilePhotosFragment();
        Bundle b = new Bundle();

        if (intent.hasExtra("target")){

            setTarget(intent.getStringExtra("target"));
            setTarget_id(intent.getIntExtra("target_id",0));

            Log.d(TAG, "onCreate: target = " + getTarget() + " target_id = " + getTarget_id());

            // switch on target to specify which extra will get added and which fragment replaced.
            b.putString("target", getTarget());
            b.putInt("target_id", getTarget_id());
            b.putInt("image_number", 0);
        }

        frag.setArguments(b);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, frag, "ppf")
                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        // fix flicker between screen transitions
        overridePendingTransition(0, 0);
    }


    public void onProfilePhotosClick(View v) {

        getFragmentManager().beginTransaction()
                .replace(R.id.container, new ProfilePhotosFragment(), "ppf")
                .addToBackStack(null)
                .commit();
    }

    public void onProfileEventsClick(View v) {

        getFragmentManager().beginTransaction()
                .replace(R.id.container, new ProfileEventsFragment(), "pef")
                .addToBackStack(null)
                .commit();
    }


    public void onSettingsClick(View v) {



        getFragmentManager().beginTransaction()
                .replace(R.id.container, new SettingsFragment(), "sf")
                .addToBackStack(null)
                .commit();
    }

    public void onClickLogout(View v) {
        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String session_id = sharedprefs.getString("session_id", "");

        SVProgressHUD mSVProgressHUD = new SVProgressHUD(this);
        mSVProgressHUD.show();
        Users um = new Users(this);
        um.logout(session_id);
    }

    public void onClickFeedback(View v) {

        Log.d(TAG, "onClickFeedback() called with: v = [" + v + "]");

        final Dialog dialog = new Dialog(ProfileActivity.this);
        dialog.setContentView(R.layout.feedback_form);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        Button btnFeedbackCancel = (Button)dialog.findViewById(R.id.btnFeedbackCancel);
        Button btnFeedbackSubmit = (Button)dialog.findViewById(R.id.btnFeedbackSubmit);
        final EditText etFeedback = (EditText) dialog.findViewById(R.id.etFeedback);

        btnFeedbackCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btnFeedbackSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Users us = new Users(ProfileActivity.this);
                us.sendFeedback(etFeedback.getText().toString());
                dialog.dismiss();
                NetworkUtils nu = new NetworkUtils(ProfileActivity.this);
                nu.networkDialog("Thanks!","We appreciate your feedback");
            }
        });
    }

    public void showEventMore(View v) {

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String currentUsername = sharedprefs.getString("username", "");

        setTheme(R.style.ActionSheetStyleiOS7);

        ActionSheet as = new ActionSheet();
        as.createBuilder(this, getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Settings", "Filter & Sort", "Multi-Select","Invite Friends")
                .setCancelableOnTouchOutside(true)
                .setListener(ProfileActivity.this)
                .setTag("more")
                .show();
    }


    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
        actionSheet.dismiss();
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {

        String tag = actionSheet.getTag();

        if (tag.equals("picturesReport")) {

            //picturesReport created the sheet
            //do picturesReport
            final Dialog dialog = new Dialog(ProfileActivity.this);
            //int picturesReport = R.layout.report_dialog;
            //int unreport = R.layout.unreport_dialog;

            switch (isReport) {

                case 0:
                    dialog.setContentView(R.layout.report_dialog);
                    isReport = 1;
                    break;

                case 1:
                    dialog.setContentView(R.layout.unreport_dialog);
                    isReport = 0;
                    break;
            }

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView btnReport = (TextView) dialog.findViewById(R.id.btnReport);
            btnReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call picturesDelete routine with pic_id
                    reportPic();
                    dialog.dismiss();
                    //refresh the listview
                }
            });

        }else {
            switch (index) {
                case 0:
                    Bundle args = new Bundle();
                    args.putInt("event_id", event_id);
                    args.putString("event_name", event_name);
                    args.putString("remaining", remaining);
                    args.putBoolean("is_owner", getIs_owner());
                    args.putString("attachedBy", "gallery");
                    args.putBoolean("lib_allowed", isLib_allowed());
                    args.putBoolean("read_only",isRead_only());
                    args.putBoolean("is_subscribed" , isSubscribed());
                    //settings fragment for this event
                    EventSettingsFragment frag = new EventSettingsFragment();
                    frag.setArguments(args);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, frag, "esf")
                            .addToBackStack(null)
                            .commit();

                    break;
                case 1:

                    Intent i = new Intent(ProfileActivity.this, EventFilterAndSortActivity.class);
                    startActivity(i);

                    break;
                case 2:
                    if(getPiclist().size()!=0) {
                        RelativeLayout defaultLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryDefault);

                        RelativeLayout multiLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryMulti);

                        LinearLayout llExtend = (LinearLayout) findViewById(R.id.llExtendHeader);
                        llExtend.setVisibility(View.GONE);

                        defaultLayout.setVisibility(View.GONE);
                        multiLayout.setVisibility(View.VISIBLE);

                        //rebuild adapter
                        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);

                        setMulti(true);

                        //get refrence to swipeContainer
                        android.support.v4.widget.SwipeRefreshLayout swipeRefresh = (android.support.v4.widget.SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

                        swipeRefresh.setRefreshing(false);
                        swipeRefresh.setEnabled(false);

                        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, getPiclist(), 2, isMulti());

                        if (gvEventPhotos != null)
                            gvEventPhotos.setAdapter(adapter);
                    }
                    break;

                case 3:
                    NetworkUtils nu = new NetworkUtils(this);
                    nu.networkDialog("Not Implemented","Inviting firends is not implemented.");
            }
        }
    }

    private void fetchMyPhotos() {
        Events ev = new Events(ProfileActivity.this);
        ev.picturesUser();
    }

    // called back after picturesUser();
    @Override
    public void MyPhotos(ArrayList<PictureModel> piclist) {

        setMyPhotos(piclist);



        //pass this list to interface in ProfilePhotosFragment
       try{
           ProfilePhotosFragment myphotoslist = (ProfilePhotosFragment) getFragmentManager().findFragmentByTag("ppf");
           if(myphotoslist.getEv().isAllUserPicsLoaded()){
               myphotoslist.MyPhotosList(getMyPhotos());
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    @Override
    public void Events(ArrayList<EventModel> eventlist) {

        this.eventlist = eventlist;

        ImageView ivEventEmpty = (ImageView) findViewById(R.id.ivEventEmpty);
        if (eventlist.size() > 0) {
            if (ivEventEmpty != null) {
                ivEventEmpty.setVisibility(View.GONE);
            }
        } else {
            if (ivEventEmpty != null) {
                ivEventEmpty.setVisibility(View.VISIBLE);
            }
        }

        ListView lvEvents = (ListView) findViewById(R.id.lvEvents);

        EventListAdapter adapter = new EventListAdapter(this, R.layout.event_list, eventlist);

        if (lvEvents != null) {
            lvEvents.setAdapter(adapter);
        }
    }

    @Override
    public void EventGalleryPictures(ArrayList<PictureModel> piclist) {
        setPiclist(piclist);

        setEventgallery(piclist);

        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);

        SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

        RelativeLayout rlEventPictureViewEmpty = (RelativeLayout) findViewById(R.id.rlEventPictureViewEmpty);
        LinearLayout llExtend = (LinearLayout)findViewById(R.id.llExtendHeader);


        if (piclist.size() == 0) {

            swipeContainer.setVisibility(View.GONE);

            rlEventPictureViewEmpty.setVisibility(View.VISIBLE);
            if(llExtend!=null){
                llExtend.setVisibility(View.GONE);
            }

        } else {

            if(llExtend!=null){
                llExtend.setVisibility(View.VISIBLE);
            }

            if (swipeContainer != null) {
                swipeContainer.setVisibility(View.VISIBLE);
                rlEventPictureViewEmpty.setVisibility(View.GONE);
            }
        }

        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, piclist, 2, isMulti());
        if (gvEventPhotos != null)
            gvEventPhotos.setAdapter(adapter);

        //populate adapter and piclist is set
    }


    @Override
    public void onGalleryViewComplete(final SwipeRefreshLayout swipeContainer, final int event_id) {

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                Events ev = new Events(ProfileActivity.this);
                ev.picturesEvent(event_id);
                swipeContainer.setRefreshing(false);
            }

        });

    }

    private void gotoEventImageGalleryView() {
        EventPictureViewFragment fragment = new EventPictureViewFragment();

        Bundle b = new Bundle();
        b.putInt("event_id", event_id);
        b.putString("event_name", event_name);
        b.putString("remaining" , remaining);
        b.putBoolean("is_owner" , getIs_owner());
        b.putString("attachedBy", "popover");

        fragment.setArguments(b);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "esgf")
                //.addToBackStack(null)
                .commit();
    }

    public void showReportUnreport(View v) {

        Log.d(TAG, "showReportUnreport called!");

        PictureTags myTag = (PictureTags) v.getTag();
        pic_id = myTag.getImg_id();

        setTheme(R.style.ActionSheetStyleiOS7);

        boolean has_reported = myTag.getHas_reported();

        if (has_reported) {
            isReport = 1;
            myTag.setHas_reported(false);
        } else {
            isReport = 0;
            myTag.setHas_reported(true);
        }

        switch (isReport) {

            case 0:
                ActionSheet.createBuilder(this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Report")
                        .setCancelableOnTouchOutside(true)
                        .setTag("picturesReport")
                        .setListener(this).show();
                break;

            case 1:
                ActionSheet.createBuilder(this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Unreport")
                        .setCancelableOnTouchOutside(true)
                        .setTag("picturesReport")
                        .setListener(this).show();
                break;
        }
    }

    private void reportPic() {

        Log.d(TAG, "pic_id= " + pic_id);

        Users um = new Users(this);
        um.picturesReport(pic_id);
    }

    public void cancelMulti (View v){
        RelativeLayout defaultLayout = (RelativeLayout)findViewById(R.id.rlEventGalleryDefault);
        RelativeLayout multiLayout = (RelativeLayout)findViewById(R.id.rlEventGalleryMulti);
        LinearLayout llExtend = (LinearLayout)findViewById(R.id.llExtendHeader);
        llExtend.setVisibility(View.VISIBLE);

        defaultLayout.setVisibility(View.VISIBLE);
        multiLayout.setVisibility(View.GONE);

        android.support.v4.widget.SwipeRefreshLayout swipeRefresh = (android.support.v4.widget.SwipeRefreshLayout)findViewById(R.id.swipeContainerEventGallery);

        swipeRefresh.setEnabled(true);

        setMulti(false);
        //rebuild adapter
        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);
        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, getPiclist(), 2, isMulti());
        if (gvEventPhotos != null)
            gvEventPhotos.setAdapter(adapter);
    }

    public void multiDownload (View v) {

        mSVProgressHUD.show();

        downloadList = new ArrayList<>();

        for (PictureModel pic : getPiclist()) {
            if (pic.isSelected()) {
                downloadList.add(pic);
                Log.d(TAG, "multiDownload: name = " + pic.getPic_url());
                //pic.setSelected(false);
            }


        }

        for (PictureModel pic : downloadList) {
            setDownloadCount(downloadList.size());

            Log.d(TAG, "multiDownload: url = " + pic.getPic_url() + " sent");
            //Log.d(TAG, "ifpicselected: getDownloadCount = " + getDownloadCount());

            Glide.with(ProfileActivity.this)
                    .load(pic.getPic_url())
                    .asBitmap()
                    .toBytes(Bitmap.CompressFormat.JPEG, 80)
                    .into(new SimpleTarget<byte[]>() {
                        @Override
                        public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {

                            Bundle b = new Bundle();
                            b.putByteArray("resource", resource);
                            getSupportLoaderManager().restartLoader(0, b, ProfileActivity.this).forceLoad();
                        }
                    });
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d(TAG, "onActivityResult: requestCode = " + requestCode);

        if (bp.handleActivityResult(requestCode, resultCode, data)){

            //requestcode = 1 -- opened from DetailCardPagerAdapter

            super.onActivityResult(requestCode, resultCode, data);

            //int pos = data.getIntExtra("pos", 0);
            //int commentCount = data.getIntExtra("commentCount", 0);


            //setComment_count(commentCount);
            //setSelectedDetailIndex(pos);
            //Log.d(TAG, "onActivityResult: position: " + pos);

//            ExtendEventFragment fragment = (ExtendEventFragment) getFragmentManager().findFragmentByTag("eef");
//            fragment.onActivityResult(requestCode, resultCode, data);
        }else{
            int pos = data.getIntExtra("pos", -1);
            //setSelectedCardIndex(pos);
            DetailCardFragment fragment = (DetailCardFragment) getFragmentManager().findFragmentByTag("dcf");
            if(fragment!=null)
                fragment.pullCardToIndex(pos);
        }
    }

    @Override
    public void setLike(String liked, TextView tvLikes) {
        //Log.d(TAG, "setLike: liked = " + liked);

        //fetch textview value to an int
        int likes = Integer.parseInt(tvLikes.getText().toString());

        switch (liked){
            case "like":
                likes++;
                tvLikes.setText(String.valueOf(likes));

                break;

            case "unlike":
                likes--;
                if(likes<0)
                    likes=0;

                tvLikes.setText(String.valueOf(likes));
                break;
        }
    }

    public void onClickExtend(View v){

        EventTags et = (EventTags)v.getTag();

        Bundle c = new Bundle();



        c.putString("event_name", et.getEvent_name());
        c.putInt("event_id",et.getEvent_id());
        c.putString("remaining",et.getRemaining());

        Log.d("Purchase", "onClickExtend: event_id = " + event_id);
        Log.d("Purchase", "onClickExtend: event_name = " + et.getEvent_name());

        ExtendEventFragment fragment = new ExtendEventFragment();

        fragment.setArguments(c);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "eef")
                .addToBackStack(null)
                .commit();


    }

    public void onClickExtendBack(View v){
        FragmentManager fm = getFragmentManager();
        fm.popBackStackImmediate();
    }

    @Override
    public void ShowCreateEventButton(final String event_name, boolean show) {
        Log.d(TAG, "ShowCreateEventButton: create a button with " + event_name + " as the name" );
        // get reference to this button and in click event go to create event fragment.

        final Button btCreate = (Button)findViewById(R.id.btCreateNoMatchEvent);
        if(btCreate!=null)
            btCreate.setVisibility(View.GONE);

        if(!show){
            btCreate.setVisibility(View.GONE);
        }

        if(!event_name.equals("") && show) {
            btCreate.setVisibility(VISIBLE);
            btCreate.setText("# Create ");
            btCreate.setText("# Create " + event_name);

            btCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    InputMethodManager imm =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    Bundle b = new Bundle();
                    b.putString("event_name", event_name);

                    CreateEventFragment cef = new CreateEventFragment();
                    cef.setArguments(b);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, cef, "evt")
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    @Override
    public void Searches(final ArrayList<EventModel> searchlist) {
        ListView lvSearch = (ListView) findViewById(R.id.lvSearch);

        if (lvSearch != null) {

            EventSearchListAdapter adapter = new EventSearchListAdapter(this, R.layout.search_list, searchlist);
            lvSearch.setAdapter(adapter);

            lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int pos, long id) {

                    InputMethodManager inputManager = (InputMethodManager) getSystemService(CommentsActivity.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    Gson gson = new Gson();

                    //private event
                    if (!searchlist.get(pos).getIs_public()) {
                        Log.d(TAG, searchlist.get(pos).getEvent_id() + " is a private event");

                        Events em = new Events(ProfileActivity.this);
                        try {
                            em.eventsPrivateAccess(pos, searchlist, "(");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    } else {

                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedprefs.edit();

                        String previousHistory = sharedprefs.getString("history", ""); //fetch previousHistory

                        List<EventModel> historyList = new ArrayList<>();

                        Log.d(TAG, "onItemClick: previousHistory = " + previousHistory);

                        if (!previousHistory.equals("")) {
                            try {
                                JSONArray searches = new JSONArray(previousHistory);

                                for (int iter = 0; iter < searches.length(); iter++) {
                                    JSONObject finalObject = searches.getJSONObject(iter);

                                    EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);

                                    historyList.add(search); // historylist now contains the items in sharedprefs as List<EventModel>
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //check list for match before adding
                            boolean match = false;
                            for (EventModel search : historyList) {
                                if (search.getEvent_name().equals(searchlist.get(pos).getEvent_name())) {
                                    match = true;
                                }
                            }

                            if (!match && !searchlist.get(pos).getIs_owner()) {
                                if (historyList.size() > 6) {
                                    //no more than 6 items added
                                    //remove oldest to make room for the new one
                                    historyList.remove(0);
                                }

                                historyList.add(searchlist.get(pos)); // append this clicked item to historylist items
                            }

                        } else {
                            //nothing to fetch in prefs, just add this item to prefs
                            if(!searchlist.get(pos).getIs_owner())
                                historyList.add(searchlist.get(pos));
                        }

                        //add history items to sharedprefs
                        String historyItems = gson.toJson(historyList);

                        editor.putString("history", historyItems);
                        editor.commit();

                        Bundle b = new Bundle();
                        b.putInt("event_id", searchlist.get(pos).getEvent_id());
                        b.putString("event_name", searchlist.get(pos).getEvent_name());
                        //b.putBoolean("is_search", true);
                        b.putString("attachedBy", "search");
                        b.putInt("position",0);
                        b.putString("remaining", searchlist.get(pos).getRemaining());
                        b.putBoolean("is_owner", searchlist.get(pos).getIs_owner());

                        EventPictureViewFragment fragment = new EventPictureViewFragment();
                        fragment.setArguments(b);

                        //hide keyboard
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        getFragmentManager().beginTransaction()
                                .replace(R.id.container, fragment, "esgf")
                                .addToBackStack(null)
                                .commit();
                    }
                }
            });
        }
    }

    @Override
    public Loader<File> onCreateLoader(int id, Bundle args) {
        return new multiDownload(this, args.getByteArray("resource"));
    }

    @Override
    public void onLoadFinished(Loader<File> loader, File data) {

        //Log.d(TAG, "onLoadFinished: name = " + data.getName() + " downloaded");

        //gallery scanner
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(data);
            mediaScanIntent.setData(contentUri);
            sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://"
                            + Environment.getExternalStorageDirectory())));
        }

        Toast.makeText(ProfileActivity.this,"Picture " + String.valueOf(downloadCount) + " completed.",Toast.LENGTH_LONG).show();
        setDownloadCount(getDownloadCount() - 1);

        if(downloadCount==0) {
            mSVProgressHUD.dismiss();

            NetworkUtils nu = new NetworkUtils(ProfileActivity.this);
            nu.downloadMultiDialog();

            RelativeLayout defaultLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryDefault);
            RelativeLayout multiLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryMulti);
            LinearLayout llExtend = (LinearLayout) findViewById(R.id.llExtendHeader);
            llExtend.setVisibility(View.VISIBLE);

            defaultLayout.setVisibility(View.VISIBLE);
            multiLayout.setVisibility(View.GONE);

            SwipeRefreshLayout swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

            swipeRefresh.setEnabled(true);

            setMulti(false);
            //rebuild adapter
            GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);
            ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(ProfileActivity.this, R.layout.grid_layout_image, getPiclist(), 2, isMulti());
            if (gvEventPhotos != null)
                gvEventPhotos.setAdapter(adapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<File> loader) {
        mSVProgressHUD.dismiss();
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {

        /**
         * @deprecated use {@see purchaseInfo.purchaseData.purchaseToken}} instead.
         */

        Log.d(TAG, "onProductPurchased: purchaseToken= " + details.purchaseInfo.purchaseData.purchaseToken);
        Log.d(TAG, "onProductPurchased: product_id = " + productId);
        Log.d(TAG, "onProductPurchased: event_id = " + getEvent_id());
        Log.d("Purchase", "onProductPurchased: purchaseToken= " + details.purchaseInfo.purchaseData.purchaseToken);
        Log.d("Purchase", "onProductPurchased: product_id = " + productId);
        Log.d("Purchase", "onProductPurchased: event_id = " + event_id);
        //details.purchaseInfo.purchaseData.purchaseToken

        mSVProgressHUD.show();
        bp.consumePurchase(productId);
        Events ev = new Events(this);
        ev.eventsExtendWithCallback(event_id, productId, details.purchaseInfo.purchaseData.purchaseToken + "", new EventExtendApiCallback());
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.e("Purchase","Purchase History restored");
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

        Log.d(TAG, "onBillingError: errorCode = " + errorCode);

        Toast.makeText(this, "onBillingError: " + Integer.toString(errorCode), Toast.LENGTH_LONG).show();

        //return to eventPictureView with refreshed info
        //FragmentManager fm = getFragmentManager();
        //fm.popBackStackImmediate();
        Log.e("Purchase","Billing error "+errorCode);
        if(error!=null)
            Log.e("Purchase","Billing error "+error.getLocalizedMessage());

    }

    @Override
    public void onBillingInitialized() {
        Log.e("Purchase","Billing Initialized");
    }

    public void setDetailCardViewPagerAdapter(DetailCardPagerAdapter detailCardViewPagerAdapter) {
        this.detailCardViewPagerAdapter = detailCardViewPagerAdapter;
    }

    public DetailCardPagerAdapter getDetailCardViewPagerAdapter() {
        return detailCardViewPagerAdapter;
    }

    private class EventExtendApiCallback implements ApiCallback {

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
            // TODO :: Destroy ExtendEventFragment and Load EventFragment
            mSVProgressHUD.dismiss();
            Log.e("Purchase","Our Server returned Success");
            // {"op_status":"success","data":{"oldexpiredate":"2017-04-30 10:03:16","newexpiredate":"2017-05-07 10:03:16","pricepaid":"0.99","hashtag":"vv_11"}}
            try {
                FragmentManager fm = getFragmentManager();
                fm.popBackStackImmediate();
                JSONObject data = res.getJSONObject("data");
                String expTime = data.getString("newexpiredate");
                EventBus.getDefault().post(new ExtendEventSuccess(Utility.getRemainingTime(expTime)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
            mSVProgressHUD.dismiss();
            Log.e("Purchase","Our Server returned FAIL");
        }

        @Override
        public void noInternet() {
            // TODO :: Show no internet available toast
            mSVProgressHUD.dismiss();
            Toast.makeText(getApplicationContext(), getString(R.string.please_check_your_net), Toast.LENGTH_LONG).show();
        }
    }

    private static class multiDownload extends AsyncTaskLoader<File> {

        File theFile;
        byte[] resource;

        public multiDownload(Context context, byte[] resource) {
            super(context);
            this.resource=resource;
        }

        @Override
        public File loadInBackground() {

            File photoDirectory = CameraHelper.getPhotoDirectory();
            //Log.d(TAG, photoDirectory.getPath());
            File file = new File(photoDirectory + "/" + String.valueOf(downloadCount) + CameraHelper.getTimeStampFilename());
            File dir = file.getParentFile();
            try {
                if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                    throw new IOException("Cannot ensure parent directory for file " + file);
                }
                BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                s.write(resource);
                s.flush();
                s.close();

                theFile=file;

            } catch (IOException e) {
                e.printStackTrace();
            }


            return theFile;
        }
    }

}








