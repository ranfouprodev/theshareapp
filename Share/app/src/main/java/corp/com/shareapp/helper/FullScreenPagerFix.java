package corp.com.shareapp.helper;

import android.content.Context;
import android.view.MotionEvent;

/**
 * Created by randyfournier on 1/26/17.
 */

public class FullScreenPagerFix extends android.support.v4.view.ViewPager {

    public FullScreenPagerFix(Context context) {
        super(context);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            //uncomment if you really want to see these errors
            //e.printStackTrace();
            return false;
        }
    }
}