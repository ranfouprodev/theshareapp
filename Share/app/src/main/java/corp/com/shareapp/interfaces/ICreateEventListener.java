package corp.com.shareapp.interfaces;

import corp.com.shareapp.model.EventModel;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface ICreateEventListener {

    void EventCreated(EventModel createdEvent);
}
