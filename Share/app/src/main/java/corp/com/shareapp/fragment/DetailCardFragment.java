package corp.com.shareapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loopj.android.http.AsyncHttpClient;

import java.util.ArrayList;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.DetailCardPagerAdapter;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.profile.ProfileActivity;

/**
 * Created by randyfournier on 9/2/16.
 */
public class DetailCardFragment extends Fragment{
    public static final String TAG = "DetailCardFrag";

    AsyncHttpClient client = new AsyncHttpClient();

    private Activity parentActivity;

    DetailCardPagerAdapter adapter;

    public Activity getParentActivity() {
        return parentActivity;
    }

    public void setParentActivity(Activity parentActivity) {
        this.parentActivity = parentActivity;
    }

    private int commentCount=0;

    private String target;

    private int target_id;

    private boolean notified;

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    private ArrayList<PictureModel> piclist = new ArrayList<>();

    public ArrayList<PictureModel> getPiclist() {
        return piclist;
    }

    public void setPiclist(ArrayList<PictureModel> piclist) {
        this.piclist = piclist;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    private String event_name;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private int position;

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    private String attachedBy;

    private int returnedPos = -1;

    public int getReturnedPos() {
        return returnedPos;
    }

    public void setReturnedPos(int returnedPos) {
        this.returnedPos = returnedPos;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public DetailCardFragment() {

    }

    private ViewPager viewPager=null;

    public ViewPager getViewPager() {
        return viewPager;
    }

    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.event_detail_view_pager_fragment, container, false);
        return theView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        Bundle b = getArguments();

        if (b != null && b.containsKey("position")) {

            setAttachedBy(b.getString("attachedBy"));

            setPosition(b.getInt("position"));
            Log.d(TAG, "onViewCreated: passed in position = " + b.getInt("position"));

            setEvent_name(b.getString("event_name"));

            setTarget(b.getString("target"));

            setTarget_id(b.getInt("target_id"));
        }

       switch (attachedBy) {
            case "myphotos":
                setPiclist(((ProfileActivity) getActivity()).getMyPhotos());
                break;

            case "gallery":
                setPiclist(((ProfileActivity) getActivity()).getEventgallery());
                break;

            case "search":
            case "popover":
                setPiclist(((EventActivity) getActivity()).getEventgallery());
                break;
        }


        if(piclist!=null) {
            int cc;
            if (getAttachedBy().equals("myphotos") || getAttachedBy().equals("gallery")) {
                cc = ProfileActivity.getComment_count();
                ProfileActivity.setComment_count(-1);
            } else {
                cc = EventActivity.getComment_count();
                EventActivity.setComment_count(-1);
            }

            if (cc != -1) {


                piclist.get(viewPager.getCurrentItem()).setNum_comments(cc);

            }
        }

        setViewPager((ViewPager) getActivity().findViewById(R.id.pagerEventDetail));

        adapter = new DetailCardPagerAdapter(getActivity(), piclist, event_name, attachedBy);

        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(position);

        if(getActivity() instanceof ProfileActivity){
            ProfileActivity activity = (ProfileActivity)getActivity();
            activity.setDetailCardViewPagerAdapter(adapter);
        }

        if(target!=null && target.equals("PICTURE_COMMENTED")){
            if(!isNotified()) {
                Bundle c = new Bundle();
                c.putString("attachedBy", attachedBy);
                c.putString("event_name", event_name);
                c.putInt("pic_id", target_id);
                c.putInt("position", viewPager.getCurrentItem());
                setPosition(viewPager.getCurrentItem());
                //Log.d(TAG, "onViewCreated: position = " + b.getInt("position"));
                c.putInt("numComments", piclist.get(viewPager.getCurrentItem()).getNum_comments());

                CommentsFragment frag = new CommentsFragment();
                frag.setArguments(c);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, frag, "cf")
                        .addToBackStack(null)
                        .commit();
                setNotified(true);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        setParentActivity((Activity)context);
    }

    public void pullCardToIndex(int returnedIndex){

        setPosition(returnedIndex);

        viewPager.setCurrentItem(returnedIndex);

    }
}
