package corp.com.shareapp.interfaces;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface IReportListener {

    void setReported(String reported);
}
