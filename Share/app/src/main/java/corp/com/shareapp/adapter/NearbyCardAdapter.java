package corp.com.shareapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import java.util.ArrayList;

import corp.com.shareapp.R;
import corp.com.shareapp.helper.ScreenHelper;
import corp.com.shareapp.location.MapActivity;
import corp.com.shareapp.main.FullScreenViewActivity;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PictureTags;
import corp.com.shareapp.network.Users;

/**
 * Created by randyfournier on 7/28/16.
 */
public class NearbyCardAdapter extends ArrayAdapter{

    private static final String TAG = "NearbyCardAdapter";

    private Context mContext;
    View convertView;

    private ArrayList<PictureModel> nearbyModelList;
    private int resource;
    private LayoutInflater inflater;

    public NearbyCardAdapter(Context context, int resource, ArrayList<PictureModel> objects) {
        super(context, resource, objects);
        this.mContext = context;
        nearbyModelList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(resource, null);
        }


        corp.com.shareapp.helper.SquareImageView ivPreviewPic = (corp.com.shareapp.helper.SquareImageView) convertView.findViewById(R.id.ivLargePreview);
        ScreenHelper sh = new ScreenHelper((Activity) mContext);
        int imageSize = (int) sh.getWidth();

        Glide.with(mContext).load(nearbyModelList.get(position).getPic_url())
                .asBitmap()
                .placeholder(R.drawable.loading)
                .error(R.drawable.network_error)
                //.override(imageSize, imageSize)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(ivPreviewPic);


        ivPreviewPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Gson gson = new Gson();
                String piclist = gson.toJson(nearbyModelList);
                Log.d(TAG, piclist);
                Intent i = new Intent(mContext, FullScreenViewActivity.class);
                i.putExtra("picturelist", piclist);
                i.putExtra("position", position);
                ((Activity) mContext).startActivityForResult(i, 100);
            }
        });

        TextView tvUsername = (TextView) convertView.findViewById(R.id.tvUsername);
        TextView tvTaken = (TextView) convertView.findViewById(R.id.tvTaken);
        TextView tvCaption = (TextView) convertView.findViewById(R.id.tvCaption);
        final TextView tvlikes = (TextView) convertView.findViewById(R.id.tvLikes);
        TextView tvComments = (TextView) convertView.findViewById(R.id.tvComments);
        final TextView tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
        ImageView ivClickToMenu = (ImageView) convertView.findViewById(R.id.ivClickToMenu);
        final ImageView ivComment = (ImageView) convertView.findViewById(R.id.ivComment);

        //store image id as tag in this comment click icon
        PictureTags picturetag = new PictureTags();
        picturetag.setImg_id(nearbyModelList.get(position).getPic_id());
        picturetag.setPosition(position);
        picturetag.setNumComments(nearbyModelList.get(position).getNum_comments());
        ivComment.setTag(picturetag);

        PictureTags picturetags = new PictureTags();
        picturetags.setImg_id(nearbyModelList.get(position).getPic_id());
        picturetags.setUrl(nearbyModelList.get(position).getPic_url());
        picturetags.setPosition(position);
        picturetags.setUsername(nearbyModelList.get(position).getUsername());
        picturetags.setHas_reported(nearbyModelList.get(position).getHas_reported());
        ivClickToMenu.setTag(picturetags);

        tvUsername.setText(nearbyModelList.get(position).getUsername());
        tvTaken.setText(nearbyModelList.get(position).getTimePosted());
        //tvTaken.setText(nearbyModelList.get(position).getTimePosted() + "d");

        tvComments.setText(String.valueOf(nearbyModelList.get(position).getNum_comments()));


        if (nearbyModelList.get(position).getCaption().length() > 0) {
            //Log.d(TAG, "caption = " + nearbyModelList.get(position).getCaption());
            tvCaption.setVisibility(View.VISIBLE);
            tvCaption.setText(nearbyModelList.get(position).getCaption());
        } else {
            tvCaption.setVisibility(View.GONE);
        }

        final String location = nearbyModelList.get(position).getLocation_name().trim();
        int maxLen = 30;

        if (location.length() > maxLen) {
            String locationText = location.substring(0, maxLen) + "...";
            tvLocation.setText(locationText);
        } else {
            tvLocation.setText(location);
        }


        tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, MapActivity.class);
                i.putExtra("lat", nearbyModelList.get(position).getLat());
                i.putExtra("lon", nearbyModelList.get(position).getLon());
                i.putExtra("location_name", location);
                i.putExtra("attachedBy", "nearby");
                i.putExtra("position", position);

                mContext.startActivity(i);
            }
        });

        final ImageView ivLike = (ImageView) convertView.findViewById(R.id.ivLike);

        ivLike.setActivated(nearbyModelList.get(position).getHas_liked());
        tvlikes.setText(String.valueOf(nearbyModelList.get(position).getLike_count()));



        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ivLike.setActivated(!ivLike.isActivated());

                Users um = new Users(mContext);
                    um.picturesLike(nearbyModelList.get(position).getPic_id(), tvlikes);
            }
        });

        return convertView;
    }

    public void logme() {
        Log.d(TAG, "She Workeee!!!!");
    }
}
