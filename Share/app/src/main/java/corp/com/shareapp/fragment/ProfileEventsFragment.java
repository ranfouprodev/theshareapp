package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.loopj.android.http.AsyncHttpClient;

import corp.com.shareapp.R;
import corp.com.shareapp.model.EventTags;
import corp.com.shareapp.network.Events;

/**
 * Created by randyfournier on 8/20/16.
 */
public class ProfileEventsFragment extends Fragment {

    public static final String TAG = "ProfileEventsFrag";
    AsyncHttpClient client = new AsyncHttpClient();

    public ProfileEventsFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit My Photos");

        Events ev = new Events(getActivity());
        ev.events();
       // Log.d(TAG, "OnResume");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.profile_events_fragment, container, false);
        Log.d(TAG, "OnCreateView");
        return theView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Events ev = new Events(getActivity());
        ev.events();
        //Log.d(TAG, "OnViewCreated");

        ListView lv = (ListView) view.findViewById(R.id.lvEvents);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView taggedView = (TextView)view.findViewById(R.id.tvEventType);
                EventTags et = (EventTags) taggedView.getTag();

                Bundle b = new Bundle();
                b.putString("attachedBy", "gallery");
                b.putInt("position", position);
                b.putInt("event_id", et.getEvent_id());
                b.putString("event_name", et.getEvent_name());
                b.putBoolean("is_search", false);
                b.putString("remaining", et.getRemaining());
                b.putBoolean("is_owner", et.is_owner());
                b.putBoolean("lib_allowed", et.isLib_allowed());
                b.putBoolean("read_only", et.isRead_only());
                b.putBoolean("is_subscribed", et.is_subscribed());

                EventPictureViewFragment frag = new EventPictureViewFragment();
                frag.setArguments(b);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, frag, "edf")
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
