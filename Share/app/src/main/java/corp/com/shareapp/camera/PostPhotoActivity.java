package corp.com.shareapp.camera;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.Gson;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.EventSearchListAdapter;
import corp.com.shareapp.adapter.PostPhotoPagerAdapter;
import corp.com.shareapp.adapter.ProfilePhotosGridViewAdapter;
import corp.com.shareapp.comment.CommentsActivity;
import corp.com.shareapp.fragment.EventPictureViewFragment;
import corp.com.shareapp.helper.ScreenHelper;
import corp.com.shareapp.interfaces.IEventButtonListener;
import corp.com.shareapp.interfaces.EventGalleryListener;
import corp.com.shareapp.interfaces.IBatchUploadListener;
import corp.com.shareapp.interfaces.IPhotosListener;
import corp.com.shareapp.interfaces.IPrivateEventListener;
import corp.com.shareapp.interfaces.ISearchListener;
import corp.com.shareapp.interfaces.IUploadListener;
import corp.com.shareapp.main.TabBottomActivity;
import corp.com.shareapp.model.EventModel;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PostPhotoModel;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.network.Nearby;
import corp.com.shareapp.user.SignupActivity;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static android.net.Uri.fromFile;

public class PostPhotoActivity extends AppCompatActivity implements IPrivateEventListener, IUploadListener, ISearchListener, EventGalleryListener, IBatchUploadListener, IEventButtonListener, IPhotosListener {

    private SVProgressHUD mSVProgressHUD;

    private static final String TAG = "PostPhotoActivity";
    ArrayList<PostPhotoModel> photos = new ArrayList<>();
    ArrayList<PictureModel> eventgallery;
    private int imageSize = 0;
    private boolean useCustomisedCamera;
    private static final int REQUEST_PERMISSIONS_CODE = 101;

    boolean byPopover = false;

    private boolean lib_allowed=true;

    private boolean subscribed;

    private boolean signupFlag;

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public boolean isLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public ArrayList<PictureModel> getEventgallery() {
        return eventgallery;
    }

    public void setEventgallery(ArrayList<PictureModel> eventgallery) {
        this.eventgallery = eventgallery;
    }

    ViewPager viewPager = null;


    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    int event_id = 0;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    String event_name = "";
    static Uri capturedImageUri = null;
    //String picPath ="";

    public void setShowing(boolean showing) {
        this.showing = showing;
    }

    boolean showing = true;

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, String.valueOf(resultCode));
        Bitmap bitmap = null;

        if (resultCode != RESULT_OK) {
            finish();
        }

        if (resultCode == RESULT_OK) {

            //Log.d(TAG, "RESULT_OK");

            setContentView(R.layout.activity_post_photo);

            //only incase the activity is triggered from signup then eventbutton should be gone else it should be always visible
            if(signupFlag)
            {
                findViewById(R.id.btnPostToEvent).setVisibility(View.GONE);
            }

            ImageView image = (ImageView) findViewById(R.id.ivPostPhoto);

            switch (requestCode) { // 1 is camera, 2 is gallery

                case 1:
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), capturedImageUri);

                        //Log.d(TAG, "rotate = " + CameraHelper.getDeviceOrientationDegrees(PostPhotoActivity.this));

                        int rotation = CameraHelper.readExifForOrientation(capturedImageUri.getPath());

                        if (rotation != 0) {
                            //rotate bitmap properly
                            Matrix matrix = new Matrix();
                            matrix.postRotate(rotation);
                            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix,
                                    true);
                        }

                        PostPhotoModel photo = new PostPhotoModel();
                        photo.setPictureAsBitmap(bitmap);
                        photo.setImagePath(capturedImageUri.getPath());

                        photos.add(photo);


                        viewPager = (ViewPager) findViewById(R.id.pagerPostPhoto);
                        PostPhotoPagerAdapter adapter = new PostPhotoPagerAdapter(PostPhotoActivity.this, photos);
                        viewPager.setAdapter(adapter);

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;

                case 2:
                    ScreenHelper sh = new ScreenHelper(PostPhotoActivity.this);
                    int imageSize = (int) sh.getWidth();
                    //Log.d(TAG, "onActivityResult: imagesize= " + imageSize);

                    ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES); //type image is a model
                    if (images != null) {
                        for (int i = 0; i < images.size(); i++) {

                            PostPhotoModel photo = new PostPhotoModel();
                            String s = images.get(i).path;
                            bitmap = BitmapFactory.decodeFile(s);
                            //bitmap = getThumbnailBitmap(s,imageSize);

                            int rotation = CameraHelper.readExifForOrientation(s);

                            //Log.d(TAG, "onActivityResult: rotation = " + rotation);

                            if (rotation != 0) {
                                //rotate bitmap properly
                                Matrix matrix = new Matrix();
                                matrix.postRotate(rotation);
                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix,
                                        true);
                            }

                            photo.setPictureAsBitmap(bitmap);
                            photo.setImagePath(s);
                            photos.add(photo);
                    }
                        viewPager = (ViewPager) findViewById(R.id.pagerPostPhoto);
                        PostPhotoPagerAdapter adapter = new PostPhotoPagerAdapter(PostPhotoActivity.this, photos);
                        viewPager.setAdapter(adapter);
                 }else{
                        finish();
                    }
                    break;
            }

            Intent intent = getIntent();

            event_id = intent.getIntExtra("event_id", 0);
            event_name = intent.getStringExtra("event_name");

            Log.d(TAG, "event_name = " + event_name);

            Button btnNearby = (Button) findViewById(R.id.btnPostToNearby);
            //final EditText etPostPhotoCaption = (EditText) findViewById(R.id.etPostPhotoCaption);


            if (btnNearby != null) {
                btnNearby.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //post to nearby

                        //Log.d(TAG, "onClick: ");

                        //checkprefs for post to gallery flag
                        //SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        //boolean stg = sharedprefs.getBoolean("save_to_gallery", true);
                        //if (stg) {
                            Glide.with(PostPhotoActivity.this)
                                    .load(capturedImageUri)
                                    .asBitmap()
                                    .toBytes(Bitmap.CompressFormat.JPEG, 80)
                                    .into(new SimpleTarget<byte[]>() {
                                        @Override
                                        public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {

                                            Observable<byte[]> just = Observable.just(resource);
                                            just.flatMap(processResponse()).subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(handleResult());
                                        }
                                    });


                        Nearby nm = new Nearby(PostPhotoActivity.this);
                        try {
                            nm.picturesNearbyCreate(new File(capturedImageUri.getPath()), photos.get(0).getCaption());

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            final Button btnPostToEvent =(Button)findViewById(R.id.btnPostToEvent);

            if (event_id != 0) {
                //set Nearby button visibility to gone
                btnNearby.setVisibility(View.GONE);

                btnPostToEvent.setVisibility(View.VISIBLE);
                btnPostToEvent.setText("Post to " + event_name);
            }

            btnPostToEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.d(TAG, "onClick: ");

                        if (btnPostToEvent.getText().toString().equals("# Post To Event")) {
                            FrameLayout popover_container = (FrameLayout) findViewById(R.id.popover_container);

                            if (showing) {
                                popover_container.setVisibility(View.VISIBLE);
                                byPopover = true;
                                showing = false;
                            } else {
                                popover_container.setVisibility(View.GONE);
                                byPopover = false;
                                showing = true;
                            }
                        }else{

                            //checkprefs for post to gallery flag
                            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                            boolean stg = sharedprefs.getBoolean("save_to_gallery", true);
                            if (stg && requestCode != 2) {
                                Glide.with(PostPhotoActivity.this)
                                        .load(capturedImageUri)
                                        .asBitmap()
                                        .toBytes(Bitmap.CompressFormat.JPEG, 80)
                                        .into(new SimpleTarget<byte[]>() {
                                            @Override
                                            public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                                                Observable<byte[]> just = Observable.just(resource);
                                                just.flatMap(processResponse()).subscribeOn(Schedulers.io())
                                                        .observeOn(AndroidSchedulers.mainThread())
                                                        .subscribe(handleResult());
                                            }
                                        });

                            }

                            // post to event specified in button text
                            Events ev = new Events(PostPhotoActivity.this);

                            Log.d(TAG, "onClick: in #post to Event path = " + photos.get(0).getImagePath());
                            //ev.picturesEventCreate(new File(photos.get(0).getImagePath()), photos.get(0).getCaption(), event_name, event_id);
                            mSVProgressHUD.show();
                            ev.uploadMulti(photos, event_name, event_id, byPopover);
                        }
                    }

                });
            }

            ImageView ivPostPhotoBack = (ImageView) findViewById(R.id.ivPostPhotoBack);
            if (ivPostPhotoBack != null) {
                ivPostPhotoBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(PostPhotoActivity.this, PostPhotoActivity.class);
                        i.putExtra("choice", "camera");
                        startActivity(i);
                    }
                });
            }
    }


    public void customToast(){

        LayoutInflater inflater = getLayoutInflater();

        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_layout_id));

        final SpannableStringBuilder str = new SpannableStringBuilder(getString(R.string.welcome_message));
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 1, 17, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //TextView tv = new TextView(context);
        //tv.setText(str);


        // set a message
        //TextView text = (TextView) layout.findViewById(R.id.toastMessage);
        //text.setText("Button is clicked!");

        // Toast...
        //final Toast toast = Toast.makeText(getBaseContext(), Html.fromHtml(getText(R.string.welcome_message).toString()),Toast.LENGTH_SHORT);
        final Toast toast = Toast.makeText(getBaseContext(), str ,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.FILL_HORIZONTAL,0,0);
        toast.setView(layout);
        toast.show();

        new CountDownTimer(7000, 1000)
        {
            public void onTick(long millisUntilFinished) {toast.show();}
            public void onFinish() {toast.show();}

        }.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // check runtime permissions for camera and file access

        mSVProgressHUD = new SVProgressHUD(this);

        Intent intent = getIntent();

        String choice = "camera";
        useCustomisedCamera = intent.getBooleanExtra("customisedcamera",false);
        signupFlag = intent.getBooleanExtra("signup_flag", false);
        if (intent.getStringExtra("choice") != null)
            choice = intent.getStringExtra("choice");

        switch (choice) {

            case "camera":
                File f = CameraHelper.generateTimeStampPhotoFile();

                capturedImageUri = fromFile(f);
                /*if(useCustomisedCamera){
                    Intent intentCustomCamera = new Intent();
                    intentCustomCamera.setAction(Camera.ACTION_NAME);
                    intentCustomCamera.putExtra(MediaStore.EXTRA_OUTPUT,
                            capturedImageUri);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intentCustomCamera, 1);
                    }
                }else*/


                //only custom toast on camera for first time when signup activity
                if(signupFlag)
                {
                    customToast();
                }

                    {
                    Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intentCamera.putExtra("android.intent.extra.quickCapture",true);
                    intentCamera.putExtra(MediaStore.EXTRA_OUTPUT,
                            capturedImageUri);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intentCamera, 1);
                    }
                }


                break;

            case "gallery":

                Intent intentGallery = new Intent(this, AlbumSelectActivity.class);
                    //set limit on number of images that can be selected, default is 10
                intentGallery.putExtra(Constants.INTENT_EXTRA_LIMIT, 50);
                startActivityForResult(intentGallery, 2);
        }
    }

    @Override
    public void Uploaded(int event_id) {
        if (event_id == 0) { //Nearby upload
            //go to Nearby
            Intent i = new Intent(this, TabBottomActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        finish();
    }

    @Override
    public void Searches(final ArrayList<EventModel> searchlist) {
        ListView lvPopupSearch = (ListView) findViewById(R.id.lvPopover);
        EventSearchListAdapter adapter = new EventSearchListAdapter(this, R.layout.search_list, searchlist);
        if (lvPopupSearch != null) {
            lvPopupSearch.setAdapter(adapter);

            lvPopupSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                ImageView reset = (ImageView) findViewById(R.id.ivPostToEventCancel);


                @Override
                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                    Button btnPostToEvent = (Button) findViewById(R.id.btnPostToEvent);

                    InputMethodManager inputManager = (InputMethodManager) getSystemService(CommentsActivity.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    setEvent_id(searchlist.get(pos).getEvent_id());
                    setEvent_name(searchlist.get(pos).getEvent_name());
                    reset.setVisibility(View.VISIBLE);
                    btnPostToEvent.setText(" Post To " + searchlist.get(pos).getEvent_name());

                    //hide the fragment
                    FrameLayout popover_container = (FrameLayout) findViewById(R.id.popover_container);
                    popover_container.setVisibility(View.GONE);

                    Gson gson = new Gson();

                    //private event
                    if (!searchlist.get(pos).getIs_public()) {
                        Log.d(TAG, searchlist.get(pos).getEvent_id() + " is a private event");

                        Events em = new Events(PostPhotoActivity.this);
                        try {
                            em.eventsPrivateAccess(pos, searchlist, "(");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    } else {

                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedprefs.edit();

                        String previousHistory = sharedprefs.getString("history", ""); //fetch previousHistory

                        List<EventModel> historyList = new ArrayList<>();

                        Log.d(TAG, "onItemClick: previousHistory = " + previousHistory);

                        if (!previousHistory.equals("")) {
                            try {
                                JSONArray searches = new JSONArray(previousHistory);

                                for (int iter = 0; iter < searches.length(); iter++) {
                                    JSONObject finalObject = searches.getJSONObject(iter);

                                    EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);

                                    historyList.add(search); // historylist now contains the items in sharedprefs as List<EventModel>
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //check list for match before adding
                            boolean match = false;
                            for (EventModel search : historyList) {
                                if (search.getEvent_name().equals(searchlist.get(pos).getEvent_name())) {
                                    match = true;
                                }
                            }

                            if (!match) {
                                if (historyList.size() > 6) {
                                    //no more than 6 items added
                                    //remove oldest to make room for the new one
                                    historyList.remove(0);
                                }

                                historyList.add(searchlist.get(pos)); // append this clicked item to historylist items
                            }

                        } else {
                            //nothing to fetch in prefs, just add this item to prefs
                            historyList.add(searchlist.get(pos));
                        }

                        //add history items to sharedprefs
                        String historyItems = gson.toJson(historyList);

                        editor.putString("history", historyItems);
                        editor.commit();

                    }
                }
            });
        }
    }

    public void onClickResetButton(View v) {
        ImageView reset = (ImageView) findViewById(R.id.ivPostToEventCancel);
        Button btnPostToEvent =(Button)findViewById(R.id.btnPostToEvent);

        reset.setVisibility(View.GONE);
        btnPostToEvent.setText("# Post To Event");
        showing=true;
        //remove current searched from history
    }

    public static String getDevicePathFromURI(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void EventGalleryPictures(ArrayList<PictureModel> piclist) {
        setEventgallery(piclist);
        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);
        SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);
        RelativeLayout rlEventPictureViewEmpty = (RelativeLayout) findViewById(R.id.rlEventPictureViewEmpty);
        LinearLayout llExtend = (LinearLayout)findViewById(R.id.llExtendHeader);

        if (piclist.size() == 0) {

            swipeContainer.setVisibility(View.GONE);

            rlEventPictureViewEmpty.setVisibility(View.VISIBLE);

            if (llExtend != null) {
                llExtend.setVisibility(View.GONE);
            }

    }else{

        if(llExtend!=null){
            llExtend.setVisibility(View.VISIBLE);
        }


        if (swipeContainer != null) {
                swipeContainer.setVisibility(View.VISIBLE);
                rlEventPictureViewEmpty.setVisibility(View.GONE);
            }
        }

        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, piclist, 2, false);
        if (gvEventPhotos != null)
            gvEventPhotos.setAdapter(adapter);
    }

    @Override
    public void batchUploaded(ArrayList<PostPhotoModel> photos, String event_name, int event_id, boolean byPopover) {

        Events ev = new Events(PostPhotoActivity.this);

        if (photos.size() == 0) {

            if(byPopover){

                // create an intent to tabbottom
//                Intent i = new Intent(this, TabBottomActivity.class);
//                i.putExtra("event_id",event_id);
//                i.putExtra("event_name", event_name);
//                startActivity(i);
                finish();

            }else{

                finish();
                ev.picturesEvent(event_id);
            }
            //open eventimagegallery
            // check if called by popover
            //yes then pass extras event_id

            //}
            // else
            //finish
            // in the EventImageGallery lets refresh it


        } else {

            File pic = new File(photos.get(0).getImagePath());

            try {

                ev.picturesEventCreate(pic, photos.get(0).getCaption(), event_name, event_id, photos, true, byPopover);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void PrivateEventSuccessful(int pos, ArrayList<EventModel> searchlist) {
        //add this event to history
        Gson gson = new Gson();

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedprefs.edit();

        String previousHistory = sharedprefs.getString("history", ""); //fetch previousHistory

        List<EventModel> historyList = new ArrayList<>();

        Log.d(TAG, "onItemClick: previousHistory = " + previousHistory);

        if (!previousHistory.equals("")) {
            try {
                JSONArray searches = new JSONArray(previousHistory);

                for (int iter = 0; iter < searches.length(); iter++) {
                    JSONObject finalObject = searches.getJSONObject(iter);

                    EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);

                    historyList.add(search); // historylist now contains the items in sharedprefs as List<EventModel>
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            //check list for match before adding
            boolean match = false;
            for (EventModel search : historyList) {
                if (search.getEvent_name().equals(searchlist.get(pos).getEvent_name())) {
                    match = true;
                }
            }

            if (!match && !searchlist.get(pos).getIs_owner()) {
                if (historyList.size() > 6) {
                    //no more than 6 items added
                    //remove oldest to make room for the new one
                    historyList.remove(0);
                }

                historyList.add(searchlist.get(pos)); // append this clicked item to historylist items
            }

        } else {
            //nothing to fetch in prefs, just add this item to prefs
            if(!searchlist.get(pos).getIs_owner())
            historyList.add(searchlist.get(pos));
        }

        //add history items to sharedprefs
        String historyItems = gson.toJson(historyList);

        editor.putString("history", historyItems);
        editor.commit();

        Bundle b = new Bundle();
        b.putInt("event_id", searchlist.get(pos).getEvent_id());
        b.putString("event_name", searchlist.get(pos).getEvent_name());
        //b.putBoolean("is_search", true);
        b.putString("attachedBy", "postPhoto");
        b.putInt("position",0);
        b.putString("remaining", searchlist.get(pos).getRemaining());
        b.putBoolean("is_owner", searchlist.get(pos).getIs_owner());

        EventPictureViewFragment fragment = new EventPictureViewFragment();
        fragment.setArguments(b);

        //hide keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "esgf")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void PrivateEventFail(final int pos, final ArrayList<EventModel> searchlist) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(PostPhotoActivity.this);

        LayoutInflater li = LayoutInflater.from(PostPhotoActivity.this);
        View privateEventLayout = li.inflate(R.layout.private_event_dialog, null);

        builder.setView(privateEventLayout);

        final AlertDialog dialog = builder.create();

        TextView tvPrivateEventname = (TextView) privateEventLayout.findViewById(R.id.tvPrivateEventName);
        tvPrivateEventname.setText(searchlist.get(pos).getEvent_name());

        final EditText etPrivateEventPassword = (EditText) privateEventLayout.findViewById(R.id.etPrivateEventPassword);

        Button btEnterPrivateEvent = (Button) privateEventLayout.findViewById(R.id.btEnterPrivateEvent);

        dialog.show();

        btEnterPrivateEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Events em = new Events(PostPhotoActivity.this);

                String password = etPrivateEventPassword.getText().toString();

                try {
                    em.eventsPrivateAccess(pos, searchlist, password);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });
    }

    private Bitmap getThumbnailBitmap(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            bitmap = null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
                : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        return bitmap;
    }

    @Override
    public void ShowCreateEventButton(String event_name, boolean show) {
        //implement empty method to make compiler happy
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("Visit Camera");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(PostPhotoActivity.this, PostPhotoActivity.class);
        i.putExtra("choice", "camera");
        startActivity(i);
    }

    @Override
    public void MyPhotos(ArrayList<PictureModel> piclist) {

    }


    private Func1<byte[], Observable<File>> processResponse() {
        return new Func1<byte[], Observable<File>>() {
            @Override
            public Observable<File> call(byte[] resource) {
                return saveToDiskRx(resource);
            }
        };
    }

    private Observable<File> saveToDiskRx(final byte[] resource) {
        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                try {

                    File photoDirectory = CameraHelper.getPhotoDirectory();
                    // Log.d(TAG, photoDirectory.getPath());
                    File file = new File(photoDirectory + "/" + CameraHelper.getTimeStampFilename());
                    File dir = file.getParentFile();

                    if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                        throw new IOException("Cannot ensure parent directory for file " + file);
                    }
                    BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                    s.write(resource);
                    s.flush();
                    s.close();

                    subscriber.onNext(file);
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        });
    }


    private Observer<File> handleResult() {
        return new Observer<File>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(File file) {
                //gallery scanner
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Intent mediaScanIntent = new Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(file);
                    mediaScanIntent.setData(contentUri);
                    PostPhotoActivity.this.sendBroadcast(mediaScanIntent);
                } else {
                    sendBroadcast(new Intent(
                            Intent.ACTION_MEDIA_MOUNTED,
                            Uri.parse("file://"
                                    + Environment.getExternalStorageDirectory())));
                }
            }
        };
    }

}































