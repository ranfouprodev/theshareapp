package corp.com.shareapp.main;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import corp.com.shareapp.R;
import corp.com.shareapp.camera.PostPhotoActivity;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.nearby.NearbyTabActivity;
import corp.com.shareapp.profile.ProfileActivity;

public class TabBottomActivity extends TabActivity {

    private static final String TAG = "TabBottomActivity";

    private int event_id = 0;
    private String event_name = "";
    private int tab = 0;

    private String target;
    private int target_id;
    TabSpec tab4;
    TabHost tabHost;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
      /* try{
           if (intent.hasExtra("event_id")) {
               event_id = intent.getIntExtra("event_id", 0);
               event_name = intent.getStringExtra("event_name");
               tab = 3;
           }

           if (intent.hasExtra("target")) {
               setTarget(intent.getStringExtra("target"));
               setTarget_id(intent.getIntExtra("id", 0));
               tab = 3;
           }
           Log.d(TAG, "target = : " + getTarget());
           Log.d(TAG, "id = : " + getTarget_id());
           Intent i = new Intent(TabBottomActivity.this, ProfileActivity.class);
           if (event_id != 0) {
               i.putExtra("event_id", event_id);
               i.putExtra("event_name", event_name);
               tab4.setContent(i);

           } else if (getTarget() != null) {
               i.putExtra("target", getTarget());
               i.putExtra("target_id", getTarget_id());
               tab4.setContent(i);

           } else {
               tab4.setContent(new Intent(this, ProfileActivity.class));
           }
           tabHost.setCurrentTab(tab);
       }catch (Exception e){
           e.printStackTrace();
       }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tab_bottom);

        //get extras
        //get string tab
        //get int event_id
        Intent intent = getIntent();

         if (intent.hasExtra("event_id")) {
            event_id = intent.getIntExtra("event_id", 0);
            event_name = intent.getStringExtra("event_name");
            tab = 3;
        }

        if (intent.hasExtra("target")) {
            setTarget(intent.getStringExtra("target"));
            setTarget_id(intent.getIntExtra("id", 0));
            tab = 3;
        }

        Log.d(TAG, "target = : " + getTarget());
        Log.d(TAG, "id = : " + getTarget_id());

        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        tabHost.getTabWidget().setDividerDrawable(null);

        TabSpec tab1 = tabHost.newTabSpec("First Tab");
        Drawable icn = ContextCompat.getDrawable(this, R.drawable.nearbyicon_selector);
        tab1.setIndicator("", icn);

        TabSpec tab2 = tabHost.newTabSpec("Second Tab");
        icn = ContextCompat.getDrawable(this, R.drawable.eventsicon_selector);
        tab2.setIndicator("", icn);

        TabSpec tab3 = tabHost.newTabSpec("Third Tab");
        icn = ContextCompat.getDrawable(this, R.drawable.cameraicon_selector);
        tab3.setIndicator("", icn);

        tab4 = tabHost.newTabSpec("Fourth tab");
        icn = ContextCompat.getDrawable(this, R.drawable.meicon_selector);
        tab4.setIndicator("", icn);


        tab1.setContent(new Intent(this, NearbyTabActivity.class));

        //tab2.setIndicator("Tab2");
        tab2.setContent(new Intent(this, EventActivity.class));

        tab3.setContent(new Intent());

        //tab4.setIndicator("Tab4");
        //if tab == 4 then
        //put extra event_id,

        Intent i = new Intent(TabBottomActivity.this, ProfileActivity.class);
        if (event_id != 0) {
            i.putExtra("event_id", event_id);
            i.putExtra("event_name", event_name);
            tab4.setContent(i);

        } else if (getTarget() != null) {
            i.putExtra("target", getTarget());
            i.putExtra("target_id", getTarget_id());
            tab4.setContent(i);

        } else {
            tab4.setContent(new Intent(this, ProfileActivity.class));
        }
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);
        tabHost.addTab(tab4);

        tabHost.getTabWidget().getChildTabViewAt(2).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent i = new Intent(TabBottomActivity.this, PostPhotoActivity.class);
                i.putExtra("choice", "camera");
                startActivityForResult(i, 201);
            }

        });

        tabHost.setCurrentTab(tab);

        //int badgeCount = 1;
        // ShortcutBadger.applyCount(TabBottomActivity.this, badgeCount);
    }


}


