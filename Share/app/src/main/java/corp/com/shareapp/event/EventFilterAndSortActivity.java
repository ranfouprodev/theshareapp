package corp.com.shareapp.event;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;

import com.facebook.appevents.AppEventsLogger;

import corp.com.shareapp.R;

public class EventFilterAndSortActivity extends AppCompatActivity {

    RadioGroup rg;

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("Visit Event Filter");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_filter_and_sort);

        rg  = (RadioGroup) findViewById(R.id.rgEventSortBy);
        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);

        String radioSetting = sharedprefs.getString("event_sorting_option","date");
        switch (radioSetting) {
            case "like":
                rg.check(R.id.radioButtonPopular);
                break;

            case "date":
                rg.check(R.id.radioButtonRecent);
                break;
        }

    }

    public void onClickCloseEventFilterAndSort(View v){
        finish();
    }

    public void onClickOkEventFilterAndSort(View v) {

        //update sharepreferences with seekbarradius and sorting option


        //persist session_id and username to shared preferences
        final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedprefs.edit();

        int checkedId = rg.getCheckedRadioButtonId();

        switch (checkedId) {
            case R.id.radioButtonPopular:
                editor.putString("event_sorting_option", "like");
                editor.commit();
                break;

            case R.id.radioButtonRecent:
                editor.putString("event_sorting_option", "date");
                editor.commit();
                break;

        }
        finish();
    }
}
