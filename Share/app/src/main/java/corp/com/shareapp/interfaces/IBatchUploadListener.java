package corp.com.shareapp.interfaces;

import java.util.ArrayList;

import corp.com.shareapp.model.PostPhotoModel;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface IBatchUploadListener {

    void batchUploaded(ArrayList<PostPhotoModel> photos, String event_name, int event_id, boolean byPopover);
}
