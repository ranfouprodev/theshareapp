package corp.com.shareapp.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by randyfournier on 8/2/16.
 */

public class CommentModel {
    private static final String TAG = "CommentModel";

    private String username;
    private String creation_date;
    private String comment_text;

    public String getSent() {
        Locale.setDefault(Locale.US);
        TimeZone.setDefault(TimeZone.getTimeZone("America/Los_Angeles"));
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime created = formatter.parseDateTime(getCreation_date());

        DateTime now = new DateTime(DateTimeZone.UTC).withZoneRetainFields(DateTimeZone.forID("America/Los_Angeles"));

        Period p = new Period(created, now, PeriodType.dayTime());

        //Log.d(TAG, String.valueOf(p.getMinutes()));

        if (p.getDays() == 0 && p.getHours() == 0) {
            return String.valueOf(p.getMinutes()) + "m";
        } else if (p.getDays() == 0 && p.getHours() > 0) {
            return String.valueOf(p.getHours()) + "h";
        } else if (p.getDays() > 0) {
            return String.valueOf(p.getDays()) + "d";
        } else {
            return "0m";
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }
}
