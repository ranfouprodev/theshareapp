package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.facebook.appevents.AppEventsLogger;

import corp.com.shareapp.R;

/**
 * Created by randyfournier on 8/20/16.
 */
public class CreateEventFragment extends Fragment implements View.OnFocusChangeListener, TextWatcher, View.OnTouchListener {

    private EditText event_nameBox;

    public CreateEventFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit Event Creation");
        //event_nameBox.requestFocus(View.FOCUS_RIGHT);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.create_event_fragment, container, false);
        return theView;
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        event_nameBox = (EditText) view.findViewById(R.id.etEventName);
        Bundle b = getArguments();
        if (b != null && b.containsKey("event_name")) {
            event_nameBox.setText(b.getString("event_name"));
        }

        event_nameBox.setOnFocusChangeListener(this);
        event_nameBox.addTextChangedListener(this);
        //event_nameBox.setSelection(event_nameBox.getText().length());

        Switch makeprivate = (Switch) view.findViewById(R.id.swPrivate);
        final LinearLayout llprivate = (LinearLayout) view.findViewById(R.id.llPrivate);

        makeprivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    llprivate.setVisibility(View.VISIBLE);
                } else {
                    llprivate.setVisibility(View.GONE);
                }
            }
        });

        ImageView ivClose = (ImageView) view.findViewById(R.id.ivEventCreateBack);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                fm.popBackStackImmediate();
            }
        });

    }

    @Override
    public void onFocusChange(View view, boolean flag) {

        if (flag) {
            int id = view.getId();
            switch (id) {
                case R.id.etEventName: {

                    if (!event_nameBox.getText().toString().startsWith("#")) {
                        event_nameBox.setSelection(event_nameBox.getText().length());
                    }
                    //event_nameBox.requestFocus(View.FOCUS_RIGHT);
                    if (((EditText) view).getText().length() == 0) {
                        ((EditText) view).setText("#");
                        ((EditText) view).setSelection(1);
                        //event_nameBox.requestFocus(View.FOCUS_RIGHT);

                    }
                   /* if (((EditText) view).getText().toString().startsWith("#") && ((EditText) view).getText().toString().length()==1)
                    {
                        ((EditText) view).setSelection(((EditText) view).getText().length());
                    }*/

                    break;
                }
            }
        }

        if (!event_nameBox.getText().toString().startsWith("#")) {
            event_nameBox.setSelection(event_nameBox.getText().length());
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.toString().length() == 0) {
            s.append("#");
            event_nameBox.requestFocus(View.FOCUS_RIGHT);
        }

        if (!s.toString().startsWith("#")){
           s.replace(0,1, "");
        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

            int id = view.getId();

            switch (id) {
                case R.id.etEventName: {
                    //event_nameBox.requestFocus(View.FOCUS_RIGHT);
                    event_nameBox.setFocusable(true);
                    event_nameBox.requestFocus();

                    if (((EditText) view).getText().length() == 0)
                        ((EditText) view).setText("#");
                    event_nameBox.setSelection(event_nameBox.getText().length());
                    return true;
                }
            }

        return false;
    }

}
