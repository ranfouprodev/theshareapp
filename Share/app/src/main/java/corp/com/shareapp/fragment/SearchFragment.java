package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;

import org.greenrobot.eventbus.EventBus;

import corp.com.shareapp.StartUserEventLoading;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.R;

/**
 * Created by randyfournier on 8/20/16.
 */

public class SearchFragment extends Fragment implements TextWatcher, View.OnFocusChangeListener {


    private static final String TAG = "SearchFragment";
    AsyncHttpClient client = new AsyncHttpClient();

    private final String blockCharacterSet = "#";
    private boolean userEventSearchInvoked;

    //EditText searchEditText = (EditText) getActivity().findViewById(R.id.etSearch);

    public SearchFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.search_fragment, container, false);
        return theView;
    }

    @Override
    public void onResume() {

        EditText searchEditText = (EditText) getActivity().findViewById(R.id.etSearch);
//        searchEditText.setFilters(new InputFilter[]{filter});
        Events em = new Events(getActivity());

        if (TextUtils.isEmpty(searchEditText.getText().toString())) {
            em.eventsSearch("", true);
        } else {
            em.eventsSearch(searchEditText.getText().toString(), false);
        }
        Log.d(TAG, "onResume value = " + searchEditText.getText().toString());
        super.onResume();

    }

    private EditText searchEditText;
    Button btCreate;
    Events em;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        searchEditText = (EditText) getActivity().findViewById(R.id.etSearch);
        TextView tvCancel = (TextView) view.findViewById(R.id.tvSearchClickCancel);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new EventTabFragment(), "evt")
                        .commit();
            }
        });

        Drawable img;
        Resources res = getResources();
        img = res.getDrawable(R.drawable.ic_search_16dp);
        if (img != null) {
            img.setBounds(0, 0, 50, 50);
        }
        searchEditText.setCompoundDrawables(img, null, null, null);
        searchEditText.setCompoundDrawablePadding(10);
        searchEditText.setPadding(40, 0, 0, 0);
        em = new Events(getActivity());

        //Log.d(TAG, "onViewCreated value = " + searchEditText.getText().toString());

        btCreate = (Button) view.findViewById(R.id.btCreateNoMatchEvent);
        searchEditText.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);
        searchEditText.addTextChangedListener(this);
        searchEditText.setOnTouchListener(new TouchListener());
        searchEditText.setOnFocusChangeListener(this);

        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        searchEditText.requestFocus();

    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.toString().length() == 0) {
            s.append("#");
        }

        if (!s.toString().startsWith("#")){
           s.replace(0,1, "");
        }

        if (s.toString().equals("#")) {
            btCreate.setVisibility(View.GONE);
            em.eventsSearch("", true);// the second arg fetches this users events

        } else {
            final String textToSearch = s.toString().replaceAll("#", "");
            if(textToSearch.length()!=0){
                em.eventsSearch(textToSearch, false);
            }
        }
    }


    public class TouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            searchEditText.setFocusable(true);
            searchEditText.requestFocus();
            searchEditText.setSelection(searchEditText.getText().length());
            if (((EditText) view).getText().length() == 0)
                ((EditText) view).setText("#");
            return false;
        }
    }

    @Override
    public void onFocusChange(View view, boolean flag) {
        if (flag) {
            int id = view.getId();
            switch (id) {
                case R.id.etSearch: {

                    if (!searchEditText.getText().toString().startsWith("#") ) {
                        searchEditText.setSelection(searchEditText.getText().length());
                    }

                    if (((EditText) view).getText().length() == 0) {
                        ((EditText) view).setText("#");
                        ((EditText) view).setSelection(1);
                    }
                    break;
                }
            }
        }

        if (!searchEditText.getText().toString().startsWith("#")) {
            searchEditText.setSelection(searchEditText.getText().length());
        }
    }
}


