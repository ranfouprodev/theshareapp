package corp.com.shareapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.model.PostPhotoModel;

/**
 * Created by randyfournier on 8/23/16.
 */
public class PostPhotoPagerAdapter extends PagerAdapter {

    private Activity _activity;
    private List<PostPhotoModel> piclist;

    // constructor
    public PostPhotoPagerAdapter(Activity activity, List<PostPhotoModel> objects) {
        this._activity = activity;
        this.piclist = objects;
    }

    @Override
    public int getCount() {
        return this.piclist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater;
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.post_photo_preview, container,
                false);

        corp.com.shareapp.helper.SquareImageView imgDisplay = (corp.com.shareapp.helper.SquareImageView) viewLayout.findViewById(R.id.ivPostPhoto);
        EditText caption = (EditText) viewLayout.findViewById(R.id.etPostPhotoCaption);

        caption.setText(piclist.get(position).getCaption());

        imgDisplay.setImageBitmap(piclist.get(position).getPictureAsBitmap());

        caption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                piclist.get(position).setCaption(s.toString());
            }
        });

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
