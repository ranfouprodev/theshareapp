package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.facebook.appevents.AppEventsLogger;
import com.loopj.android.http.AsyncHttpClient;

import java.util.ArrayList;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.ProfilePhotosGridViewAdapter;
import corp.com.shareapp.interfaces.IMyPhotosListener;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.network.Events;

/**
 * Created by randyfournier on 8/20/16.
 */
public class ProfilePhotosFragment extends Fragment implements IMyPhotosListener {

    public static final String TAG = "ProfilePhotosFragment";
    AsyncHttpClient client = new AsyncHttpClient();

    private String target = "";
    private int target_id = 0;
    private int image_num;

    private boolean notified;

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    public ArrayList<PictureModel> getPicturelist() {
        return picturelist;
    }

    public void setPicturelist(ArrayList<PictureModel> picturelist) {
        this.picturelist = picturelist;
    }

    private ArrayList<PictureModel> picturelist;

    private IMyPhotosListener imyphotoslistener;

    public int getImage_num() {
        return image_num;
    }

    public void setImage_num(int image_num) {
        this.image_num = image_num;
    }

    private int id_position;

    public int getId_position() {
        return id_position;
    }

    public void setId_position(int id_position) {
        this.id_position = id_position;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }
    Events ev;

    public Events getEv() {
        return ev;
    }

    public ProfilePhotosFragment() {

    }
    GridView gvMyPhotos;
    ImageView imageView_bg;
    ImageView imageViewLoading;
    ProfilePhotosGridViewAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.profile_photos_fragment, container, false);
        return theView;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit My Photos");
        Log.d(TAG, "onResume: called");

        //populate the gridview with pictures for this user
        ev = new Events(getActivity());
        ev.picturesUser();

        Log.d(TAG, "onResume: picturesUser() called");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        gvMyPhotos = (GridView) view.findViewById(R.id.gvMyPhotos);
        imageView_bg = (ImageView) view.findViewById(R.id.imageView_bg);
        imageViewLoading = (ImageView) view.findViewById(R.id.imageView_loading);
        adapter = new ProfilePhotosGridViewAdapter(view.getContext(), R.layout.grid_layout_image, new ArrayList<PictureModel>(), 3, false);
        gvMyPhotos.setAdapter(adapter);
        if (adapter.getCount() == 0) {
            if (gvMyPhotos != null) {
                gvMyPhotos.setVisibility(View.GONE);
                imageView_bg.setVisibility(View.INVISIBLE);
                imageViewLoading.setVisibility(View.VISIBLE);
            }
        } else {
            if (gvMyPhotos != null) {
                gvMyPhotos.setVisibility(View.VISIBLE);
                imageView_bg.setVisibility(View.INVISIBLE);
                imageViewLoading.setVisibility(View.INVISIBLE);
            }
        }
        gvMyPhotos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle b = new Bundle();
                b.putString("attachedBy", "myphotos");
                b.putInt("position", position);
                b.putString("event_name", (getPicturelist().get(position).getEvent_name()));

                DetailCardFragment frag = new DetailCardFragment();
                frag.setArguments(b);

                /*getFragmentManager().beginTransaction()
                        .add(R.id.container, frag,"ecf")
                        .addToBackStack(null)
                        .commit();*/
                replaceFragment(frag);
            }
        });
    }
    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(R.id.container, fragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
    @Override
    public void MyPhotosList(ArrayList<PictureModel> piclist) {
        setPicturelist(piclist);
        adapter.addAll(getPicturelist());
        adapter.notifyDataSetChanged();
        if (adapter.getCount() == 0) {
            if (gvMyPhotos != null) {
                gvMyPhotos.setVisibility(View.GONE);
                imageView_bg.setVisibility(View.VISIBLE);
                imageViewLoading.setVisibility(View.INVISIBLE);
            }
        } else {
            if (gvMyPhotos != null) {
                gvMyPhotos.setVisibility(View.VISIBLE);
                imageView_bg.setVisibility(View.INVISIBLE);
                imageViewLoading.setVisibility(View.INVISIBLE);
            }
        }
        Log.d(TAG, "MyPhotos: size = " + piclist.size());

        Bundle b = getArguments();
        if (b != null && b.containsKey("target")) {

            setTarget(b.getString("target"));
            setTarget_id(b.getInt("target_id"));

            Log.d(TAG, "MyPhotos: target = " + getTarget() + " target_id = " + getTarget_id());

            for(int i=0 ; i < piclist.size() ; i++ ) {
                if (piclist.get(i).getPic_id() == getTarget_id()) {
                    setId_position(i);
                }
            }

            Log.d(TAG, "MyPhotosList: pic_id mapped to position " + getId_position());
            if(!isNotified()) {

                Bundle c = new Bundle();
                c.putString("attachedBy", "myphotos");
                c.putInt("position", getId_position());
                c.putString("event_name", (piclist.get(getId_position()).getEvent_name()));
                c.putString("target", getTarget());
                c.putInt("target_id",getTarget_id());

                DetailCardFragment frag = new DetailCardFragment();
                frag.setArguments(c);

                /*getFragmentManager().beginTransaction()
                        .replace(R.id.container, frag, "ecf")
                        .addToBackStack(null)
                        .commit();*/
                replaceFragment(frag);
                setNotified(true);
            }

        }
    }
}
