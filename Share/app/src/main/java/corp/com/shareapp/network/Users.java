package corp.com.shareapp.network;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.facebook.login.LoginManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import corp.com.shareapp.R;
import corp.com.shareapp.camera.PostPhotoActivity;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.interfaces.ILikeListener;
import corp.com.shareapp.interfaces.IReportListener;
import corp.com.shareapp.main.LoginActivity;
import corp.com.shareapp.main.TabBottomActivity;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.util.TextUtils;

/**
 * Created by randyfournier on 7/6/16.
 */
public class Users extends ContextWrapper {

    private static final String TAG = "Users";

    AsyncHttpClient client = new AsyncHttpClient();
    private boolean accountExists;

    public boolean getAccountExists() {
        return accountExists;
    }

    public void setAccountExists(boolean accountExists) {
        this.accountExists = accountExists;
    }
// no ssl workaround
    /*AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);*/

    public Users(Context base) {
        super(base);
    }

    //maps to api -- /api/users/create and /api/users/login
    public void createOrLogin(String username, String email, final String password, final String loginSignup, final SVProgressHUD spinner) {
        Log.d(TAG, "createOrLogin: ");
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            RequestParams params = new RequestParams();
            params.put("username", username);
            params.put("password", password);

            String postfix = getString(R.string.rt_login);

            if (!email.equals("")) {
                params.put("email", email);
                postfix = getString(R.string.rt_create);
            }
            client.post(getString(R.string.host) + postfix, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    JSONCreateOrLogin(res, password, loginSignup);
                    spinner.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, "res = " + res);
                    spinner.dismiss();
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            spinner.dismiss();
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void createFb(final String username, final String email, final String fb_id, final String access_token) throws JSONException, UnsupportedEncodingException {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            JSONObject params = new JSONObject();
            params.put("username", username);
            params.put("email", email);
            params.put("password","password");
            params.put("access_token", access_token);
            params.put("fb_id", fb_id);

            ByteArrayEntity entity = new ByteArrayEntity(params.toString().getBytes("UTF-8"));

            Log.d(TAG, "createFb: username = " + username);
            Log.d(TAG, "createFb: email = " + email);
            Log.d(TAG, "createFb: access_token = " + access_token);
            Log.d(TAG, "createFb: fb_id = " + fb_id);

            client.post(getBaseContext(),getString(R.string.host) + getString(R.string.rt_create_fb), entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {


                    Log.d(TAG, "createFB onSuccess: " + res.toString());
                    Log.d(TAG, "onSuccess: email= " + email);

                    if (res.has("error_message")) {

                        String message = null;
                        try {
                            message = res.getString("error_message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String op_status = "Error";

                        nu.networkDialog(op_status, message);

                    }else {

                        JSONObject data = null;
                        try {
                            data = res.getJSONObject("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String session_id = null;
                        try {
                            session_id = data.getString("session_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedprefs.edit();
                        editor.putBoolean("fb_registered",true);
                        editor.putBoolean("fb_loggedin",true);
                        editor.putString("fb_access_token",access_token);
                        editor.putString("fb_id",fb_id);
                        editor.putString("fb_username",username);
                        editor.putString("fb_email",email);
                        editor.putString("session_id", session_id);

                        editor.commit();

                        //login
                            Intent i = new Intent(Users.this, TabBottomActivity.class);
                            startActivity(i);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, "res in onfailure = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public boolean loginFb(String fb_id, String access_token) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            RequestParams params = new RequestParams();
            params.put("fb_id", fb_id);
            params.put("access_token", access_token);

            client.post(getString(R.string.host) + getString(R.string.rt_login_fb), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    Log.d(TAG, "loginFB onSuccess: " + res.toString());

                    SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedprefs.edit();

                    if (res.has("error_message")){
                       setAccountExists(false);
                       editor.putBoolean("fb_loggedin",false);
                       editor.commit();
                    }else {


                        JSONObject data = null;
                        try {
                            data = res.getJSONObject("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String session_id = null;
                        try {
                            session_id = data.getString("session_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        setAccountExists(true);

                        editor.putString("session_id", session_id);
                        editor.putBoolean("fb_loggedin",true);
                        editor.commit();

                        Log.d(TAG, "onSuccess: session_id = " + session_id);

                        Intent i = new Intent(Users.this, TabBottomActivity.class);

                        startActivity(i);
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {

            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }

        return getAccountExists();
    }

    public void logout(String session_id) {

        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);

            client.post(getString(R.string.host) + getString(R.string.rt_logout), params, new TextHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String res) {

                    //reset username and session id
                    //open login activity
                    SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedprefs.edit();
                    editor.remove("session_id");
                    editor.remove("username");
                    editor.remove("fb_loggedin");
                    LoginManager.getInstance().logOut();
                    editor.commit();
                    //Log.d(TAG, "removed!");
                    //Log.d(TAG, "res = " + res);

                    Intent i = new Intent(Users.this, LoginActivity.class);
                    startActivity(i);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void sendFeedback(String feedback) {

        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            String app_version="";

            PackageManager manager = getBaseContext().getPackageManager();
            try {
                PackageInfo info = manager.getPackageInfo(
                        getBaseContext().getPackageName(), 0);
                app_version = info.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            String phone_version = "Android";

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("feedback", feedback);
            params.put("app_version", app_version);
            params.put("phone_version", getDeviceName());

            Log.d(TAG, "sendFeedback: feedback = " + feedback + " app_version = " + app_version + " phoneVersion = " + getDeviceName());

            client.post(getString(R.string.host) + getString(R.string.rt_sendFeedback), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "onSuccess: res = " + res.toString());

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void updateRegId() {

        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");
            String registration_id = sharedprefs.getString("firebaseToken", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("registration_id", registration_id);

            Log.d(TAG, "updateRegId: registration_id = " + registration_id);

            client.post(getString(R.string.host) + getString(R.string.rt_updateRegId), params, new TextHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String res) {
                    //
                    Log.d(TAG, "updateRegId -- onSuccess: res = " + res);
                    SharedPreferences.Editor editor = sharedprefs.edit();
                    editor.putBoolean("tokenUploaded", true);
                    editor.commit();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void passwordForgot(String email, final SVProgressHUD spinner) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            RequestParams params = new RequestParams();
            params.put("email", email);

            client.post(getString(R.string.host) + getString(R.string.rt_forgot_pw), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    spinner.dismiss();
                    JSONpasswordForgot(res);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    private void JSONCreateOrLogin(JSONObject res, String password, String loginSignup) {

        Log.d(TAG, "res = " + res.toString());

        NetworkUtils nu = new NetworkUtils(this.getBaseContext());

        String op_status="";

        try {
            if (res.has("error_message")) {

                String message = res.getString("error_message");
                //op_status = res.getString("op_status");

                if (loginSignup.equals("signup"))
                    message = "The username must be at least 5 characters, and can contain only letters and numbers. Please enter a valid email. The password must be at least 8 characters and must contain at least one number, one lowercase letter, and one upper case letter.";

                String status = "Error";

                nu.networkDialog(status, message);

            } else {

                JSONObject data = res.getJSONObject("data");

                String session_id = data.getString("session_id");

                String username = data.getString("username");
                //Log.d(TAG, "session_id = " + session_id + "  usernameFromApi= " + usernameFromApi + ".");

                //persist session_id and username to shared preferences
                SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedprefs.edit();

                if(loginSignup.equals("signup"))
                {
                    editor.putBoolean("signed_up",true);
                }

                editor.putString("session_id", session_id);
                editor.putString("username", username);
                editor.putString("password", password);
                editor.commit();

                Log.d(TAG, "session_id! = " + session_id);

                if(res.getString("op_status").equals("success")) {

                    if(loginSignup.equals("signup")){

                        Intent i = new Intent(this, PostPhotoActivity.class);
                        i.putExtra("choice", "camera");
                        i.putExtra("customisedcamera",true);
                        i.putExtra("signup_flag",true);
                        i.addFlags(i.FLAG_ACTIVITY_CLEAR_TASK | i.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);

                    }else {
                        Intent i = new Intent(this, TabBottomActivity.class);
                        i.addFlags(i.FLAG_ACTIVITY_CLEAR_TASK | i.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d(TAG, "Malformed Json or other parse issue " + e);
        }
    }

    private void JSONpasswordForgot(JSONObject res) {

        Log.d(TAG, res.toString());

        String message;

        NetworkUtils nu = new NetworkUtils(this);

        try {
            String op_status = res.getString("op_status");

            if (res.has("error_message")) {

                //message = res.getString("error_message");
                message = "There was an error requesting the password change. Please try again later.";

                op_status = "Error";

                nu.networkDialog(op_status, message);

            } else {
                message = "If the email exists, an email will be sent to that address to reset your password." + System.getProperty("line.separator") + "Please check your SPAM folder for the email.";

                nu.networkDialog(op_status, message);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void picturesLike(int pic_id, final TextView tvLikes) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("pic_id", pic_id);

            client.post(getString(R.string.host) + getString(R.string.rt_pictures_like), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, "onSuccess: picturesLike " + res.toString());
                    String operation="";


                    try {
                        JSONObject data = res.getJSONObject("data");

                        operation = data.getString("operation");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Activity activity = (Activity) getBaseContext();
                    ILikeListener ill = (ILikeListener) activity;
                    ill.setLike(operation, tvLikes);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void picturesReport(int pic_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);

        if (nu.isOnline()) {
            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("pic_id", pic_id);

            client.post(getString(R.string.host) + getString(R.string.rt_pictures_report), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {

                    Log.d(TAG, res.toString());
                    String operation="";

                    try {
                        JSONObject data = res.getJSONObject("data");

                        operation = data.getString("operation");

                        Log.d(TAG, "onSuccess: operation = " + operation);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Activity activity = (Activity) getBaseContext();
                    IReportListener irl = (IReportListener)activity;
                    irl.setReported(operation);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    Log.d(TAG, "res = " + res);

                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    /** Returns the consumer friendly device name */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }
}

