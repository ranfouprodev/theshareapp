package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import corp.com.shareapp.R;
import corp.com.shareapp.nearby.NearbyTabActivity;
import corp.com.shareapp.network.Nearby;

/**
 * Created by randyfournier on 8/20/16.
 */
public class NearbyCardFragment extends Fragment{

    private static final String TAG = "NearbyCardFragment";

    private OnNearbyViewCompleteListener mListener;

    public NearbyCardFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.rv_activity_nearby, container, false);
        return theView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Log.d(TAG, "onViewCreated: ");

        Nearby nm = new Nearby(getActivity());

        int returned = NearbyTabActivity.getSelectedCardIndex();

        nm.picturesNearby();

        final SwipeRefreshLayout  swipeContainer = (SwipeRefreshLayout)view.findViewById(R.id.swipeContainerNearby);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Nearby nm = new Nearby(getActivity());
                NearbyTabActivity.setSelectedCardIndex(-1);
                nm.picturesNearby();
                swipeContainer.setRefreshing(false);
            }
        });
    }

    public static interface OnNearbyViewCompleteListener {
        public abstract void onNearbyViewComplete(SwipeRefreshLayout swipeContainer);
    }
}
