package corp.com.shareapp;

/**
 * Created by ashwinnbhanushali on 20/04/17.
 */

public  class ExtendEventSuccess {
    private String remainingTime;

    public ExtendEventSuccess(String remainingTime) {
        this.remainingTime = remainingTime;
    }

    public String getRemainingTime() {
        return remainingTime;
    }
}
