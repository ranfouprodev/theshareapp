package corp.com.shareapp.nearby;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import corp.com.shareapp.main.TabBottomActivity;
import corp.com.shareapp.R;

public class NearbyFilterAndSortActivity extends AppCompatActivity {

    private static final String TAG = "NBFilterAndSortActivity";

    SeekBar sb;
    RadioGroup rg;
    final float[] radius = {0.5f, 1, 2, 5, 10, 25, 50};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_filter_and_sort);

        final TextView tvRadius = (TextView) findViewById(R.id.tvRadius);

        sb = (SeekBar) findViewById(R.id.seekBarRadius);
        rg = (RadioGroup) findViewById(R.id.rgSortBy);

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        sb.setProgress(sharedprefs.getInt("radius", 6));

        tvRadius.setText(String.valueOf(radius[sb.getProgress()]) + " Miles");

        String radioSetting = sharedprefs.getString("sorting_option", "date");
        switch (radioSetting) {
            case "like":
                rg.check(R.id.radioButtonPopular);
                break;

            case "date":
                rg.check(R.id.radioButtonRecent);
                break;

            case "distance":
                rg.check(R.id.radioButtonClosest);
                break;
        }

        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                tvRadius.setText(String.valueOf(radius[progress]) + " Miles");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void onClickCloseFilterAndSort(View v) {
        Intent i = new Intent(NearbyFilterAndSortActivity.this, TabBottomActivity.class);
        startActivity(i);
        finish();

    }

    public void onClickOkFilterAndSort(View v) {

        //update sharepreferences with seekbarradius and sorting option


        //persist session_id and username to shared preferences
        final SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedprefs.edit();
        editor.putInt("radius", sb.getProgress());
        //editor.putString("username", usernameFromApi);
        editor.commit();
        //Log.d(TAG, "session_id! = " + session_id);

        String radius = String.valueOf(sharedprefs.getInt("radius", 5));

        Log.d(TAG, "radius= " + radius);


        int checkedId = rg.getCheckedRadioButtonId();

        switch (checkedId) {
            case R.id.radioButtonPopular:
                editor.putString("sorting_option", "like");
                editor.commit();
                break;

            case R.id.radioButtonRecent:
                editor.putString("sorting_option", "date");
                editor.commit();
                break;

            case R.id.radioButtonClosest:
                editor.putString("sorting_option", "distance");
                editor.commit();
                break;
        }
        Intent i = new Intent(NearbyFilterAndSortActivity.this, TabBottomActivity.class);
        Bundle b = new Bundle();
        b.putString("newActivity", "Nearby"); //Your id
        i.putExtras(b); //Put your id to your next Intent
        startActivity(i);
        finish();
    }
}
