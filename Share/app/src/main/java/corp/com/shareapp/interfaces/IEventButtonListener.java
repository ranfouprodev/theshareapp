package corp.com.shareapp.interfaces;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface IEventButtonListener {

    void ShowCreateEventButton(String event_name, boolean show);
}
