package corp.com.shareapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.camera.CameraHelper;
import corp.com.shareapp.fragment.CommentsFragment;
import corp.com.shareapp.fragment.EventPictureViewFragment;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.location.MapActivity;
import corp.com.shareapp.main.FullScreenViewActivity;
import corp.com.shareapp.main.TabBottomActivity;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PictureTags;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.network.Users;

import static corp.com.shareapp.R.id.container;

/**
 * Created by randyfournier on 8/23/16.
 */
public class DetailCardPagerAdapter extends PagerAdapter{

    private static final String TAG = "EventCardPager";

    private Activity _activity;
    private List<PictureModel> pictureModelList;
    private String _event_name = "";
    private String attachedBy ="";

    // constructor
    public DetailCardPagerAdapter(Activity activity, List<PictureModel> objects, String event_name, String attached) {
        this._activity = activity;
        this.pictureModelList = objects;
        this._event_name = event_name;
        attachedBy = attached;
    }

    @Override
    public int getCount() {
        if(this.pictureModelList!=null) {
            return this.pictureModelList.size();
        }else{
            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public Object instantiateItem(ViewGroup vg, final int position) {
        ImageView imgDisplay;
        LayoutInflater inflater;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.detail_card_view, vg,
                false);

        imgDisplay = (ImageView) convertView.findViewById(R.id.ivLargePreview);

       // ScreenHelper sh = new ScreenHelper(_activity);
        //int imageSize = (int) sh.getWidth();

        Glide.with(_activity).load(pictureModelList.get(position).getPic_url())
                .asBitmap()
                .error(R.drawable.network_error)
                //.thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.loading)
                .into(imgDisplay);

        imgDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create intent
                // put extra nearbyModelList, position
                //startactivity (this, fullscreenviewactivity.class

                Gson gson = new Gson();
                String piclist = gson.toJson(pictureModelList);
                Log.d(TAG, piclist);
                Intent i = new Intent(_activity, FullScreenViewActivity.class);
                i.putExtra("picturelist", piclist);
                i.putExtra("position", position);
                i.putExtra("attachedBy", attachedBy);
                _activity.startActivityForResult(i, 1);
            }
        });


        TextView tvUsername = (TextView) convertView.findViewById(R.id.tvUsername);
        TextView tvTaken = (TextView) convertView.findViewById(R.id.tvTaken);
        TextView tvCaption = (TextView) convertView.findViewById(R.id.tvCaption);
        final TextView tvLocation = (TextView) convertView.findViewById(R.id.tvLocation);
        //ImageView ivLocation = (ImageView) convertView.findViewById(R.id.ivMapIcon);
        LinearLayout llLocation = (LinearLayout) convertView.findViewById(R.id.llLocation);
        final TextView tvlikes = (TextView) convertView.findViewById(R.id.tvLikes);
        TextView tvDelete = (TextView) convertView.findViewById(R.id.tvDelete);
        TextView tvCommentDetails = (TextView) convertView.findViewById(R.id.tvCommentDetails);
        tvCommentDetails.setText(String.valueOf(pictureModelList.get(position).getNum_comments()));
        ImageView ivClose = (ImageView) convertView.findViewById(R.id.ivEventDetailBack);
        ImageView ivDownload = (ImageView) convertView.findViewById(R.id.ivEventImageDetailDownload);
        ImageView ivReport = (ImageView) convertView.findViewById(R.id.ivDetailReport);
        LinearLayout llDetailsDelete = (LinearLayout) convertView.findViewById(R.id.llDetailsDelete);
        LinearLayout llDetailsReport = (LinearLayout) convertView.findViewById(R.id.llDetailsReport);

        PictureTags picturetags = new PictureTags();
        picturetags.setUrl(pictureModelList.get(position).getPic_url());
        ivDownload.setTag(picturetags);


        SharedPreferences sharedprefs = _activity.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String currentUsername = sharedprefs.getString("username", "");

        //.d(TAG, "currentUsername = " + currentUsername);

        if (!currentUsername.equals(pictureModelList.get(position).getUsername())) {
            llDetailsDelete.setVisibility(View.GONE);
            llDetailsReport.setVisibility(View.VISIBLE);

            PictureTags pts = new PictureTags();

            pts.setImg_id(pictureModelList.get(position).getPic_id());
            pts.setHas_reported(pictureModelList.get(position).getHas_reported());
            ivReport.setTag(pts);

        } else {
            llDetailsDelete.setVisibility(View.VISIBLE);
            llDetailsReport.setVisibility(View.GONE);
        }



        //set event_name
        final TextView tvEvent_Name = (TextView) convertView.findViewById(R.id.tvEventDetailName);

        RelativeLayout.LayoutParams layoutParams =
                (RelativeLayout.LayoutParams)tvEvent_Name.getLayoutParams();

        Log.d(TAG, "instantiateItem: attachedby = " + attachedBy);

        if(!attachedBy.equals("myphotos")){
            layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.ivEventDetailBack);
        }else{
            layoutParams.addRule(RelativeLayout.RIGHT_OF, 0);
            tvEvent_Name.setTextColor(Color.BLACK);
        }

        tvEvent_Name.setLayoutParams(layoutParams);

        if(attachedBy.equals("gallery")) {
            tvEvent_Name.setText("");
        }else if(attachedBy.equals("search")){

            tvEvent_Name.setText(_event_name);

        }else {
            tvEvent_Name.setText(pictureModelList.get(position).getEvent_name());
        }

        tvEvent_Name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvEvent_Name.getText().equals("Nearby")) {
                    Intent i = new Intent(_activity, TabBottomActivity.class);
                    _activity.startActivity(i);
                } else {

                    SharedPreferences sharedprefs = _activity.getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                    String currentUsername = sharedprefs.getString("username", "");
                    if (currentUsername.equals(pictureModelList.get(position).getUsername()) && attachedBy.equals("myphotos")) {

                    //Log.d(TAG, "onClick: click!");

                    Bundle b = new Bundle();
                    b.putString("attachedBy", attachedBy);
                    b.putInt("position", position);
                    b.putInt("event_id", pictureModelList.get(position).getEvent_id());
                    b.putString("event_name", _event_name);
                    b.putBoolean("is_search", false);
                    b.putString("remaining", pictureModelList.get(position).getRemaining());


                        b.putBoolean("is_owner", true);

                        EventPictureViewFragment frag = new EventPictureViewFragment();
                        frag.setArguments(b);

                        _activity.getFragmentManager().beginTransaction()
                                .replace(container, frag, "edf")
                                .addToBackStack(null)
                                .commit();

                    }
                }
            }
        });

        if (pictureModelList.get(position).getType().equals("location")) {
            llLocation.setVisibility(View.VISIBLE);

            String location = pictureModelList.get(position).getLocation_name().trim();

            int maxLen = 30;

            if (location.length() > maxLen) {
                tvLocation.setText(location.substring(0, maxLen));
            } else {
                tvLocation.setText(location);
            }

            tvEvent_Name.setText("Nearby");
        } else {
            llLocation.setVisibility(View.GONE);
        }


        ImageView ivComments = (ImageView) convertView.findViewById(R.id.ivComments);
        ivComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "clicked");

                Bundle b = new Bundle();
                b.putString("attachedBy", attachedBy);
                b.putString("event_name", tvEvent_Name.getText().toString());
                b.putInt("pic_id", pictureModelList.get(position).getPic_id());
                b.putInt("position",position);
                b.putInt("numComments", pictureModelList.get(position).getNum_comments());

                CommentsFragment frag = new CommentsFragment();
                frag.setArguments(b);

                _activity.getFragmentManager().beginTransaction()
                        .replace(R.id.container, frag, "cf")
                        .addToBackStack(null)
                        .commit();
            }
        });

        tvUsername.setText(pictureModelList.get(position).getUsername());
        tvTaken.setText(pictureModelList.get(position).getTimePosted());

        //.d(TAG, "instantiateItem: caption =" + pictureModelList.get(position).getCaption());

        //collapse layout with no caption in this picture
        if (pictureModelList.get(position).getCaption()!="") {
            tvCaption.setText(pictureModelList.get(position).getCaption());
        } else {
            tvCaption.setVisibility(View.GONE);
        }

        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "Delete this image with id " + pictureModelList.get(position).getPic_id() + " session_id=QGYXEtYFZJZw");

                final Dialog dialog = new Dialog(_activity);
                dialog.setContentView(R.layout.delete_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                TextView btnDelete = (TextView) dialog.findViewById(R.id.btnDelete);

                    btnDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //call picturesDelete routine with pic_id
                            Events ev = new Events(_activity);
                            ev.picturesDelete(pictureModelList.get(position).getPic_id());

                            dialog.dismiss();

                            FragmentManager fm = _activity.getFragmentManager();
                            fm.popBackStackImmediate();
                        }
                    });
            }
        });

        ImageView btDownload = (ImageView) convertView.findViewById(R.id.ivEventImageDetailDownload);

        btDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(_activity, "Downloading...", Toast.LENGTH_SHORT).show();
                //Download
                Log.d(TAG, pictureModelList.get(position).getPic_url());
                Glide.with(_activity)
                        .load(pictureModelList.get(position).getPic_url())
                        .asBitmap()
                        .toBytes(Bitmap.CompressFormat.JPEG, 80)
                        .into(new SimpleTarget<byte[]>() {
                            @Override
                            public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                                new AsyncTask<Void, Void, File>() {

                                    @Override
                                    protected File doInBackground(Void... params) {
                                        File photoDirectory = CameraHelper.getPhotoDirectory();
                                        //Log.d(TAG, photoDirectory.getPath());
                                        File file = new File(photoDirectory + "/" + CameraHelper.getTimeStampFilename());
                                        File dir = file.getParentFile();
                                        try {
                                            if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                                                throw new IOException("Cannot ensure parent directory for file " + file);
                                            }
                                            BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                                            s.write(resource);
                                            s.flush();
                                            s.close();
                                            return file;

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(File file) {
                                        NetworkUtils nu = new NetworkUtils(_activity);
                                        nu.downloadDialog();

                                        //gallery scanner
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                            Intent mediaScanIntent = new Intent(
                                                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                            Uri contentUri = Uri.fromFile(file);
                                            mediaScanIntent.setData(contentUri);
                                            _activity.sendBroadcast(mediaScanIntent);
                                        } else {
                                            _activity.sendBroadcast(new Intent(
                                                    Intent.ACTION_MEDIA_MOUNTED,
                                                    Uri.parse("file://"
                                                            + Environment.getExternalStorageDirectory())));
                                        }

                                    }

                                }.execute();

                            }
                        });
            }
        });


        final ImageView ivLike = (ImageView) convertView.findViewById(R.id.ivLike);

        ivLike.setActivated(pictureModelList.get(position).getHas_liked());

        tvlikes.setText(String.valueOf(pictureModelList.get(position).getLike_count()));

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ivLike.setActivated(!ivLike.isActivated());

                Users um = new Users(_activity);

                    um.picturesLike(pictureModelList.get(position).getPic_id(),tvlikes);
            }
        });


        tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(_activity, MapActivity.class);

                i.putExtra("lat", pictureModelList.get(position).getLat());
                i.putExtra("lon", pictureModelList.get(position).getLon());

                i.putExtra("location_name", tvLocation.getText());

                i.putExtra("attachedBy","detail");

                i.putExtra("position",position);

                _activity.startActivity(i);
            }
        });


        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = _activity.getFragmentManager();
                fm.popBackStackImmediate();
            }
        });

        (vg).addView(convertView);

        return convertView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

}
