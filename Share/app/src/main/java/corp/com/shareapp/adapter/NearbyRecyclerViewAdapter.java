package corp.com.shareapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.location.MapActivity;
import corp.com.shareapp.main.FullScreenViewActivity;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PictureTags;
import corp.com.shareapp.network.Users;

/**
 * Created by randyfournier on 2/21/17.
 */

public class NearbyRecyclerViewAdapter extends RecyclerView.Adapter<NearbyRecyclerViewAdapter.NearbyCardViewHolder>{

    private static final String TAG = "NearbyRecyclerViewAdapt";

    private Context mContext;
    private List<PictureModel> nearbyModelList;

    public NearbyRecyclerViewAdapter(Context mContext, List<PictureModel> nearbyModelList) {
        this.mContext = mContext;
        this.nearbyModelList = nearbyModelList;
    }

    @Override
    public NearbyCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nearby_card_view, parent, false);
        return new NearbyCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NearbyCardViewHolder holder, final int position) {
        //add logic holder contains the views

        Glide.with(mContext).load(nearbyModelList.get(position).getPic_url())
                .asBitmap()
                .placeholder(R.drawable.loading)
                .error(R.drawable.network_error)
                //.override(imageSize, imageSize)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.ivPreviewPic);

        holder.ivPreviewPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Gson gson = new Gson();
                String piclist = gson.toJson(nearbyModelList);
                Log.d(TAG, piclist);
                Intent i = new Intent(mContext, FullScreenViewActivity.class);
                i.putExtra("picturelist", piclist);
                i.putExtra("position", position);
                ((Activity) mContext).startActivityForResult(i, 100);
            }
        });

        //store image id as tag in this comment click icon
        PictureTags picturetag = new PictureTags();
        picturetag.setImg_id(nearbyModelList.get(position).getPic_id());
        picturetag.setPosition(position);
        picturetag.setNumComments(nearbyModelList.get(position).getNum_comments());
        holder.ivComment.setTag(picturetag);

        PictureTags picturetags = new PictureTags();
        picturetags.setImg_id(nearbyModelList.get(position).getPic_id());
        picturetags.setUrl(nearbyModelList.get(position).getPic_url());
        picturetags.setPosition(position);
        picturetags.setUsername(nearbyModelList.get(position).getUsername());
        picturetags.setHas_reported(nearbyModelList.get(position).getHas_reported());
        holder.ivClickToMenu.setTag(picturetags);

        holder.tvUsername.setText(nearbyModelList.get(position).getUsername());
        holder.tvTaken.setText(nearbyModelList.get(position).getTimePosted());
        //tvTaken.setText(nearbyModelList.get(position).getTimePosted() + "d");

        holder.tvComments.setText(String.valueOf(nearbyModelList.get(position).getNum_comments()));


        if (nearbyModelList.get(position).getCaption().length() > 0) {
            //Log.d(TAG, "caption = " + nearbyModelList.get(position).getCaption());
            holder.tvCaption.setVisibility(View.VISIBLE);
            holder.tvCaption.setText(nearbyModelList.get(position).getCaption());
        } else {
            holder.tvCaption.setVisibility(View.GONE);
        }

        final String location = nearbyModelList.get(position).getLocation_name().trim();
        int maxLen = 30;

        if (location.length() > maxLen) {
            String locationText = location.substring(0, maxLen) + "...";
            holder.tvLocation.setText(locationText);
        } else {
            holder.tvLocation.setText(location);
        }


        holder.tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, MapActivity.class);
                i.putExtra("lat", nearbyModelList.get(position).getLat());
                i.putExtra("lon", nearbyModelList.get(position).getLon());
                i.putExtra("location_name", location);
                i.putExtra("attachedBy", "nearby");
                i.putExtra("position", position);

                mContext.startActivity(i);
            }
        });



        holder.ivLike.setActivated(nearbyModelList.get(position).getHas_liked());
        holder.tvlikes.setText(String.valueOf(nearbyModelList.get(position).getLike_count()));



        holder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.ivLike.setActivated(!holder.ivLike.isActivated());

                Users um = new Users(mContext);
                um.picturesLike(nearbyModelList.get(position).getPic_id(), holder.tvlikes);
            }
        });


    }

    @Override
    public int getItemCount() {
        return ((nearbyModelList != null) && (nearbyModelList.size() !=0) ? nearbyModelList.size() : 0);
    }

    static class NearbyCardViewHolder extends RecyclerView.ViewHolder {

        corp.com.shareapp.helper.SquareImageView ivPreviewPic;
        TextView tvUsername;
        TextView tvTaken;
        TextView tvCaption;
        TextView tvlikes;
        TextView tvComments;
        TextView tvLocation;
        ImageView ivClickToMenu;
        ImageView ivComment;
        ImageView ivLike;

        public NearbyCardViewHolder(View itemView) {

            super(itemView);

            ivPreviewPic = (corp.com.shareapp.helper.SquareImageView) itemView.findViewById(R.id.ivLargePreview);
            tvUsername = (TextView) itemView.findViewById(R.id.tvUsername);
            tvTaken = (TextView) itemView.findViewById(R.id.tvTaken);
            tvCaption = (TextView) itemView.findViewById(R.id.tvCaption);
            tvlikes = (TextView) itemView.findViewById(R.id.tvLikes);
            tvComments = (TextView) itemView.findViewById(R.id.tvComments);
            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            ivClickToMenu = (ImageView) itemView.findViewById(R.id.ivClickToMenu);
            ivComment = (ImageView) itemView.findViewById(R.id.ivComment);
            ivLike = (ImageView) itemView.findViewById(R.id.ivLike);

        }
    }
}
