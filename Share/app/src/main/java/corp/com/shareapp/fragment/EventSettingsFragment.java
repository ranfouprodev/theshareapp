package corp.com.shareapp.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import corp.com.shareapp.R;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.profile.ProfileActivity;

/**
 * Created by randyfournier on 8/20/16.
 */
public class EventSettingsFragment extends Fragment {

    private static final String TAG = "EventSettingsFragment";
    private int event_id=0;
    private String event_name = "";
    private String remaining = "";
    private boolean is_owner = true;
    private String attachedBy = "";
    private boolean lib_allowed = true;
    private boolean read_only;

    public boolean isRead_only() {
        return read_only;
    }

    public void setRead_only(boolean read_only) {
        this.read_only = read_only;
    }

    private boolean subscribed;

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public boolean isLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public boolean getIs_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public EventSettingsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.event_settings_fragment, container, false);
        return theView;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit Event Settings");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //get bundle
        //get event_name
        //get event_id
        //retrieve getRemaining
        //add onclick listener for extend event
        //add logic to picturesDelete event -- goes back to My Events with empty picture

        Bundle b = getArguments();
        if (b != null && b.containsKey("event_id")) {
            setEvent_name(b.getString("event_name"));
            setEvent_id(b.getInt("event_id"));
            setRemaining(b.getString("remaining"));
            setIs_owner(b.getBoolean("is_owner"));
            setAttachedBy(b.getString("attachedBy"));
            setLib_allowed(b.getBoolean("lib_allowed"));
            setRead_only(b.getBoolean("read_only"));
            setSubscribed(b.getBoolean("is_subscribed"));
        }

        Switch swPostFromLibrary = (Switch) view.findViewById(R.id.swPostingFromLibrary);
        Switch swReadOnly = (Switch)view.findViewById(R.id.swReadOnly);

        ImageView ivSubscribeInfo = (ImageView) view.findViewById(R.id.ivSubscribeInfo);
        ImageView ivPostFromLibrary = (ImageView) view.findViewById(R.id.ivPostFromLibrary);
        ImageView ivReadOnly = (ImageView) view.findViewById(R.id.ivReadOnly);

        final Switch swSubscribe = (Switch) view.findViewById(R.id.swSubscribe);

        Log.d(TAG, "onViewCreated: isSubscribed = " + isSubscribed());

        swSubscribe.setChecked(isSubscribed());

        swSubscribe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //if (swSubscribe.isChecked() != isChecked) {

                    Events ev = new Events(getActivity());

                    if (isChecked) {
                        //call subscribe
                        ev.subscribe(getEvent_id());
                    } else {
                        //call unsubscribe
                        ev.unsubscribe(getEvent_id());
                    }

                switch(getAttachedBy()){

                    case "gallery":
                        ProfileActivity pa = ((ProfileActivity)getActivity());
                        pa.setSubscribed(isChecked);
                        pa.setLib_allowed(isLib_allowed());

                        break;
                    case "search":
                    case "popover":

                        EventActivity ea = ((EventActivity)getActivity());
                        ea.setSubscribed(isChecked);
                        ea.setLib_allowed(isLib_allowed());

                        break;
                }
                }
            //}

        });

        swReadOnly.setChecked(isRead_only());

        swReadOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Events ev = new Events(getActivity());

                    ev.read_only(getEvent_id());

                if(getAttachedBy().equals("gallery")) {
                    ProfileActivity parentActivity = ((ProfileActivity) getActivity());
                    parentActivity.setRead_only(isChecked);
                }else{
                    EventActivity parentActivity = ((EventActivity) getActivity());
                }
            }
        });


        ivPostFromLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkUtils nu = new NetworkUtils(getActivity());

                //network dialog used as an info dialog
                nu.networkDialog("Allow Library","Turn this off to prevent people from posting pictures that were not taken and posted directly from Share.");
            }
        });

        ivSubscribeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkUtils nu = new NetworkUtils(getActivity());

                //network dialog used as an info dialog
                nu.networkDialog("Subscribe to Event","Subscribing to an event lets you recieve notifications when there's activity, such as someone adding a picture or the event getting close to expiration.");
            }
        });

        ivReadOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkUtils nu = new NetworkUtils(getActivity());

                //network dialog used as an info dialog
                nu.networkDialog("Make Read Only","Turn this on to prevent others from posting to this event. If on, only you will be able to add pictures to the event. This setting does not change who can view the pictures.");
            }
        });



        Log.d(TAG, "Is_owner= " + getIs_owner());

        TextView tvTitle = (TextView) view.findViewById(R.id.tvEventSettingsTitle);
        tvTitle.setText(getEvent_name() + " Settings");

        TextView tvExpires = (TextView) view.findViewById(R.id.tvEventSettingsExpires);

        //if (getRemaining() != null && getRemaining().length() > 0 && getRemaining().charAt(getRemaining().length() - 1) == 'd') {
            //setRemaining(getRemaining().substring(0, getRemaining().length() - 1) + " days");
        //}

        String duration="";
        char lastChar = getRemaining().charAt(getRemaining().length() - 1);
        switch(lastChar){
            case 'd':
                duration = "days";
                break;

            case 'h':
                duration = "hours";
                break;

            case 'm':
                duration = "minutes";
                break;
        }

        String expiresMessage = getRemaining().substring(0, getRemaining().length()-1) + " " + duration + ".";

        tvExpires.setText(expiresMessage);

        Button btDelete = (Button) view.findViewById(R.id.btDeleteEvent);
        btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d(TAG, "Delete event with id:" + event_id);
                //Log.d(TAG, "attachedBy= " + attachedBy);
                final Events ev = new Events(getActivity());

                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.delete_event_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                TextView btnCancel = (TextView) dialog.findViewById(R.id.btnDeleteEventCancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                TextView btnDelete = (TextView) dialog.findViewById(R.id.btEventDelete);
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ev.eventsDelete(getEvent_id());

                        dialog.dismiss();

                        if(getAttachedBy() != null && getAttachedBy().equals("gallery")){
                            ev.events();

                            getFragmentManager().beginTransaction()
                                    .replace(R.id.container, new ProfileEventsFragment())
                                    .commit();
                        }else{
                            FragmentManager fm = getFragmentManager();
                            fm.popBackStack();
                            fm.popBackStack();
                            fm.popBackStackImmediate();
                        }
                    }
                });
            }
        });

        Button btExtend = (Button) view.findViewById(R.id.btSettingsExtendEvent);

            /*btExtend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Extend Events clicked!");


                }
            });*/


        if (getIs_owner()) {
            swPostFromLibrary.setVisibility(View.VISIBLE);
            btExtend.setVisibility(View.VISIBLE);
            btDelete.setVisibility(View.VISIBLE);
        } else {
            swPostFromLibrary.setVisibility(View.INVISIBLE);
            btExtend.setVisibility(View.INVISIBLE);
            btDelete.setVisibility(View.INVISIBLE);
        }

        ImageView ivClose = (ImageView) view.findViewById(R.id.ivEventSettingsBack);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getFragmentManager();
                fm.popBackStackImmediate();
            }
        });

        swPostFromLibrary.setChecked(isLib_allowed());

        swPostFromLibrary.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //commit prefs on change
                //editor.putBoolean("allow_posting", isChecked);
                //editor.commit();
                Events ev = new Events(getActivity());
                ev.lib_allowed(getEvent_id());

                if(getAttachedBy().equals("gallery")) {
                    ProfileActivity parentActivity = ((ProfileActivity) getActivity());
                    parentActivity.setLib_allowed(isChecked);
                }else{
                    EventActivity parentActivity = ((EventActivity) getActivity());
                    parentActivity.setLib_allowed(isChecked);
                }
            }
        });
    }
}
