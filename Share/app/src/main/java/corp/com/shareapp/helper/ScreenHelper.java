package corp.com.shareapp.helper;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;

/**
 * Created by randyfournier on 9/2/16.
 */
public class ScreenHelper {

    private Activity activity;
    private float dpWidth;
    private float dpHeight;

    public ScreenHelper(Activity activity) {
        this.activity = activity;

        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = activity.getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels / density;
        dpWidth = outMetrics.widthPixels / density;
    }

    public float getWidth() {
        return dpWidth;
    }

    public float getHeight() {
        return dpHeight;
    }

}
