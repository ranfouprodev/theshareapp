package corp.com.shareapp.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import corp.com.shareapp.R;
import corp.com.shareapp.network.Nearby;
import corp.com.shareapp.network.Users;
import corp.com.shareapp.user.LostPasswordActivity;
import corp.com.shareapp.user.SignupActivity;


public class LoginActivity extends AppCompatActivity {

    /**
     * This is the main/default activity
     *
     *
     */

    private static final String TAG = "LoginActivity";

    private static final int REQUEST_PERMISSIONS_CODE = 101; // identifies runtime permissions request

    private SVProgressHUD mSVProgressHUD; // 3rd party progressHud (busy spinner) see: https://github.com/SVProgressHUD/SVProgressHUD

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Log.d(TAG, "onCreate");

        mSVProgressHUD = new SVProgressHUD(this);

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            checkNewInstall(); // on install check allowed location, show allowedlocation page or signup page or do autologin

            Log.d(TAG, "onCreate: checkNewInstall called!");
        }else{
            if (permissionCheck==0){
                checkNewInstall();
            }
        }



        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        REQUEST_PERMISSIONS_CODE);
            }
        }

        setContentView(R.layout.activity_main); // main is composite layout -- includes @layout/login
    }


    @Override
    protected void onResume() {
        super.onResume();
        // fix flicker between screen transitions
        overridePendingTransition(0, 0);
    }

    private void checkNewInstall() {

                LoginManager.getInstance().logOut();

                SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);

                Users um = new Users(this);



                // have we logged in previously? if so we will have a session_id in sharedpreferences
                if (sharedprefs.contains("session_id")) {

                    // take user to bottom tabs page

                    Intent i = new Intent(this, TabBottomActivity.class);

                    //testing data by kanaiya
                    //i.putExtra("target","PICTURE_LIKED");
                    //i.putExtra("id",1827920);

                    if (getIntent().hasExtra("target")) {
                        String setTarget = (getIntent().getStringExtra("target"));
                        int setTarget_id = Integer.parseInt((getIntent().getStringExtra("target_id")));
                        i.putExtra("target",setTarget);
                        i.putExtra("id",setTarget_id);

                    }
                    startActivity(i);
                    finish();

                } else if (!sharedprefs.contains("visitedSignup")) { // flag set when we click go to login link in signup

                    // call check location in Nearby class in network package.

                    Nearby nb = new Nearby(this);
                    nb.checkLocation();
                }

                /** check have we logged in previously with facebook (provided a username (if facebook graphapi didn't return one)
                 * and a password and registered with share api
                 * a facebook id and access token (token provided from share api) will be persisted in sharedpreferences
                 * facebook login logic can be found in FacebookLoginfragment
                 */

                if (sharedprefs.contains("fb_loggedin")) {
                    um.loginFb(sharedprefs.getString("fb_id", ""), sharedprefs.getString("fb_access_token", ""));
                }


    }

    public void onClickToLostPassword(View v) { // called from @layout/login.xml -- textViewLostPassword.Onclick()
        //Log.d(TAG, "onClickToLostPassword");

        // take user to lost password screen
        Intent i = new Intent(LoginActivity.this, LostPasswordActivity.class);
        startActivity(i);
    }

    public void onClickToSignup(View v) { // textViewSignUp.onclick()

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);

        boolean disAllowed = sharedprefs.getBoolean("disallowLocation",false); // set if we got the checklocation page previously

        if(!disAllowed) { // you can signup from this alllowed Lat/lon
            Intent i = new Intent(LoginActivity.this, SignupActivity.class);
            startActivity(i);

        }else{ // lat/lon not allowed to signup as new user

            SharedPreferences.Editor editor = sharedprefs.edit();
            editor.putBoolean("visitedCheckLocation", false);
            editor.commit();

            // user clicked signup but not allowed, take them back to checklocation page
            Nearby nb = new Nearby(this);
            nb.checkLocation();
        }
    }

    public void onClickLogin(View v) { // do login to backend api

        //get edittext's texts and trim spaces
        EditText usernameField = (EditText) findViewById(R.id.editTextUsername);
        EditText passwordField = (EditText) findViewById(R.id.editTextPassword);

        // if they use their email as username trim email host postfix
        String usernametemp = usernameField.getText().toString().trim();
        String[] u = usernametemp.split("@");
        final String username = u[0];
        //Log.d(TAG, username);

        final String password = passwordField.getText().toString().trim();

        if (username.equals("")) {
            usernameField.setError("Incomplete, Please enter a username.");

            if (password.equals(""))
                passwordField.setError("Incomplete, Please enter a password.");

        } else {

            Users um = new Users(LoginActivity.this); // call create/login method in Users class in network package

            mSVProgressHUD.show();

            // 3rd arg = is this signup or login?
            // last arg is an instance of this spinning progress hud to allow it to be dismissed
            um.createOrLogin(username, "", password, "login", mSVProgressHUD);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        checkNewInstall(); // on install check allowed location, show allowedlocation page or signup page or do autologin


    }
}
