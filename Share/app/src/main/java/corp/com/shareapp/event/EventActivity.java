package corp.com.shareapp.event;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.baoyz.actionsheet.ActionSheet;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.ExtendEventSuccess;
import corp.com.shareapp.R;
import corp.com.shareapp.StartUserEventLoading;
import corp.com.shareapp.Utility;
import corp.com.shareapp.adapter.EventListAdapter;
import corp.com.shareapp.adapter.EventSearchListAdapter;
import corp.com.shareapp.adapter.ProfilePhotosGridViewAdapter;
import corp.com.shareapp.camera.CameraHelper;
import corp.com.shareapp.comment.CommentsActivity;
import corp.com.shareapp.fragment.CreateEventFragment;
import corp.com.shareapp.fragment.DetailCardFragment;
import corp.com.shareapp.fragment.EventPictureViewFragment;
import corp.com.shareapp.fragment.EventSettingsFragment;
import corp.com.shareapp.fragment.EventTabFragment;
import corp.com.shareapp.fragment.ExtendEventFragment;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.interfaces.ApiCallback;
import corp.com.shareapp.interfaces.EventGalleryListener;
import corp.com.shareapp.interfaces.ICreateEventListener;
import corp.com.shareapp.interfaces.IEventButtonListener;
import corp.com.shareapp.interfaces.IEventsListener;
import corp.com.shareapp.interfaces.ILikeListener;
import corp.com.shareapp.interfaces.IPhotosListener;
import corp.com.shareapp.interfaces.IPrivateEventListener;
import corp.com.shareapp.interfaces.ISearchListener;
import corp.com.shareapp.interfaces.IUploadListener;
import corp.com.shareapp.model.EventModel;
import corp.com.shareapp.model.EventTags;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.model.PictureTags;
import corp.com.shareapp.network.Events;
import corp.com.shareapp.network.Users;
import cz.msebera.android.httpclient.Header;

import static android.view.View.VISIBLE;


public class EventActivity extends FragmentActivity implements BillingProcessor.IBillingHandler, IPrivateEventListener, ActionSheet.ActionSheetListener, EventGalleryListener, IEventButtonListener, ISearchListener, ICreateEventListener, IEventsListener, IUploadListener, EventPictureViewFragment.OnGalleryViewCompleteListener, ILikeListener, IPhotosListener, LoaderManager.LoaderCallbacks<File> {


    public ArrayList<PictureModel> getEventgallery() {
        return eventgallery;
    }

    ArrayList<PictureModel> eventgallery;

    public ArrayList<EventModel> getEventlist() {
        return eventlist;
    }

    ArrayList<EventModel> eventlist;
    ArrayList<PictureModel> piclist;

    private ArrayList<PictureModel> downloadList;

    private static int downloadCount2 = 0;

    public ArrayList<PictureModel> getPiclist() {
        return piclist;
    }

    public void setPiclist(ArrayList<PictureModel> piclist) {
        this.piclist = piclist;
    }

    int event_id = 0;
    String event_name = "";
    private boolean multi = false;
    private boolean lib_allowed;
    Events events;

    private boolean read_only;

    public boolean isRead_only() {
        return read_only;
    }

    public void setRead_only(boolean read_only) {
        this.read_only = read_only;
    }

    private boolean subscribed;

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    private static int comment_count = -1;

    public boolean isLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    public static int getComment_count() {
        return comment_count;
    }

    public static void setComment_count(int comment_count) {
        EventActivity.comment_count = comment_count;
    }

    public static int getPosition() {
        return position;
    }

    public static void setPosition(int position) {
        EventActivity.position = position;
    }

    private static int position;

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    String attachedBy = "";

    public Boolean getIs_owner() {
        return is_owner;
    }

    public void setIs_owner(Boolean is_owner) {
        this.is_owner = is_owner;
    }

    Boolean is_owner = true;

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    final public static String TAG = "EventActivity";
    String remaining = "";
    int isReport = 0;
    int pic_id;
    boolean allowPostingFromLibrary = true;

    private SVProgressHUD mSVProgressHUD;

    private int downloadingCount = 0;

    public BillingProcessor bp;

    public BillingProcessor getBp() {
        return bp;
    }

    public void setBp(BillingProcessor bp) {
        this.bp = bp;
    }

    private static int returnedPos = -1;

    public static int getReturnedPos() {
        return returnedPos;
    }

    public static void setReturnedPos(int returnedPos) {
        EventActivity.returnedPos = returnedPos;
    }

    public static int getDownloadCount2() {
        return downloadCount2;
    }

    public static void setDownloadCount2(int downloadCount) {
        EventActivity.downloadCount2 = downloadCount;
    }

    public int getDownloadingCount() {
        return downloadingCount;
    }

    public void setDownloadingCount(int downloadingCount) {
        this.downloadingCount = downloadingCount;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.container_layout);
        EventBus.getDefault().register(this);
        events = new Events(this);
        mSVProgressHUD = new SVProgressHUD(this);
        if (BillingProcessor.isIabServiceAvailable(getApplicationContext())) {

            bp = BillingProcessor.newBillingProcessor(this, getString(R.string.license_key), "13805237321912938442", this);
            bp.initialize();
        }
        // check runtime for file access

        EventTabFragment event = new EventTabFragment();
        getFragmentManager().beginTransaction()
                .add(R.id.container, event)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onCreateEventButtonClicked(View v) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, new CreateEventFragment(), "evt")
                .addToBackStack(null)
                .commit();
    }

    public void onEventCreate(View v) {

        LinearLayout llprivate = (LinearLayout) findViewById(R.id.llPrivate);
        final EditText en = (EditText) findViewById(R.id.etEventName);
        final String eventName = en.getText().toString().trim().replaceAll("#", "");

        if (eventName.length() == 0) {
            Toast.makeText(getApplicationContext(), "Please Enter Event Name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (llprivate.getVisibility() == View.VISIBLE) {

            EditText password = (EditText) findViewById(R.id.etCreate_event_password);
            EditText confirmPassword = (EditText) findViewById(R.id.etCreateEventPasswordConfirm);

            if (password.getText().toString().equals(confirmPassword.getText().toString())) {
                Events em = new Events(this);
                em.eventsPrivateCreate(eventName, password.getText().toString());
            } else {
                confirmPassword.setError("This has to be the same as Password. Please try again");
                //password.setText("");
                confirmPassword.setText("");
            }

        } else {

            //hide the keyboard

            InputMethodManager inputManager = (InputMethodManager) getSystemService(CommentsActivity.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            Events em = new Events(this);
            em.eventsPublicCreate(eventName);
        }
    }


    public void setEventgallery(ArrayList<PictureModel> eventgallery) {
        this.eventgallery = eventgallery;
    }

    @Override
    public void EventGalleryPictures(ArrayList<PictureModel> piclist) {

        setPiclist(piclist);

        setEventgallery(piclist);

        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);

        SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

        RelativeLayout rlEventPictureViewEmpty = (RelativeLayout) findViewById(R.id.rlEventPictureViewEmpty);
        LinearLayout llExtend = (LinearLayout) findViewById(R.id.llExtendHeader);

        if (piclist.size() == 0) {

            if (swipeContainer != null)
                swipeContainer.setVisibility(View.GONE);

            rlEventPictureViewEmpty.setVisibility(View.VISIBLE);
            if (llExtend != null) {
                llExtend.setVisibility(View.GONE);
            }

        } else {

            if (llExtend != null) {
                llExtend.setVisibility(View.VISIBLE);
            }


            if (swipeContainer != null) {
                swipeContainer.setVisibility(View.VISIBLE);
                rlEventPictureViewEmpty.setVisibility(View.GONE);
            }
        }

        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, piclist, 2, false);
        if (gvEventPhotos != null)
            gvEventPhotos.setAdapter(adapter);
    }

    public void showEventMore(View v) {

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        String currentUsername = sharedprefs.getString("username", "");


        setTheme(R.style.ActionSheetStyleiOS7);


        ActionSheet.createBuilder(this, getSupportFragmentManager())
                .setCancelButtonTitle("Cancel")
                .setOtherButtonTitles("Settings", "Filter & Sort", "Multi-Select", "Invite Friends")
                .setCancelableOnTouchOutside(true)
                .setListener(EventActivity.this).show();
    }

    public void showReportUnreport(View v) {

        Log.d(TAG, "showReportUnreport called!");

        PictureTags myTag = (PictureTags) v.getTag();
        Log.d(TAG, "showReportUnreport: img_id = " + myTag.getImg_id());
        pic_id = myTag.getImg_id();

        setTheme(R.style.ActionSheetStyleiOS7);

        boolean has_reported = myTag.getHas_reported();

        if (has_reported) {
            isReport = 1;
            myTag.setHas_reported(false);
        } else {
            isReport = 0;
            myTag.setHas_reported(true);
        }

        switch (isReport) {

            case 0:
                ActionSheet.createBuilder(this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Report")
                        .setCancelableOnTouchOutside(true)
                        .setTag("picturesReport")
                        .setListener(this).show();
                break;

            case 1:
                ActionSheet.createBuilder(this, getSupportFragmentManager())
                        .setCancelButtonTitle("Cancel")
                        .setOtherButtonTitles("Unreport")
                        .setCancelableOnTouchOutside(true)
                        .setTag("picturesReport")
                        .setListener(this).show();
                break;
        }
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index) {
        String tag = actionSheet.getTag();

        if (tag.equals("picturesReport")) {

            //picturesReport created the sheet
            //do picturesReport
            final Dialog dialog = new Dialog(EventActivity.this);
            //int picturesReport = R.layout.report_dialog;
            //int unreport = R.layout.unreport_dialog;

            switch (isReport) {

                case 0:
                    dialog.setContentView(R.layout.report_dialog);
                    isReport = 1;
                    break;

                case 1:
                    dialog.setContentView(R.layout.unreport_dialog);
                    isReport = 0;
                    break;
            }

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            TextView btnCancel = (TextView) dialog.findViewById(R.id.btnCancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView btnReport = (TextView) dialog.findViewById(R.id.btnReport);
            btnReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //call picturesDelete routine with pic_id
                    reportPic();
                    dialog.dismiss();
                    //refresh the listview
                }
            });

        } else {

            switch (index) {
                case 0:
                    //settings fragment for this event
                    Bundle args = new Bundle();
                    args.putInt("event_id", event_id);
                    args.putString("event_name", event_name);
                    args.putString("remaining", remaining);
                    args.putBoolean("is_owner", getIs_owner());
                    args.putBoolean("lib_allowed", isLib_allowed());
                    args.putString("attachedBy", "search");
                    args.putBoolean("is_subscribed", isSubscribed());

                    EventSettingsFragment frag = new EventSettingsFragment();
                    frag.setArguments(args);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, frag, "esf")
                            .addToBackStack(null)
                            .commit();
                    break;
                case 1:
                    Intent i = new Intent(EventActivity.this, EventFilterAndSortActivity.class);
                    startActivity(i);
                    break;
                case 2:

                    if (getPiclist().size() != 0) {
                        RelativeLayout defaultLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryDefault);
                        RelativeLayout multiLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryMulti);
                        LinearLayout llExtend = (LinearLayout) findViewById(R.id.llExtendHeader);
                        llExtend.setVisibility(View.GONE);

                        defaultLayout.setVisibility(View.GONE);
                        multiLayout.setVisibility(View.VISIBLE);

                        //rebuild adapter
                        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);
                        setMulti(true);

                        //get refrence to swipeContainer
                        android.support.v4.widget.SwipeRefreshLayout swipeRefresh = (android.support.v4.widget.SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

                        swipeRefresh.setRefreshing(false);
                        swipeRefresh.setEnabled(false);

                        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, getPiclist(), 2, isMulti());
                        if (gvEventPhotos != null)
                            gvEventPhotos.setAdapter(adapter);
                    }
                    break;

                case 3:
                    NetworkUtils nu = new NetworkUtils(this);
                    nu.networkDialog("Not Implemented", "Inviting firends is not implemented.");
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StartUserEventLoading event) {
        events.eventsSearch("", true);
    }

    @Override
    public void Searches(final ArrayList<EventModel> searchlist) {
        ListView lvSearch = (ListView) findViewById(R.id.lvSearch);

        if (lvSearch != null) {

            EventSearchListAdapter adapter = new EventSearchListAdapter(this, R.layout.search_list, searchlist);
            lvSearch.setAdapter(adapter);

            lvSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int pos, long id) {

                    InputMethodManager inputManager = (InputMethodManager) getSystemService(CommentsActivity.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    Gson gson = new Gson();

                    //private event
                    if (!searchlist.get(pos).getIs_public()) {
                        Log.d(TAG, searchlist.get(pos).getEvent_id() + " is a private event");

                        Events em = new Events(EventActivity.this);
                        try {
                            em.eventsPrivateAccess(pos, searchlist, "(");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    } else {

                        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedprefs.edit();

                        String previousHistory = sharedprefs.getString("history", ""); //fetch previousHistory

                        List<EventModel> historyList = new ArrayList<>();

                        Log.d(TAG, "onItemClick: previousHistory = " + previousHistory);

                        if (!previousHistory.equals("")) {
                            try {
                                JSONArray searches = new JSONArray(previousHistory);

                                for (int iter = 0; iter < searches.length(); iter++) {
                                    JSONObject finalObject = searches.getJSONObject(iter);

                                    EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);

                                    historyList.add(search); // historylist now contains the items in sharedprefs as List<EventModel>
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //check list for match before adding
                            boolean match = false;
                            for (EventModel search : historyList) {
                                if (search.getEvent_name().equals(searchlist.get(pos).getEvent_name())) {
                                    match = true;
                                }
                            }

                            if (!match && !searchlist.get(pos).getIs_owner()) {
                                if (historyList.size() > 6) {
                                    //no more than 6 items added
                                    //remove oldest to make room for the new one
                                    historyList.remove(0);
                                }

                                historyList.add(searchlist.get(pos)); // append this clicked item to historylist items
                            }

                        } else {
                            //nothing to fetch in prefs, just add this item to prefs
                            if (!searchlist.get(pos).getIs_owner())
                                historyList.add(searchlist.get(pos));
                        }

                        //add history items to sharedprefs
                        String historyItems = gson.toJson(historyList);

                        editor.putString("history", historyItems);
                        editor.commit();

                        Bundle b = new Bundle();
                        b.putInt("event_id", searchlist.get(pos).getEvent_id());
                        b.putString("event_name", searchlist.get(pos).getEvent_name());
                        //b.putBoolean("is_search", true);
                        b.putString("attachedBy", "search");
                        b.putInt("position", 0);
                        b.putString("remaining", searchlist.get(pos).getRemaining());
                        b.putBoolean("is_owner", searchlist.get(pos).getIs_owner());
                        b.putBoolean("is_subscribed", searchlist.get(pos).is_subscribed());

                        //Log.d(TAG, "onItemClick: libAllowed = " + searchlist.get(pos).getLib_allowed());
                        setLib_allowed(searchlist.get(pos).getLib_allowed());

                        b.putBoolean("lib_allowed", searchlist.get(pos).getLib_allowed());
                        b.putBoolean("read_only", searchlist.get(pos).isRead_only());

                        EventPictureViewFragment fragment = new EventPictureViewFragment();
                        fragment.setArguments(b);

                        //hide keyboard
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        getFragmentManager().beginTransaction()
                                .replace(R.id.container, fragment, "esgf")
                                .addToBackStack(null)
                                .commit();
                    }
                }
            });
        }
    }

    @Override
    public void EventCreated(EventModel createdEvent) {
        Bundle b = new Bundle();
        b.putInt("event_id", createdEvent.getEvent_id());
        b.putString("event_name", createdEvent.getEvent_name());
        b.putBoolean("new_event", true);
        b.putString("attachedBy", "search");
        b.putString("remaining", createdEvent.getRemaining());
        b.putBoolean("is_owner", createdEvent.getIs_owner());

        Log.d(TAG, "EventCreated: lib_allowed = " + createdEvent.getLib_allowed());

        b.putBoolean("lib_allowed", createdEvent.getLib_allowed());

        EventPictureViewFragment fragment = new EventPictureViewFragment();
        fragment.setArguments(b);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "esgf")
                .addToBackStack(null)
                .commit();
    }

    public void setEventlist(ArrayList<EventModel> eventlist) {
        this.eventlist = eventlist;
    }

    @Override
    public void Events(ArrayList<EventModel> eventlist) {
        setEventlist(eventlist);
        ImageView ivEventEmpty = (ImageView) findViewById(R.id.ivEventEmpty);

        if (eventlist.size() > 0) {
            if (ivEventEmpty != null)
                ivEventEmpty.setVisibility(View.GONE);

        } else {
            if (ivEventEmpty != null)
                ivEventEmpty.setVisibility(VISIBLE);
        }

        ListView lvEvents = (ListView) findViewById(R.id.lvEvents);
        EventListAdapter adapter = new EventListAdapter(this, R.layout.event_list, eventlist);
        if (lvEvents != null)
            lvEvents.setAdapter(adapter);
    }

    @Override
    public void Uploaded(int event_id) {
        Events ev = new Events(EventActivity.this);
        ev.picturesEvent(event_id);
    }

    @Override
    public void onGalleryViewComplete(final SwipeRefreshLayout swipeContainer, final int event_id) {

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Events ev = new Events(EventActivity.this);
                ev.picturesEvent(event_id);
                swipeContainer.setRefreshing(false);
            }
        });
    }

    private void reportPic() {

        Log.d(TAG, "pic_id= " + pic_id);

        Users um = new Users(this);
        um.picturesReport(pic_id);
    }

    @Override
    public void PrivateEventSuccessful(int pos, ArrayList<EventModel> searchlist) {
        //add this event to history
        Gson gson = new Gson();

        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedprefs.edit();

        String previousHistory = sharedprefs.getString("history", ""); //fetch previousHistory

        List<EventModel> historyList = new ArrayList<>();

        Log.d(TAG, "onItemClick: previousHistory = " + previousHistory);

        if (!previousHistory.equals("")) {
            try {
                JSONArray searches = new JSONArray(previousHistory);

                for (int iter = 0; iter < searches.length(); iter++) {
                    JSONObject finalObject = searches.getJSONObject(iter);

                    EventModel search = gson.fromJson(finalObject.toString(), EventModel.class);

                    historyList.add(search); // historylist now contains the items in sharedprefs as List<EventModel>
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            //check list for match before adding
            boolean match = false;
            for (EventModel search : historyList) {
                if (search.getEvent_name().equals(searchlist.get(pos).getEvent_name())) {
                    match = true;
                }
            }

            if (!match) {
                if (historyList.size() > 6) {
                    //no more than 6 items added
                    //remove oldest to make room for the new one
                    historyList.remove(0);
                }

                historyList.add(searchlist.get(pos)); // append this clicked item to historylist items
            }

        } else {
            //nothing to fetch in prefs, just add this item to prefs
            historyList.add(searchlist.get(pos));
        }

        //add history items to sharedprefs
        String historyItems = gson.toJson(historyList);

        editor.putString("history", historyItems);
        editor.commit();

        Bundle b = new Bundle();
        b.putInt("event_id", searchlist.get(pos).getEvent_id());
        b.putString("event_name", searchlist.get(pos).getEvent_name());
        //b.putBoolean("is_search", true);
        b.putString("attachedBy", "search");
        b.putString("remaining", searchlist.get(pos).getRemaining());
        b.putBoolean("is_owner", searchlist.get(pos).getIs_owner());

        EventPictureViewFragment fragment = new EventPictureViewFragment();
        fragment.setArguments(b);

        //hide keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, "esgf")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void PrivateEventFail(final int pos, final ArrayList<EventModel> searchlist) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(EventActivity.this);

        LayoutInflater li = LayoutInflater.from(EventActivity.this);
        View privateEventLayout = li.inflate(R.layout.private_event_dialog, null);

        builder.setView(privateEventLayout);

        final AlertDialog dialog = builder.create();

        TextView tvPrivateEventname = (TextView) privateEventLayout.findViewById(R.id.tvPrivateEventName);
        tvPrivateEventname.setText(searchlist.get(pos).getEvent_name());

        final EditText etPrivateEventPassword = (EditText) privateEventLayout.findViewById(R.id.etPrivateEventPassword);

        Button btEnterPrivateEvent = (Button) privateEventLayout.findViewById(R.id.btEnterPrivateEvent);

        dialog.show();

        btEnterPrivateEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Events em = new Events(EventActivity.this);

                String password = etPrivateEventPassword.getText().toString();

                try {
                    em.eventsPrivateAccess(pos, searchlist, password);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });
    }

    @Override
    public void ShowCreateEventButton(final String event_name, boolean show) {
        Log.d(TAG, "ShowCreateEventButton: create a button with " + event_name + " as the name");
        // get reference to this button and in click event go to create event fragment.

        final Button btCreate = (Button) findViewById(R.id.btCreateNoMatchEvent);
        if (btCreate != null)
            btCreate.setVisibility(View.GONE);

        if (!show) {
            btCreate.setVisibility(View.GONE);
        }


        if (!event_name.equals("") && show && btCreate != null) {

            btCreate.setVisibility(View.VISIBLE);
            btCreate.setText("# Create ");
            btCreate.setText("# Create " + event_name);

            btCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    Bundle b = new Bundle();
                    b.putString("event_name", event_name);

                    CreateEventFragment cef = new CreateEventFragment();
                    cef.setArguments(b);

                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, cef, "evt")
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    public void cancelMulti(View v) {
        RelativeLayout defaultLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryDefault);
        RelativeLayout multiLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryMulti);
        LinearLayout llExtend = (LinearLayout) findViewById(R.id.llExtendHeader);
        llExtend.setVisibility(View.VISIBLE);

        defaultLayout.setVisibility(View.VISIBLE);
        multiLayout.setVisibility(View.GONE);

        android.support.v4.widget.SwipeRefreshLayout swipeRefresh = (android.support.v4.widget.SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

        swipeRefresh.setEnabled(true);

        setMulti(false);
        //rebuild adapter
        GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);
        ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(this, R.layout.grid_layout_image, getPiclist(), 2, isMulti());
        if (gvEventPhotos != null)
            gvEventPhotos.setAdapter(adapter);
    }

    public void multiDownload(View v) {

        mSVProgressHUD.show();

        downloadList = new ArrayList<>();

        for (PictureModel pic : getPiclist()) {
            if (pic.isSelected()) {
                downloadList.add(pic);
                Log.d(TAG, "multiDownload: name = " + pic.getPic_url());
                //pic.setSelected(false);
            }


        }

        for (PictureModel pic : downloadList) {
            setDownloadCount2(downloadList.size());

            Log.d(TAG, "multiDownload: url = " + pic.getPic_url() + " sent");
            //Log.d(TAG, "ifpicselected: getDownloadCount = " + getDownloadCount());

            Glide.with(EventActivity.this)
                    .load(pic.getPic_url())
                    .asBitmap()
                    .toBytes(Bitmap.CompressFormat.JPEG, 80)
                    .into(new SimpleTarget<byte[]>() {
                        @Override
                        public void onResourceReady(final byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {

                            Bundle b = new Bundle();
                            b.putByteArray("resource", resource);
                            getSupportLoaderManager().restartLoader(1, b, EventActivity.this).forceLoad();
                        }
                    });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if (bp != null && !bp.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data);
                int pos = data.getIntExtra("pos", -1);
                //setSelectedCardIndex(pos);
                DetailCardFragment fragment = (DetailCardFragment) getFragmentManager().findFragmentByTag("dcf");
                fragment.pullCardToIndex(pos);
            } else {
                int pos = data.getIntExtra("pos", -1);
                //setSelectedCardIndex(pos);
                DetailCardFragment fragment = (DetailCardFragment) getFragmentManager().findFragmentByTag("dcf");
                if (fragment != null)
                    fragment.pullCardToIndex(pos);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void setLike(String liked, TextView tvLikes) {
        Log.d(TAG, "setLike: liked = " + liked);

        //fetch textbox value to an int
        int likes = Integer.parseInt(tvLikes.getText().toString());

        switch (liked) {
            case "like":
                likes++;
                tvLikes.setText(String.valueOf(likes));

                break;

            case "unlike":
                likes--;
                if (likes < 0)
                    likes = 0;

                tvLikes.setText(String.valueOf(likes));
                break;
        }
    }


    public void onClickExtend(View v) {
        if (bp != null) {
            boolean isOneTimePurchaseSupported = bp.isOneTimePurchaseSupported();
            if (isOneTimePurchaseSupported) {
                EventTags et = (EventTags) v.getTag();

                Bundle c = new Bundle();
                c.putString("event_name", et.getEvent_name());
                c.putInt("event_id", et.getEvent_id());
                c.putString("remaining", et.getRemaining());
                Log.d("Purchase", "onClickExtend: event_id = " + event_id);
                Log.d("Purchase", "onClickExtend: event_name = " + et.getEvent_name());
                ExtendEventFragment fragment = new ExtendEventFragment();

                fragment.setArguments(c);

                getFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment, "eef")
                        .addToBackStack(null)
                        .commit();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.onetime_purchase_not_supported), Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.google_play_service_not_avail), Toast.LENGTH_LONG).show();
        }


    }

    public void onClickExtendBack(View v) {
        FragmentManager fm = getFragmentManager();
        fm.popBackStackImmediate();
    }

    @Override
    public void MyPhotos(ArrayList<PictureModel> piclist) {

    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {

        /**
         * @deprecated use {@see purchaseInfo.purchaseData.purchaseToken}} instead.
         */

        Log.d(TAG, "onProductPurchased: purchaseToken= " + details.purchaseInfo.purchaseData.purchaseToken);
        Log.d(TAG, "onProductPurchased: product_id = " + productId);
        Log.d(TAG, "onProductPurchased: event_id = " + event_id);

        Log.d("Purchase", "onProductPurchased: purchaseToken= " + details.purchaseInfo.purchaseData.purchaseToken);
        Log.d("Purchase", "onProductPurchased: product_id = " + productId);
        Log.d("Purchase", "onProductPurchased: event_id = " + event_id);
        //details.purchaseInfo.purchaseData.purchaseToken

        mSVProgressHUD.show();
        bp.consumePurchase(productId);
        Events ev = new Events(this);
        ev.eventsExtendWithCallback(event_id, productId, details.purchaseInfo.purchaseData.purchaseToken + "", new EventExtendApiCallback());
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.e("Purchase", "Purchase History restored");
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

        Log.d(TAG, "onBillingError: errorCode = " + errorCode);

        Toast.makeText(this, "onBillingError: " + Integer.toString(errorCode), Toast.LENGTH_LONG).show();

        //return to eventPictureView with refreshed info
        //FragmentManager fm = getFragmentManager();
        //fm.popBackStackImmediate();
        Log.e("Purchase", "Billing error " + errorCode);
        if (error != null)
            Log.e("Purchase", "Billing error " + error.getLocalizedMessage());
    }

    @Override
    public void onBillingInitialized() {
        Log.e("Purchase", "Billing Initialized");
    }

    @Override
    public Loader<File> onCreateLoader(int id, Bundle args) {
        return new multiDownload(this, args.getByteArray("resource"));
    }

    @Override
    public void onLoadFinished(Loader<File> loader, File data) {

        Log.d(TAG, "onLoadFinished: name = " + data.getName() + " downloaded");

        //gallery scanner
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(data);
            mediaScanIntent.setData(contentUri);
            sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://"
                            + Environment.getExternalStorageDirectory())));
        }

        Toast.makeText(EventActivity.this, "Picture " + String.valueOf(downloadCount2) + " completed.", Toast.LENGTH_LONG).show();

        setDownloadCount2(getDownloadCount2() - 1);

        if (downloadCount2 == 0) {
            mSVProgressHUD.dismiss();

            NetworkUtils nu = new NetworkUtils(EventActivity.this);
            nu.downloadMultiDialog();

            RelativeLayout defaultLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryDefault);
            RelativeLayout multiLayout = (RelativeLayout) findViewById(R.id.rlEventGalleryMulti);
            LinearLayout llExtend = (LinearLayout) findViewById(R.id.llExtendHeader);
            llExtend.setVisibility(View.VISIBLE);

            defaultLayout.setVisibility(View.VISIBLE);
            multiLayout.setVisibility(View.GONE);

            SwipeRefreshLayout swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeContainerEventGallery);

            swipeRefresh.setEnabled(true);

            setMulti(false);
            //rebuild adapter
            GridView gvEventPhotos = (GridView) findViewById(R.id.gvEventPhotos);
            ProfilePhotosGridViewAdapter adapter = new ProfilePhotosGridViewAdapter(EventActivity.this, R.layout.grid_layout_image, getPiclist(), 2, isMulti());
            if (gvEventPhotos != null)
                gvEventPhotos.setAdapter(adapter);
        }

    }

    @Override
    public void onLoaderReset(Loader<File> loader) {

    }

    private class EventExtendApiCallback implements ApiCallback {

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
            // TODO :: Destroy ExtendEventFragment and Load EventFragment
            mSVProgressHUD.dismiss();
            Log.e("Purchase","Our Server returned Success");
            try {
                FragmentManager fm = getFragmentManager();
                fm.popBackStackImmediate();
                JSONObject data = res.getJSONObject("data");
                String expTime = data.getString("newexpiredate");
                EventBus.getDefault().post(new ExtendEventSuccess(Utility.getRemainingTime(expTime)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
            mSVProgressHUD.dismiss();
            Log.e("Purchase","Our Server returned FAIL");
        }

        @Override
        public void noInternet() {
            // TODO :: Show no internet available toast
            mSVProgressHUD.dismiss();
            Toast.makeText(getApplicationContext(), getString(R.string.please_check_your_net), Toast.LENGTH_LONG).show();
        }
    }

    private static class multiDownload extends AsyncTaskLoader<File> {

        File theFile;
        byte[] resource;

        public multiDownload(Context context, byte[] resource) {
            super(context);
            this.resource = resource;
        }

        @Override
        public File loadInBackground() {

            File photoDirectory = CameraHelper.getPhotoDirectory();
            //Log.d(TAG, photoDirectory.getPath());
            File file = new File(photoDirectory + "/" + String.valueOf(downloadCount2) + CameraHelper.getTimeStampFilename());
            File dir = file.getParentFile();
            try {
                if (!dir.mkdirs() && (!dir.exists() || !dir.isDirectory())) {
                    throw new IOException("Cannot ensure parent directory for file " + file);
                }
                BufferedOutputStream s = new BufferedOutputStream(new FileOutputStream(file));
                s.write(resource);
                s.flush();
                s.close();

                theFile = file;


            } catch (IOException e) {
                e.printStackTrace();
            }


            return theFile;
        }
    }
}


