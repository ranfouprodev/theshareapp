package corp.com.shareapp.location;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by randyfournier on 10/25/16.
 */

public class GooglePlayServiceUtility {

    private static final String TAG ="ServiceUtil";
    public static final int GPS_ERRORDIALOG_REQUEST = 9002;

    public static boolean isPlayServiceAvailable(Context context) {
        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        switch (isAvailable) {
            case ConnectionResult.SUCCESS:
                Log.i(TAG, "Connected to service");
                return true;
            case ConnectionResult.SERVICE_MISSING:
                Log.i(TAG, "Service missing");
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Log.i(TAG, "Update required");
                break;
            case ConnectionResult.SERVICE_INVALID:
                Log.i(TAG, "Service invalid");
                break;

            default:
                Log.i(TAG, "Different value: " + isAvailable);
                break;
        }
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable,
                (Activity)context, GPS_ERRORDIALOG_REQUEST);
        dialog.show();
        return false;
    }

}
