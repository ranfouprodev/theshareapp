package corp.com.shareapp.camera;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;

import java.io.File;
import java.io.IOException;

/**
 * Created by randyfournier on 8/5/16.
 */
public class CameraHelper {

    static final String TAG = "CameraHelper";



    public static File getPhotoDirectory() {
        File outputDir = null;
        String externalStorageState = Environment.getExternalStorageState();
        if (externalStorageState.equals(Environment.MEDIA_MOUNTED)) {
            File pictureDir =
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DCIM);
            outputDir = new File(pictureDir, "Camera");
            Log.e(TAG, outputDir.toString());
            if (!outputDir.exists()) {
                if (!outputDir.mkdirs()) {
                    String message = "Failed to create directory:" + outputDir.getAbsolutePath();
                    Log.e(TAG, message);
                    outputDir = null;
                }
            }
        }

        return outputDir;
    }

    public static File generateTimeStampPhotoFile() {
        File photoFile = null;
        File outputDir = getPhotoDirectory();

        if (outputDir != null) {
            //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String timeStamp = String.valueOf(System.currentTimeMillis()/1000);
            String photoFileName = "IMG_" + timeStamp + ".jpg";

            photoFile = new File(outputDir, photoFileName);
            Log.e(TAG, photoFileName);
        }

        return photoFile;
    }

    public static String getTimeStampFilename(){

        String timeStamp = String.valueOf(System.currentTimeMillis()/1000);
        String photoFileName = "IMG_" + timeStamp + ".jpg";

        return photoFileName;
    }




    public static Uri generateTimeStampPhotoFileUri() {
        Uri photoFileUri = null;
        File photoFile = generateTimeStampPhotoFile();

        if (photoFile != null) {
            photoFileUri = Uri.fromFile(photoFile);
        }

        return photoFileUri;
    }

    public static int getDeviceOrientationDegrees(Context context) {
        int degrees = 0;

        WindowManager windowManager =
                (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int rotation = windowManager.getDefaultDisplay().getRotation();

        switch(rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        return degrees;
    }

    public static int readExifForOrientation(String photoPath){

        int orientation=0;

        ExifInterface ei = null;

        try {
            ei = new ExifInterface(photoPath);

            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch(orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    orientation = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    orientation = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    orientation = 270;
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return orientation;
    }
}
