package corp.com.shareapp.interfaces;

import java.util.ArrayList;

import corp.com.shareapp.model.EventModel;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface IPrivateEventListener {

    void PrivateEventSuccessful(int pos, ArrayList<EventModel> searchlist);
    void PrivateEventFail(int pos, ArrayList<EventModel> searchlist);
}
