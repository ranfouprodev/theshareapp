package corp.com.shareapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.model.PictureModel;
import corp.com.shareapp.nearby.NearbyTabActivity;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by randyfournier on 8/23/16.
 */
public class FullScreenImageAdapter extends PagerAdapter {

    Context mContext;
    private Activity _activity;
    private List<PictureModel> piclist;
    private LayoutInflater inflater;
    private static final String TAG = "FullScreenImageAdapter";



    // constructor
    public FullScreenImageAdapter(Activity activity, List<PictureModel> objects) {
        this._activity = activity;
        this.piclist = objects;
    }

    @Override
    public int getCount() {
        return this.piclist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        final ImageView imgDisplay;
        final PhotoViewAttacher mAttacher;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

        imgDisplay = (ImageView) viewLayout.findViewById(R.id.ivFullScreen);

        mAttacher = new PhotoViewAttacher(imgDisplay);

        SimpleTarget target = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {

                imgDisplay.setImageBitmap( bitmap );

                mAttacher.update();
            }
        };

        Glide.with(_activity).load(piclist.get(position).getPic_url())
                .asBitmap()
                .placeholder(R.drawable.loading_spinner)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .error(R.drawable.network_error)
                .into(target);

        ((ViewPager) container).addView(viewLayout);

        mAttacher.setOnViewTapListener(new PhotoViewAttacher.OnViewTapListener() {
            @Override
            public void onViewTap(View view, float x, float y) {
                Log.d(TAG, "onClick: current pic position = " + position);
                Intent i = new Intent(_activity, NearbyTabActivity.class);
                i.putExtra("pos", position);
                _activity.setResult(100, i);
                _activity.finish();
                // fetch value from activity calledFrom
                // if called From Nearby then open Nearby with position
            }
        });

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
