package corp.com.shareapp.network;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import corp.com.shareapp.R;
import corp.com.shareapp.adapter.CommentListAdapter;
import corp.com.shareapp.helper.NetworkUtils;
import corp.com.shareapp.model.CommentModel;
import cz.msebera.android.httpclient.Header;

/**
 * Created by randyfournier on 7/6/16.
 */
public class Comments extends ContextWrapper {

    private static final String TAG = "Comments";

    AsyncHttpClient client = new AsyncHttpClient();

    // no ssl workaround
    /*AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);*/


    public Comments(Context base) {
        super(base);
    }

    public void pictureGet(int pic_id, final TextView tvNoComments) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("pic_id", pic_id);

            client.get(getString(R.string.host) + getString(R.string.rt_comments_picture_get), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    try {
                        JSONpictureGet(res, tvNoComments);
                        //Log.d(TAG, res.toString());
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void userGet(int user_id) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            RequestParams params = new RequestParams();
            params.put("user_id", user_id);

            client.get(getString(R.string.host) + getString(R.string.rt_comments_user_get), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    JSONuserGet(res);
                    //Log.d(TAG, res.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }

    public void JSONpictureGet(JSONObject res, TextView tvNoComments) {

        Log.d(TAG,res.toString());

        // check for critical error
        try {
            if (res.has("error_message")) {

                String error_level = res.getString("error_level");
                String message = res.getString("error_message");

                if (error_level.equals("critical")) {

                    NetworkUtils nu = new NetworkUtils(this);
                    nu.criticalError("Error", message);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject dataObject = res.getJSONObject("data");
            JSONArray parentArray = dataObject.getJSONArray("comments");

            List<CommentModel> commentlist = new ArrayList<>();

            Gson gson = new Gson();
            for (int i = 0; i < parentArray.length(); i++) {
                JSONObject finalObject = parentArray.getJSONObject(i);
                CommentModel comment = gson.fromJson(finalObject.toString(), CommentModel.class);
                commentlist.add(comment);
            }

            if(tvNoComments!=null) {
                if (commentlist.size() == 0) {
                    tvNoComments.setVisibility(View.VISIBLE);
                } else {
                    tvNoComments.setVisibility(View.GONE);
                }
            }

            Activity activity = (Activity) getBaseContext();

            ListView lvComments = (ListView) activity.findViewById(R.id.lvComments);

            CommentListAdapter adapter = new CommentListAdapter(activity, R.layout.comment_list, commentlist);

            if(lvComments!=null)
            lvComments.setAdapter(adapter);

            lvComments.setSelection(adapter.getCount() - 1);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //may be redundant -- might substitiute with JSONpictureGet from userGet results
    public void JSONuserGet(JSONObject res) {

        //Log.d(TAG,resp.toString());

        // check for critical error
        try {
            if (res.has("error_message")) {

                String error_level = res.getString("error_level");
                String message = res.getString("error_message");

                if (error_level.equals("critical")) {

                    NetworkUtils nu = new NetworkUtils(this);
                    nu.criticalError("Error", message);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject dataObject = res.getJSONObject("data");
            JSONArray parentArray = dataObject.getJSONArray("comments");

            List<CommentModel> commentlist = new ArrayList<>();

            Gson gson = new Gson();
            for (int i = 0; i < parentArray.length(); i++) {
                JSONObject finalObject = parentArray.getJSONObject(i);
                CommentModel comment = gson.fromJson(finalObject.toString(), CommentModel.class);
                commentlist.add(comment);
            }

            Activity activity = (Activity) getBaseContext();

            ListView lvComments = (ListView) activity.findViewById(R.id.lvComments);

            CommentListAdapter adapter = new CommentListAdapter(activity, R.layout.comment_list, commentlist);

            lvComments.setAdapter(adapter);

            lvComments.setSelection(adapter.getCount() - 1);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void createComment(final String comment_text, final int pic_id, final TextView tvNoComments) {
        //check for connection
        final NetworkUtils nu = new NetworkUtils(this);
        if (nu.isOnline()) {

            SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            String session_id = sharedprefs.getString("session_id", "");

            RequestParams params = new RequestParams();
            params.put("session_id", session_id);
            params.put("pic_id", pic_id);
            params.put("comment_text", comment_text);

            client.post(getString(R.string.host) + getString(R.string.rt_comments_create), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject res) {
                    //Log.d(TAG, res.toString());
                    pictureGet(pic_id, tvNoComments);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    //Log.d(TAG, "res = " + res);
                    nu.networkDialog("Unreachable! (" + statusCode + ")", "Share's server is currently unavailable.");
                }
            });

        } else {
            nu.networkDialog("Can't Connect!", "Check your connection to the internet.");
        }
    }
}

