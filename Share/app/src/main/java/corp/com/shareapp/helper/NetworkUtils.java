package corp.com.shareapp.helper;


import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import corp.com.shareapp.network.Users;
import corp.com.shareapp.R;

/**
 * Created by randyfournier on 7/6/16.
 */
public class NetworkUtils extends ContextWrapper {

    private static final String TAG = "NetworkUtils";


    public NetworkUtils(Context base) {
        super(base);
    }

    public boolean isOnline() { // do we have a connection to the internet
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public void criticalError(String caption, String message){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.network_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvCaption = (TextView)dialog.findViewById(R.id.tvNetworkDialogCaption);
        TextView tvMessage = (TextView)dialog.findViewById(R.id.tvNetworkDialogMessage);
        TextView tv_OK = (TextView)dialog.findViewById(R.id.tvNetworkDialogOK);
        tvCaption.setText(caption);
        tvMessage.setText(message);

        tv_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call Logout
                SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                String session_id = sharedprefs.getString("session_id", "");

                Users um = new Users(NetworkUtils.this);

                um.logout(session_id); // logout to re-login because of dead session_id -- logout method takes us back to login screen
            }
        });

        dialog.show();

    }

    public void networkDialog(String op_status, String message){

        /*Activity activity = (Activity)getBaseContext();

        TSnackbar snack = TSnackbar.make(activity.findViewById(R.id.snackContainer), message, TSnackbar.LENGTH_LONG);
        View view = snack.getView();
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams)view.getLayoutParams();
        //params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        view.setPadding(30,30,30,30);
        view.setBackgroundColor(Color.RED);
        snack.show();*/

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.network_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        TextView tvCaption = (TextView)dialog.findViewById(R.id.tvNetworkDialogCaption);

        tvCaption.setText(StringUtils.capitalize(op_status.toLowerCase()));

        TextView tvMessage = (TextView)dialog.findViewById(R.id.tvNetworkDialogMessage);
        tvMessage.setText(message);

        TextView tv_OK = (TextView)dialog.findViewById(R.id.tvNetworkDialogOK);
        tv_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void deleteDialog(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.delete_event_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView btnCancel = (TextView)dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView btnDelete = (TextView)dialog.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call picturesDelete routine with pic_id
                //picturesDelete();
                dialog.dismiss();
                //refresh the listview
            }
        });
    }

    public void downloadDialog(){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.download_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView tv_OK = (TextView)dialog.findViewById(R.id.tvNetworkDialogOK);
        tv_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void downloadMultiDialog(){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.download_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        TextView tvmessage = (TextView)dialog.findViewById(R.id.tvNetworkDialogMessage);
        tvmessage.setText("The pictures have been saved to your library");

        TextView tv_OK = (TextView)dialog.findViewById(R.id.tvNetworkDialogOK);
        tv_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


}

