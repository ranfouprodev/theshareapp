package corp.com.shareapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import corp.com.shareapp.model.EventModel;
import corp.com.shareapp.R;

/**
 * Created by randyfournier on 7/28/16.
 */
public class EventSearchListAdapter extends ArrayAdapter {

    private static final String TAG = "EventListAdapter";

    //private Context mContext;

    private List<EventModel> eventModelList;
    private int resource;
    private LayoutInflater inflater;

    public EventSearchListAdapter(Context context, int resource, List<EventModel> objects) {
        super(context, resource, objects);
        // this.mContext = context;
        eventModelList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.search_list, null);
        }

        TextView tvEventName = (TextView) convertView.findViewById(R.id.tvResult);
        TextView tvRemaining = (TextView) convertView.findViewById(R.id.tvSearchRemaining);


        tvEventName.setText(eventModelList.get(position).getEvent_name());
        tvRemaining.setText(eventModelList.get(position).getRemaining() + " left");

        return convertView;
    }
}
