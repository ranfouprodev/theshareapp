package corp.com.shareapp;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by ashwinnbhanushali on 20/04/17.
 */

public class Utility {

    private Utility(){}

    public static String getRemainingTime(String expirationDate){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime end = formatter.parseDateTime(expirationDate);

        DateTime now = new DateTime();

        Period p = new Period(now, end, PeriodType.dayTime());

        if (p.getDays() == 0 && p.getHours() == 0) {
            return String.valueOf(p.getMinutes()) + "m";
        } else if (p.getDays() == 0 && p.getHours() > 0) {
            return String.valueOf(p.getHours()) + "h";
        } else if (p.getDays() > 0) {
            return String.valueOf(p.getDays()) + "d";
        } else {
            return "0m";
        }
    }
}
