package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.facebook.appevents.AppEventsLogger;

import corp.com.shareapp.R;

/**
 * Created by randyfournier on 8/20/16.
 */
public class SettingsFragment extends Fragment {

    public SettingsFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit My Settings");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.settings_fragment, container, false);

        Switch save_to_gallery = (Switch)theView.findViewById(R.id.swSaveToGallery);

        SharedPreferences sharedprefs = getActivity().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedprefs.edit();

        save_to_gallery.setChecked(sharedprefs.getBoolean("save_to_gallery", true));

        save_to_gallery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //commit prefs on change
                editor.putBoolean("save_to_gallery", isChecked);
                editor.commit();
            }
        });




        return theView;
    }
}
