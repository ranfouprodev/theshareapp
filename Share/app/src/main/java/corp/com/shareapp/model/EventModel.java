package corp.com.shareapp.model;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by randyfournier on 7/28/16.
 */
public class EventModel {

    private static final String TAG = "EventModel";

    private int event_id;
    private String event_name;
    private boolean is_public;

    public boolean getIs_public() {
        return is_public;
    }

    private boolean has_access;
    private boolean is_owner;
    private boolean lib_allowed;
    private boolean read_only;

    public boolean isRead_only() {
        return read_only;
    }

    public void setRead_only(boolean read_only) {
        this.read_only = read_only;
    }

    private String expire_date;
    private String creation_date;
    private int pic_count;
    private String type;

    private boolean is_subscribed;

    public boolean is_subscribed() {
        return is_subscribed;
    }

    public void setIs_subscribed(boolean is_subscribed) {
        this.is_subscribed = is_subscribed;
    }

    public String getRemaining() {

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime end = formatter.parseDateTime(getExpire_date());

        DateTime now = new DateTime();

        Period p = new Period(now, end, PeriodType.dayTime());

        if (p.getDays() == 0 && p.getHours() == 0) {
            return String.valueOf(p.getMinutes()) + "m";
        } else if (p.getDays() == 0 && p.getHours() > 0) {
            return String.valueOf(p.getHours()) + "h";
        } else if (p.getDays() > 0) {
            return String.valueOf(p.getDays()) + "d";
        } else {
            return "0m";
        }
    }


    public String getType() {

        return is_public ? "Public" : "Private";
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return "#" + event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public void setIs_public(boolean is_public) {
        this.is_public = is_public;
    }

    public boolean getHas_access() {
        return has_access;
    }

    public void setHas_access(boolean has_access) {
        this.has_access = has_access;
    }

    public boolean getIs_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    public boolean getLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public int getPic_count() {
        return pic_count;
    }

    public void setPic_count(int pic_count) {
        this.pic_count = pic_count;
    }
}
