package corp.com.shareapp.user;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.bigkoo.svprogresshud.SVProgressHUD;

import corp.com.shareapp.R;
import corp.com.shareapp.main.LoginActivity;
import corp.com.shareapp.network.Users;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";

    private SVProgressHUD mSVProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mSVProgressHUD = new SVProgressHUD(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        // fix flicker between screen transitions
        overridePendingTransition(0, 0);
    }

    public void onClickToLogin(View v) {

        //set visitedSignup
        SharedPreferences sharedprefs = getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedprefs.edit();
        editor.putBoolean("visitedSignup", true);
        editor.putString("fb_id", "home");
        editor.commit();

        Intent i = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(i);
    }

    public void onClickSignup(View v) {

        //get edittext's texts and trim spaces
        EditText usernameField = (EditText) findViewById(R.id.editTextUsername);
        EditText passwordField = (EditText) findViewById(R.id.editTextPasswordSignup);
        EditText confirmPasswordField = (EditText) findViewById(R.id.editTextConfirmPassword);
        EditText emailField = (EditText) findViewById(R.id.editTextEmail);

        String username = usernameField.getText().toString().trim();
        String password = passwordField.getText().toString().trim();
        String confirmPassword = confirmPasswordField.getText().toString().trim();
        String email = emailField.getText().toString().trim();

        //Log.d(TAG,"Password = " + password + " confirmPassword = " + confirmPassword);

        if (password.equals(confirmPassword)) {


            Users um = new Users(this);
            mSVProgressHUD.show();
            um.createOrLogin(username, email, password, "signup", mSVProgressHUD);
        } else {

            confirmPasswordField.setError("This has to be the same as Password. Please try again");
            passwordField.setText("");
            confirmPasswordField.setText("");
        }


    }
}
