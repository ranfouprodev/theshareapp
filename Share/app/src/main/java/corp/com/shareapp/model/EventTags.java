package corp.com.shareapp.model;

/**
 * Created by randyfournier on 8/2/16.
 */

public class EventTags {

    private int event_id;
    private String event_name;
    private String remaining;
    private boolean is_owner;
    private boolean lib_allowed;
    private boolean read_only;
    private boolean is_subscribed;

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public boolean is_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    public boolean isLib_allowed() {
        return lib_allowed;
    }

    public void setLib_allowed(boolean lib_allowed) {
        this.lib_allowed = lib_allowed;
    }

    public boolean isRead_only() {
        return read_only;
    }

    public void setRead_only(boolean read_only) {
        this.read_only = read_only;
    }

    public boolean is_subscribed() {
        return is_subscribed;
    }

    public void setIs_subscribed(boolean is_subscribed) {
        this.is_subscribed = is_subscribed;
    }
}