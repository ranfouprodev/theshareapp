package corp.com.shareapp.interfaces;

import android.widget.TextView;

/**
 * Created by randyfournier on 9/9/16.
 */
public interface ILikeListener {

    void setLike(String liked, TextView tvLikes);
}
