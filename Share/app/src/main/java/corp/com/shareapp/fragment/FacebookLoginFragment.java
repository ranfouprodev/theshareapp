package corp.com.shareapp.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import corp.com.shareapp.R;
import corp.com.shareapp.helper.DialogLauncher;
import corp.com.shareapp.network.Users;


/**
 * A placeholder fragment containing a simple view.
 */
public class FacebookLoginFragment extends Fragment {

    private static final String TAG = "FacebookLoginFragment";
    private String username;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private com.facebook.login.widget.LoginButton loginButton;
    //private LoginButton loginButton;
    private CallbackManager callbackManager;

    public FacebookLoginFragment() {


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_fb_login, container, false);

        //Log.d(TAG, "onCreateView");

        //remove left icon on the button
        Button fb_login_button = (Button) view.findViewById(R.id.login_button);
        fb_login_button.setCompoundDrawables(null, null, null, null);

        setupFacebookLogin(view);

        return view;
    }

    private void setupFacebookLogin(View view) {

        callbackManager = CallbackManager.Factory.create();

        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        loginButton.setFragment(this);
        loginButton.setReadPermissions("email");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {

                final AccessToken accessToken = loginResult.getAccessToken();

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(final JSONObject object, GraphResponse response) {
                                //Log.v("FacebookLoginFragment", response.toString());

                                //call login
                                Users us = new Users(getActivity());

                                    if (!us.loginFb(accessToken.getUserId(),accessToken.getToken())) {

                                        //ask for username after successful get of facebook_id
                                        final Dialog dialog = new Dialog(getActivity());

                                        dialog.setContentView(R.layout.facebook_username_dialog);
                                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        dialog.show();

                                        TextView tvCancel = (TextView) dialog.findViewById(R.id.btnFacebookCancel);
                                        TextView tvRegister = (TextView) dialog.findViewById(R.id.btnFacebookRegister);
                                        final TextView tvUsername = (TextView) dialog.findViewById(R.id.tvFBUsername);

                                        tvCancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                LoginManager.getInstance().logOut();
                                                accessToken.setCurrentAccessToken(null);
                                                dialog.dismiss();
                                            }
                                        });

                                        tvRegister.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                setUsername(tvUsername.getText().toString());

                                                LoginManager.getInstance().logOut();
                                                accessToken.setCurrentAccessToken(null);
                                                dialog.dismiss();

                                                // ask graph for email
                                                String emailInTextbox = null;
                                                try {
                                                    emailInTextbox = (object.getString("email"));
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                // check if no email is returned
                                                // switch dialog message

                                                // dialog to alert user no email or do they like this email or do they want to change it
                                                dialog.setContentView(R.layout.facebook_email_dialog);
                                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                                dialog.show();

                                                TextView tvFacebookEmailMessage = (TextView) dialog.findViewById(R.id.tvFacebookEmailMessage);
                                                if(emailInTextbox.equals("")){
                                                    tvFacebookEmailMessage.setText("We weren't able to get your email from Facebook. Please enter an email address.");
                                                }


                                                TextView tvEmailCancel = (TextView) dialog.findViewById(R.id.btnFacebookEmailCancel);
                                                TextView tvProceed = (TextView) dialog.findViewById(R.id.btnFacebookEmailProceed);
                                                final EditText etFacebookEmail = (EditText) dialog.findViewById(R.id.etFacebookEmailAddress);

                                                //Log.d(TAG, "onClick: email = "+email);
                                                etFacebookEmail.setText(emailInTextbox);

                                                tvEmailCancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        LoginManager.getInstance().logOut();
                                                        accessToken.setCurrentAccessToken(null);
                                                        dialog.dismiss();
                                                    }
                                                });

                                                tvProceed.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        // call api with fb_id, email, password
                                                        setEmail(etFacebookEmail.getText().toString());
                                                        Users us = new Users(getActivity());
                                                        try {
                                                            us.createFb(getUsername(),getEmail(),accessToken.getUserId(),accessToken.getToken());
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        } catch (UnsupportedEncodingException e) {
                                                            e.printStackTrace();
                                                        }
                                                        dialog.dismiss();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                DialogLauncher.launchAlert(getActivity(), "Canceled", "Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException exception) {
                DialogLauncher.launchAlert(getActivity(), "Error", "Could not log in with Facebook.");
            }
        });

    }
}
