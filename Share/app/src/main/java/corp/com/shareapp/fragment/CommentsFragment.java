package corp.com.shareapp.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;

import corp.com.shareapp.R;
import corp.com.shareapp.comment.CommentsActivity;
import corp.com.shareapp.event.EventActivity;
import corp.com.shareapp.nearby.NearbyTabActivity;
import corp.com.shareapp.network.Comments;
import corp.com.shareapp.profile.ProfileActivity;

/**
 * Created by randyfournier on 8/20/16.
 */
public class CommentsFragment extends Fragment {

    private static final String TAG ="CommentsFragment";

    private int position;

    private int num_comments;

    public int getNum_comments() {
        return num_comments;
    }

    public void setNum_comments(int num_comments) {
        this.num_comments = num_comments;
    }

    private int pic_id;

    public int getPic_id() {
        return pic_id;
    }

    public void setPic_id(int pic_id) {
        this.pic_id = pic_id;
    }

    public String getAttachedBy() {
        return attachedBy;
    }

    public void setAttachedBy(String attachedBy) {
        this.attachedBy = attachedBy;
    }

    private String attachedBy;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    private String event_name;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public CommentsFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View theView = inflater.inflate(R.layout.comments_fragment, container, false);
        return theView;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Visit Comments");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Bundle b = getArguments();

        if (b != null) {

            setPic_id(b.getInt("pic_id"));
            setPosition(b.getInt("position"));
            //Log.d(TAG, "onViewCreated: passed in pos = " + b.getInt("position"));
            setEvent_name(b.getString("event_name"));
            setAttachedBy(b.getString("attachedBy"));
            setNum_comments(b.getInt("numComments"));
        }

        //Log.d(TAG, "onViewCreated: CommentsFragment position = " + getPosition() + " attachedBy= " + getAttachedBy());
        Log.d(TAG, "onViewCreated: pic_id = " + getPic_id());

        final TextView tvNoComments = (TextView)view.findViewById(R.id.tvNoComments);
        final Comments cm = new Comments(getActivity());

        cm.pictureGet(getPic_id(), tvNoComments);

        final EditText etComment = (EditText) view.findViewById(R.id.tvComment);
        final Button btComment = (Button) view.findViewById(R.id.btComment);

        btComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(CommentsActivity.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                String comment = etComment.getText().toString().trim();


                if (getPic_id() != 0 && !comment.equals("")) {
                    cm.createComment(comment, getPic_id(), tvNoComments);
                    etComment.setText("");
                    setNum_comments(getNum_comments()+1);
                }
            }
        });

        // move this to activity
        ImageView ivCommentsBack = (ImageView)view.findViewById(R.id.ivCommentsBack);
        ivCommentsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getAttachedBy().equals("nearby")){
                    //NearbyTabActivity.setComment_count(getNum_comments());
                    NearbyTabActivity.setSelectedCardIndex(position);
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStackImmediate();

                }else if(getAttachedBy().equals("myphotos") || getAttachedBy().equals("gallery")){
                    ProfileActivity.setComment_count(getNum_comments());
                    Log.d(TAG, "onClick: in comments numComments = " + getNum_comments());
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStackImmediate();

                }else{
                    EventActivity.setComment_count(getNum_comments());
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStackImmediate();
                }

            }
        });

    }
}
