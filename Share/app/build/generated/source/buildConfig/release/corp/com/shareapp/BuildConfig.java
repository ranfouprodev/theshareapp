/**
 * Automatically generated file. DO NOT MODIFY
 */
package corp.com.shareapp;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "corp.com.shareapp";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 45;
  public static final String VERSION_NAME = "1.0";
}
